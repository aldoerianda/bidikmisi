<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTahunToSmkSelects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('smk_selects', function (Blueprint $table) {
            $table->string('tahun',5)->default((date('Y')));
        });
        Schema::table('sma_selects', function (Blueprint $table) {
            $table->string('tahun',5)->default((date('Y')));
        });
        Schema::table('man_selects', function (Blueprint $table) {
            $table->string('tahun',5)->default((date('Y')));
        });
        Schema::table('daerah_selects', function (Blueprint $table) {
            $table->string('tahun',5)->default((date('Y')));
        });
        Schema::table('prodi_selects', function (Blueprint $table) {
            $table->string('tahun',5)->default((date('Y')));
        });
        Schema::table('cadangans', function (Blueprint $table) {
            $table->string('tahun',5)->default((date('Y')));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('smk_selects', function (Blueprint $table) {
            //
        });
    }
}
