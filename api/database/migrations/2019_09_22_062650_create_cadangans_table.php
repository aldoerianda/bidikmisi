<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCadangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cadangans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nomor_pendaftaran',50)->unique();
            $table->string('nisn',20)->unique();
            $table->string('nama_peserta',50);
            $table->string('nama_sekolah',100);
            $table->string('provinsi_sekolah',50);
            $table->string('akreditasi_sekolah',100);
            $table->string('jurusan_asal',50);
            $table->double('nilai');
            $table->string('kode_jur',100);
            $table->text('jur_diterima',100);
            $table->string('daftar_bidikmisi',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cadangans');
    }
}
