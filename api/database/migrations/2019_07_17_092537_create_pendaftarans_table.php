<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendaftaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftarans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nisn',20);
            $table->string('nama_peserta',50);
            $table->string('nomor_pendaftaran',50);
            $table->string('jurusan_asal',50);
            $table->string('kode_pil1',10);
            $table->text('pilihan1',100);
            $table->string('kode_pil2',10)->nullable();
            $table->text('pilihan2',100)->nullable();
            $table->string('kode_pil3',10)->nullable();
            $table->text('pilihan3',100)->nullable();
            $table->string('kode_jur',100)->nullable();
            $table->text('jur_diterima',100)->nullable();
            $table->string('daftar_bidikmisi',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftarans');
    }
}
