<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Symfony\Component\HttpFoundation\Request;

class CreateNilaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilais', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nisn',20);
            $table->string('nama_peserta',50);
            $table->string('nama_sekolah',50);
            $table->double('nilai_rata');
            $table->double('nilai_prestasi')->nullable();
            $table->double('nilai_jurusan')->nullable();
            $table->double('nilai_rangking')->nullable();
            $table->double('nilai_total_sebelum')->nullable();
            $table->double('nilai_total_sesudah')->nullable();
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilais');
    }
}
