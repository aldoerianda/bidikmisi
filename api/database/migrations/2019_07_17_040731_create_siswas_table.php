<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table -> string('nisn')->unique();
            $table->string('nama_peserta',50);
            $table->string('tanggal_lahir');
            $table->text('alamat')->nullable();
            $table->string('kecamatan',50);
            $table->string('kabupaten',50);
            $table->string('provinsi',50);
            $table->string('telpon',15)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
