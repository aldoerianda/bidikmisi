<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSekolahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sekolahs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nisn',20);
            $table->string('nama_peserta',50);
            $table->string('nama_sekolah',100);
            $table->string('kecamatan_sekolah',50);
            $table->string('kota_sekolah',50);
            $table->string('provinsi_sekolah',50);
            $table->string('akreditasi_sekolah',100);
            $table->string('jurusan_asal',50);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sekolahs');
    }
}
