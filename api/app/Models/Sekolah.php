<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    // public $timestamps = false;

    protected $fillable = [
        'nisn','nama_peserta','nama_sekolah','kecamatan_sekolah','kota_sekolah',
        'provinsi_sekolah','akreditasi_sekolah',
        'jurusan_asal'
    ];
    public function siswa(){
        return $this->belongsTo(Siswa::class,'nisn');
    }
}
