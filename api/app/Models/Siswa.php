<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    // public $timestamps = false;

    protected $fillable = [
        'nisn','nama_peserta','tanggal_lahir','alamat','kecamatan','kabupaten',
        'provinsi','telepon'
    ];
    public function siswa(){
        return $this->belongsTo(Siswa::class,'nisn');
    }
}
