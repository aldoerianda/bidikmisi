<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SmkSelect extends Model
{
    protected $fillable = [
        'nomor_pendaftaran','nisn','nama_peserta','nama_sekolah',
        'provinsi_sekolah','akreditasi_sekolah',
        'jurusan_asal','nilai','kode_jur','jur_diterima','daftar_bidikmisi'
    ];
    public function siswa(){
        return $this->belongsTo(Siswa::class,'nisn');
    }
    public function nilai(){
        return $this->belongsTo(Nilai::class,'nisn');
    }
}
