<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    // public $timestamps = false;

    protected $fillable = [
        'nisn','nama_peserta','nomor_pendaftaran','jurusan_asal','kode_pil1','pilihan1','kode_pil2','pilihan2',
        'kode_pil3','pilihan3','kode_jur','jur_diterima','daftar_bidikmisi'
    ];
    public function siswa(){
        return $this->belongsTo(Siswa::class,'nisn');
    }
}
