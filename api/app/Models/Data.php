<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;

class Data implements ToModel
{
    // public $timestamps = false;

    public $fillable = ['no_pendaftaran','nisn','nama_peserta'];
}
