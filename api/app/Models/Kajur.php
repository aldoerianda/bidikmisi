<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kajur extends Model
{
    protected $fillable = [
        'name','email','jurusan','password'
    ];
    public function jurusan(){
        return $this->belongsTo(Jurusan::class,'kode');
    }
}
