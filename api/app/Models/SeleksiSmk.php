<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SeleksiSmk extends Model
{
    protected $fillable = [
        'nisn','nama_peserta','nama_sekolah',
        'provinsi_sekolah','akreditasi_sekolah',
        'jurusan_asal'
    ];
    public function siswa(){
        return $this->belongsTo(Siswa::class,'nisn');
    }
    public function nilai(){
        return $this->belongsTo(Nilai::class,'nisn');
    }
}
