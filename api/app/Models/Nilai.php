<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    // public $timestamps = false;
    protected $fillable = [
        'nisn','nama_peserta','nama_sekolah','nilai_rata','nilai_prestasi','nilai_jurusan',
        'nilai_rangking','nilai_total_sebelum','nilai_total_sesudah'
    ];
    public function siswa(){
        return $this->belongsTo(Siswa::class,'nisn');
    }
}
