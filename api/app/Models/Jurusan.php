<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $fillable = [
        'kode','nama_jurusan',
    ];
    public function prodi(){
        return $this->belongsTo(Prodi::class,'kode');
    }

}
