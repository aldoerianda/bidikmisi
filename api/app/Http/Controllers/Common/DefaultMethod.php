<?php

namespace App\Http\Controllers\Common;

use Defuse\Crypto\File;
use Illuminate\Http\Request;
use App\Http\Resources\apiResources as Resources;
use Illuminate\Support\Facades\File as LaraFile;
use Illuminate\Support\Facades\DB;
use Excel;
// use App\Models\Siswa;
// use App\Models\Sekolah;

trait DefaultMethod
{
    public function index(Request $request)
    {
        $value = '';
        try {
            if (isset($request->page)) {
                $value = $this->getModels()::with($this->relation)->paginate(10);
                return new Resources ($value);
            }
            $value = $this->getModels()::with($this->relation)->get();
            return new Resources($value);
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }


    public function store(Request $request)
    {


        $data = $request->except('_token');

        try {
            if ($request->has('password')) {
                $data['password'] = bcrypt($data['password']);

            }

            return $this->getModels()::create($data);
        } catch (\Exception $e) {
        }catch(\Exception $e) {
            return $e->getMessage();
        }
    }

    public function show($id)
    {
        try {
            $value = $this->getModels()::with($this->relation)->find($id);
            return new Resources ($value);
        } catch (\Exception $e) {
            return $e->getMessage();
        }

    }

    public function update(Request $request, $id)
    {
        $data = $request->except('_token');
        try {
            $getData = $this->getModels()::find($id);
            $fileNameDB = $getData->image;
            if ($request->image) {

                if (LaraFile::exists(public_path() . '/imageservice/'.$fileNameDB)) {
                    LaraFile::delete(public_path() . '/imageservice/'.$fileNameDB);
                }

                $exploded = explode(',', $request->image);
                $decoded = base64_decode($exploded[1]);
                if (str_contains($exploded[0], 'jpg'))
                    $extension = 'jpg';
                else
                    $extension = 'png';
                $fileName = str_random() . '.' . $extension;
                $path = public_path() . '/imageservice/' . $fileName;
                file_put_contents($path, $decoded);

                $update = $this->getModels()::find($id)->update($request->except('image') + ['image' => $fileName]);

                if( $update) return response()->json(['message' => 'data berhsil diubah']);

            }

            $value = $this->getModels()::find($id)->update($data);

            if ($value) {
                return response()->json(['message' => 'data berhsil diubah']);
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    public function destroy($id)
    {
        try {
            $getData = $this->getModels()::find($id);
            $fileNameDB = $getData->image;
            if (LaraFile::exists(public_path() . '/imageservice/'.$fileNameDB)) {
                LaraFile::delete(public_path() . '/imageservice/'.$fileNameDB);
            }

            $value = $this->getModels()::find($id)->destroy($id);

            return response()->json(['message' => 'data Berhasil dihapus'], 200);

        } catch (\Exception $e) {
            dd($e);
            return $e->getMessage();
        }

    }
}
