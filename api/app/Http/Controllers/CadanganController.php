<?php

namespace App\Http\Controllers;

use App\Models\Cadangan;
use Illuminate\Http\Request;
use App\Models\DaerahSelect;

class CadanganController extends Controller
{
   public $model = 'Cadangan';
   public $relation = [];
public function deleteCadangan (){
    Cadangan::truncate();
    return response()->json([ 'status' => 'done' ]);

}
   public function listDataCadangan ($kode){
    $baru=Cadangan::where('kode_jur',$kode)
    ->where('tahun',date('Y'))
    ->orderBy('nama_sekolah')->get();

    // dd($baru);
    return response()->json($baru);
}
}
