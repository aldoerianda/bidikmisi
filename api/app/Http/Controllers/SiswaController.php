<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use Illuminate\Http\Request;
use DB;
use Excel;
class SiswaController extends Controller
{
    public $model = 'Siswa';
    public $relation = [];

    public function input (Request $request){
        // dd($request->file('image'));
        if ($request->hasFile('image')) {
            DB::transaction(function () use ($request) {
                $path = $request->file('image')->getRealPath();
                // dd($path);
                $dat = Excel::load($path, function($reader){})->get();
                    if(!empty($dat) && $dat->count()){
                        foreach ($dat as $key => $value){
                            $checkdata = Siswa::where('nisn', $value->nisn)->get();
                            if(count($checkdata)>0){
                            }else{
                                $siswa = new Siswa();
                                $siswa->nisn = $value->nisn;
                                $siswa->nama_peserta = $value->nama_peserta;
                                $siswa->tanggal_lahir = $value->tanggal_lahir;
                                $siswa->jenis_kelamin = $value -> jenis_kelamin;
                                $siswa->alamat = $value->alamat;
                                $siswa->kecamatan = $value->kecamatan;
                                $siswa->kabupaten = $value->kabupaten;
                                $siswa->provinsi = $value->provinsi;
                                $siswa->telpon = (strln($value['telpon']>14)? '080000000000' : $value['telpon']);
                                $siswa->save();
                                $sekolah = new Sekolah();
                                $sekolah->nisn = $value->nisn;
                                $sekolah->nama_peserta = $value->nama_peserta;
                                $sekolah->nama_sekolah = $value->nama_sekolah;
                                $sekolah->provinsi_sekolah = $value->provinsi_sekolah;
                                $sekolah->kota_sekolah = $value->kabupaten_kota_sekolah;
                                $sekolah->akreditasi_sekolah = $value->akreditasi_sekolah;
                                $sekolah->jurusan_asal = $value->jurusan_asal;
                                $sekolah->save();
                                $nilai = new Nilai();
                                $nilai->nisn = $value->nisn;
                                $nilai->nama_peserta = $value->nama_peserta;
                                $nilai->nama_sekolah = $value->nama_sekolah;
                                $nilai->nilai_rata = $value->nilai_ratarata_rapor;
                                $nilai->nilai_prestasi = $value->nilai_prestasi;
                                $nilai->nilai_jurusan = $value->nilai_jurusan;
                                $nilai->nilai_rangking = $value->nilai_rangking;
                                $nilai->nilai_total_sebelum = $value->nilai_total_sebelum_normalisasi;
                                $nilai->nilai_total_sesudah = $value->nilai_total_sesudah_normalisasi;
                                $nilai->save();
                                $pendaftaran = new Pendaftaran();
                                $pendaftaran->nisn = $value->nisn;
                                $pendaftaran->nama_peserta = $value->nama_peserta;
                                $pendaftaran->jurusan_asal = $value->jurusan_asal;
                                $pendaftaran->kode_pil1 = $value->kode_pil1;
                                $pendaftaran->pilihan1 = $value->pilihan_1;
                                $pendaftaran->kode_pil2 = $value->kode_pil2;
                                $pendaftaran->pilihan2 = $value->pilihan_2;
                                $pendaftaran->kode_pil3 = $value->kode_pil3;
                                $pendaftaran->pilihan3 = $value->pilihan_3;
                                $pendaftaran->save();
                                // Log::info($checkdata);
                            }
                        }
                    }
            });
        }
        return back();

    }


}
