<?php

namespace App\Http\Controllers;

use App\Models\DaerahSelect;
use App\Models\ProdiSelect;
use App\Models\Prodi;
use App\Models\Jurusan;
use Illuminate\Http\Request;
use App\Models\Cadangan;

class ProdiSelectController extends Controller
{
    public $model = 'ProdiSelect';
    public $relation = [];

    public function getSelectJurusan(Request $request){
        $prodi = DaerahSelect::where('kode_jur',$request->kode)
        ->where('tahun',date('Y'))
        ->get()->toArray();
        // $all = DaerahSelect::all()->toArray();
        // dd($prodi);

        foreach ($prodi as $newValue) {
            // dd($newValue['nomor_pendaftaran']);
            ProdiSelect::create([
                'nomor_pendaftaran' => $newValue['nomor_pendaftaran'],
                'nisn' => $newValue['nisn'],
                'nama_peserta' => $newValue['nama_peserta'],
                'nama_sekolah' => $newValue['nama_sekolah'],
                'provinsi_sekolah' => $newValue['provinsi_sekolah'],
                'akreditasi_sekolah' => $newValue['akreditasi_sekolah'],
                'jurusan_asal' => $newValue['jurusan_asal'],
                'nilai' => $newValue['nilai'],
                'kode_jur' => $newValue['kode_jur'],
                'jur_diterima' => $newValue['jur_diterima'],
                'daftar_bidikmisi' => $newValue['daftar_bidikmisi'],
                // 'nilai' =>$newValue->nilai_total_sesudah
            ]);
        }
        $isi=ProdiSelect::where('kode_jur',$request->kode)->orderBy('nilai','desc')->get('id')->take($request->diterima);
        // dd($isi);
//mengambil id yang telah masuk seleksi
        $ids = array_map(function ($item) {
            return $item['id'];
        },$isi->toArray());

        $not = ProdiSelect::where('kode_jur',$request->kode)->whereNotIn('id', $ids)->get()->take($request->cadangan);
        $idsnot = array_map(function($item){
            return $item['id'];
        },$not->toArray());

        foreach ($not as $newValue) {
            Cadangan::create([
                'nomor_pendaftaran' => $newValue['nomor_pendaftaran'],
                'nisn' => $newValue['nisn'],
                'nama_peserta' => $newValue['nama_peserta'],
                'nama_sekolah' => $newValue['nama_sekolah'],
                'provinsi_sekolah' => $newValue['provinsi_sekolah'],
                'akreditasi_sekolah' => $newValue['akreditasi_sekolah'],
                'jurusan_asal' => $newValue['jurusan_asal'],
                'nilai' => $newValue['nilai'],
                'kode_jur' => $newValue['kode_jur'],
                'jur_diterima' => $newValue['jur_diterima'],
                'daftar_bidikmisi' => $newValue['daftar_bidikmisi'],
                // 'nilai' =>$newValue->nilai_total_sesudah
            ]);
        }
        $delet = ProdiSelect::where('kode_jur',$request->kode)->whereNotIn('id', $ids)->delete();

        return response()->json([ 'status' => 'done' ]);
    }

    public function listDataProdi ($kode){
        $baru=ProdiSelect::where('kode_jur',$kode)
        ->where('tahun',date('Y'))
        ->orderBy('nama_sekolah')->orderBy('nilai','desc')->get();

        // dd($baru);
        return response()->json($baru);
    }
    public function listDataJurusan1 (){
        // $a = Prodi::join('jurusans','jurusans.kode','=','kode_jurusan')->get();
        return ProdiSelect::join('prodis','prodis.kode','=','kode_jur')
        ->join('jurusans','jurusans.kode','=','prodis.kode_jurusan')
        ->select(
            'prodi_selects.nama_sekolah',
            'prodi_selects.nomor_pendaftaran',
            'prodi_selects.nisn',
            'prodi_selects.nama_peserta',
            'prodi_selects.provinsi_sekolah',
            'prodi_selects.akreditasi_sekolah',
            'prodi_selects.jurusan_asal',
            'prodi_selects.nilai',
            'prodi_selects.kode_jur',
            'prodi_selects.jur_diterima',
            'prodi_selects.daftar_bidikmisi'
        )
        ->where('jurusans.kode','=','1')->orderBy('nilai','desc')->get();

    }
    public function listDataJurusan2 (){
        // $a = Prodi::join('jurusans','jurusans.kode','=','kode_jurusan')->get();
        return ProdiSelect::join('prodis','prodis.kode','=','kode_jur')
        ->join('jurusans','jurusans.kode','=','prodis.kode_jurusan')
        ->select(
            'prodi_selects.nama_sekolah',
            'prodi_selects.nomor_pendaftaran',
            'prodi_selects.nisn',
            'prodi_selects.nama_peserta',
            'prodi_selects.provinsi_sekolah',
            'prodi_selects.akreditasi_sekolah',
            'prodi_selects.jurusan_asal',
            'prodi_selects.nilai',
            'prodi_selects.kode_jur',
            'prodi_selects.jur_diterima',
            'prodi_selects.daftar_bidikmisi'
        )
        ->where('jurusans.kode','=','2')->orderBy('nilai','desc')->get();

    }
    public function listDataJurusan3 (){
        // $a = Prodi::join('jurusans','jurusans.kode','=','kode_jurusan')->get();
        return ProdiSelect::join('prodis','prodis.kode','=','kode_jur')
        ->join('jurusans','jurusans.kode','=','prodis.kode_jurusan')
        ->select(
            'prodi_selects.nama_sekolah',
            'prodi_selects.nomor_pendaftaran',
            'prodi_selects.nisn',
            'prodi_selects.nama_peserta',
            'prodi_selects.provinsi_sekolah',
            'prodi_selects.akreditasi_sekolah',
            'prodi_selects.jurusan_asal',
            'prodi_selects.nilai',
            'prodi_selects.kode_jur',
            'prodi_selects.jur_diterima',
            'prodi_selects.daftar_bidikmisi'
        )
        ->where('jurusans.kode','=','3')->orderBy('nilai','desc')->get();

    }
    public function listDataJurusan4 (){
        // $a = Prodi::join('jurusans','jurusans.kode','=','kode_jurusan')->get();
        return ProdiSelect::join('prodis','prodis.kode','=','kode_jur')
        ->join('jurusans','jurusans.kode','=','prodis.kode_jurusan')
        ->select(
            'prodi_selects.nama_sekolah',
            'prodi_selects.nomor_pendaftaran',
            'prodi_selects.nisn',
            'prodi_selects.nama_peserta',
            'prodi_selects.provinsi_sekolah',
            'prodi_selects.akreditasi_sekolah',
            'prodi_selects.jurusan_asal',
            'prodi_selects.nilai',
            'prodi_selects.kode_jur',
            'prodi_selects.jur_diterima',
            'prodi_selects.daftar_bidikmisi'
        )
        ->where('jurusans.kode','=','4')->orderBy('nilai','desc')->get();

    }
    public function listDataJurusan5 (){
        // $a = Prodi::join('jurusans','jurusans.kode','=','kode_jurusan')->get();
        return ProdiSelect::join('prodis','prodis.kode','=','kode_jur')
        ->join('jurusans','jurusans.kode','=','prodis.kode_jurusan')
        ->select(
            'prodi_selects.nama_sekolah',
            'prodi_selects.nomor_pendaftaran',
            'prodi_selects.nisn',
            'prodi_selects.nama_peserta',
            'prodi_selects.provinsi_sekolah',
            'prodi_selects.akreditasi_sekolah',
            'prodi_selects.jurusan_asal',
            'prodi_selects.nilai',
            'prodi_selects.kode_jur',
            'prodi_selects.jur_diterima',
            'prodi_selects.daftar_bidikmisi'
        )
        ->where('jurusans.kode','=','5')->orderBy('nilai','desc')->get();

    }
    public function listDataJurusan6 (){
        // $a = Prodi::join('jurusans','jurusans.kode','=','kode_jurusan')->get();
        return ProdiSelect::join('prodis','prodis.kode','=','kode_jur')
        ->join('jurusans','jurusans.kode','=','prodis.kode_jurusan')
        ->select(
            'prodi_selects.nama_sekolah',
            'prodi_selects.nomor_pendaftaran',
            'prodi_selects.nisn',
            'prodi_selects.nama_peserta',
            'prodi_selects.provinsi_sekolah',
            'prodi_selects.akreditasi_sekolah',
            'prodi_selects.jurusan_asal',
            'prodi_selects.nilai',
            'prodi_selects.kode_jur',
            'prodi_selects.jur_diterima',
            'prodi_selects.daftar_bidikmisi'
        )
        ->where('jurusans.kode','=','6')->orderBy('nilai','desc')->get();

    }
    public function listDataJurusan7 (){
        // $a = Prodi::join('jurusans','jurusans.kode','=','kode_jurusan')->get();
        return ProdiSelect::join('prodis','prodis.kode','=','kode_jur')
        ->join('jurusans','jurusans.kode','=','prodis.kode_jurusan')
        ->select(
            'prodi_selects.nama_sekolah',
            'prodi_selects.nomor_pendaftaran',
            'prodi_selects.nisn',
            'prodi_selects.nama_peserta',
            'prodi_selects.provinsi_sekolah',
            'prodi_selects.akreditasi_sekolah',
            'prodi_selects.jurusan_asal',
            'prodi_selects.nilai',
            'prodi_selects.kode_jur',
            'prodi_selects.jur_diterima',
            'prodi_selects.daftar_bidikmisi'
        )
        ->where('jurusans.kode','=','7')->orderBy('nilai','desc')->get();

    }

    public function deleteProdi($kode){
        ProdiSelect::where('kode_jur',$kode)->delete();
        return response()->json([ 'status' => 'done' ]);
    }
}
