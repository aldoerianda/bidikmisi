<?php

namespace App\Http\Controllers;

use DB;
use Session;
use Excel;
use File;
// use Excel;
use App\Models\Data;
use Illuminate\Http\Request;
//use Maatwebsite\Excel\Facades\Excel;
// use Session;

class DataController extends Controller
{
    protected $model = 'Data';

    public function importExport()

    {

        return view('importExport');

    }

    public function downloadExcel($type)

    {

        $data = Item::get()->toArray();



        return Excel::create('itsolutionstuff_example', function($excel) use ($data) {

            $excel->sheet('mySheet', function($sheet) use ($data)

            {

                $sheet->fromArray($data);

            });

        })->download($type);

    }

    public function importExcel(Request $request)
    {


        // $request->validate([

        //     'import_file' => 'required'

        // ]);



        // $path = $request->file('import_file')->getRealPath();
        // //dd($path);
        // $data = Excel::load($path)->get();
        // //dd(1);


        // if($data->count()){

        //     foreach ($data as $key => $value) {
        //         //dd(1);

        //         $arr[] = ['no pendaftaran' => $value->no_pendaftaran,
        //                  'nisn' => $value->nisn,
        //                  'nama peserta'=>$value->nama_peserta,
        //                 ];

        //     }




        //     if(!empty($arr)){

        //         Item::insert($arr);

        //     }

        // }

        // return back()->with('success', 'Insert Record successfully.');

 //validate the xls file
 //dd(1);
 $this->validate($request, array(
    'file'      => 'required'
));


if($request->hasFile('file')){
    $extension = File::extension($request->file->getClientOriginalName());
    if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

        $path = $request->file->getRealPath();
        $data = Excel::load($path, function($reader) {
        })->get();
        if(!empty($data) && $data->count()){

            foreach ($data as $key => $value) {
                $insert[] = [
                'nisn' => $value->nisn,
                'nama peserta' => $value->nama_peserta,
                ];
            }

            if(!empty($insert)){

                $insertData = DB::table('data')->insert($insert);
                if ($insertData) {
                    Session::flash('success', 'Your Data has successfully imported');
                }else {
                    Session::flash('error', 'Error inserting the data..');
                    return back();
                }
            }
        }

        return back();

    }else {
        Session::flash('error', 'File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
        return back();
    }
}
    }


}
