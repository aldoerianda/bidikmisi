<?php

namespace App\Http\Controllers;

use App\Models\Prodi;
use Illuminate\Http\Request;
use Response;

class ProdiController extends Controller
{
    public $model = 'Prodi';
    public $relation = [];

    public function getListTI(){
         $prodi = Prodi::select()->where('kode_jurusan','1')->get();

        return Response::json($prodi);

    }
    public function getListEc(){
        $prodi = Prodi::select()->where('kode_jurusan','2')->get();

       return Response::json($prodi);

   }
   public function getListSipil(){
    $prodi = Prodi::select()->where('kode_jurusan','3')->get();

   return Response::json($prodi);

    }
    public function getListMesin(){
        $prodi = Prodi::select()->where('kode_jurusan','4')->get();

       return Response::json($prodi);

   }
   public function getListAkun(){
    $prodi = Prodi::select()->where('kode_jurusan','5')->get();

   return Response::json($prodi);

}
public function getListBi(){
    $prodi = Prodi::select()->where('kode_jurusan','6')->get();

   return Response::json($prodi);

}
public function getListAdm(){
    $prodi = Prodi::select()->where('kode_jurusan','7')->get();

   return Response::json($prodi);

}
public function getLIstAll(){
    $prodi = Prodi::All();
    return Response::json($prodi);
}

}
