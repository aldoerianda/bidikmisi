<?php

namespace App\Http\Controllers;

use App\Models\SeleksiSmk;
use Illuminate\Http\Request;
use App\Models\Sekolah;
use DB;
use Response;

class SeleksiSmkController extends Controller
{
    public function helo () {
        return 'dd';
    }
    public function getListSmk ($request) {
        $sekolah = \App\Models\SeleksiSmk::class;
        // foreach($sekolah as ){

        // }
        return DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah',
        'akreditasi_sekolah','provinsi_sekolah','jurusan_asal')
        ->where('akreditasi_sekolah','=', $request->akreditasi ? $request->akreditasi : null)
        // ->where('nilai','=', '70')
        ->where(function ($query){
            $query->where('nama_sekolah', 'LIKE' , '%SMK%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMKN%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMKS%');
        })
        ->get();

    }

    public function seleksiSmk (Request $request){
        $selecsmk = $this->getListSmk($request);   
        
        return Response::json($selecsmk);

        // return $insert = $sekolah::create($selecsmk);
    }

    public function storeSeleksi(Request $request) {
        $selecsmk = $this->getListSmk($request);

        foreach ($selecsmk as $newValue) {
            SeleksiSmk::create([  
                'nisn' => $newValue->nisn,
                'nama_peserta' => $newValue->nama_peserta,
                'nama_sekolah' => $newValue->nama_sekolah,
                'provinsi_sekolah' => $newValue->provinsi_sekolah,
                'akreditasi_sekolah' => $newValue->akreditasi_sekolah,
                'jurusan_asal' => $newValue->jurusan_asal
            ]);
        }

        return response()->json([ 'status' => 'done' ]);        
    }
}
