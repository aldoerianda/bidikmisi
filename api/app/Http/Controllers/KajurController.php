<?php

namespace App\Http\Controllers;

use App\Models\Kajur;
use Illuminate\Http\Request;

class KajurController extends Controller
{
    // protected $model = 'Kajur';
    // protected $relation = [];

    public function store (Request $request){
        $data = $request->all();
        return Kajur::create($data);
    }
}
