<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Siswa;
use App\Models\Sekolah;
use App\Models\Nilai;
use App\Models\Pendaftaran;
// use Illuminate\Http\Request;
use DB;
use Excel;
use App\Imports\SiswaImport;
use Response;

class SiswaImportController extends Controller
{

    protected $model = 'Siswa';
    public $relation = [];

    public function input (Request $request){
        if ($request->hasFile('image')) {
            DB::transaction(function () use ($request) {

                Excel::import( new SiswaImport , $request->file('image'));
                // $path = $request->file('image')->getRealPath();
                // // dd($path);
                // $dat = Excel::load($path, function($reader){})->get();
                //     if(!empty($dat) && $dat->count()){
                //         foreach ($dat as $key => $value){
                //             $checkdata = Siswa::where('nisn', $value->nisn)->get();
                //             if(count($checkdata)>0){
                //             }else{
                //                 $siswa = new Siswa();
                //                 $siswa->nisn = $value->nisn;
                //                 $siswa->nama_peserta = $value->nama_peserta;
                //                 $siswa->tanggal_lahir = $value->tanggal_lahir;
                //                 $siswa->jenis_kelamin = $value -> jenis_kelamin;
                //                 $siswa->alamat = $value->alamat;
                //                 $siswa->kecamatan = $value->kecamatan;
                //                 $siswa->kabupaten = $value->kabupaten;
                //                 $siswa->provinsi = $value->provinsi;
                //                 $siswa->telpon = (strln($value['telpon']>14)? '080000000000' : $value['telpon']);
                //                 $siswa->save();
                //                 $sekolah = new Sekolah();
                //                 $sekolah->nisn = $value->nisn;
                //                 $sekolah->nama_peserta = $value->nama_peserta;
                //                 $sekolah->nama_sekolah = $value->nama_sekolah;
                //                 $sekolah->provinsi_sekolah = $value->provinsi_sekolah;
                //                 $sekolah->kota_sekolah = $value->kabupaten_kota_sekolah;
                //                 $sekolah->akreditasi_sekolah = $value->akreditasi_sekolah;
                //                 $sekolah->jurusan_asal = $value->jurusan_asal;
                //                 $sekolah->save();
                //                 $nilai = new Nilai();
                //                 $nilai->nisn = $value->nisn;
                //                 $nilai->nama_peserta = $value->nama_peserta;
                //                 $nilai->nama_sekolah = $value->nama_sekolah;
                //                 $nilai->nilai_rata = $value->nilai_ratarata_rapor;
                //                 $nilai->nilai_prestasi = $value->nilai_prestasi;
                //                 $nilai->nilai_jurusan = $value->nilai_jurusan;
                //                 $nilai->nilai_rangking = $value->nilai_rangking;
                //                 $nilai->nilai_total_sebelum = $value->nilai_total_sebelum_normalisasi;
                //                 $nilai->nilai_total_sesudah = $value->nilai_total_sesudah_normalisasi;
                //                 $nilai->save();
                //                 $pendaftaran = new Pendaftaran();
                //                 $pendaftaran->nisn = $value->nisn;
                //                 $pendaftaran->nama_peserta = $value->nama_peserta;
                //                 $pendaftaran->jurusan_asal = $value->jurusan_asal;
                //                 $pendaftaran->kode_pil1 = $value->kode_pil1;
                //                 $pendaftaran->pilihan1 = $value->pilihan_1;
                //                 $pendaftaran->kode_pil2 = $value->kode_pil2;
                //                 $pendaftaran->pilihan2 = $value->pilihan_2;
                //                 $pendaftaran->kode_pil3 = $value->kode_pil3;
                //                 $pendaftaran->pilihan3 = $value->pilihan_3;
                //                 $pendaftaran->save();
                //                 // Log::info($checkdata);
                //             }
                //         }
                //     }
            });
        }
        return back();

    }
    public function selectAll(){
        return Siswa::join('nilais','nilais.nisn','=','siswas.nisn')
        ->join('sekolahs','sekolahs.nisn','=','siswas.nisn')
        ->join('pendaftarans','pendaftarans.nisn','=','siswas.nisn')
        ->select(
            'siswas.nisn',
            'siswas.nama_peserta',
            'sekolahs.nama_sekolah',
            'sekolahs.provinsi_sekolah',
            'sekolahs.akreditasi_sekolah',
            'sekolahs.jurusan_asal',
            'nilais.nilai_total_sesudah',
            'pendaftarans.nomor_pendaftaran',
            'pendaftarans.kode_jur',
            'pendaftarans.jur_diterima',
            'pendaftarans.daftar_bidikmisi'
        )
        ->orderBy('nilai_total_sesudah','desc')
        ->get();
    }

    public function seleksiSekolah(Request $request){
        $data = ($request->all());
        $sekolah = Sekolah::all();
        $selecsmk = DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah',
        'akreditasi_sekolah','provinsi_sekolah','jurusan_asal')
        ->where('akreditasi_sekolah','=','A')
        ->where(function ($query){
            $query->where('nama_sekolah', 'LIKE' , '%SMK%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMKN%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMKS%');
        })
        ->get();
        // DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah','akreditasi_sekolah','jurusan_asal')
        // ->where('nama_sekolah', 'LIKE' , '%SMK%')
        // ->where('akreditasi_sekolah','=','A')
        // ->orWhere('nama_sekolah', 'LIKE' , '%SMKN%')
        // ->orWhere('nama_sekolah', 'LIKE' , '%SMKS%')
        // >orWhere('nama_sekolah', 'LIKE' , '%SMK%')
        // ->paginate(20);
        // ->limit()
        // ->grupby();
        // dd($selecsmk);
        return Response::json($selecsmk);
    }
    public function seleksiSmk(Request $request){
        // $data = ($request->all());
        // $sekolah = Sekolah::all();
        $selecsmk = DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah',
        'akreditasi_sekolah','provinsi_sekolah','jurusan_asal')
        ->where('akreditasi_sekolah','=','B')
        ->where(function ($query){
            $query->where('nama_sekolah', 'LIKE' , '%SMK%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMKN%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMKS%');
        })
        ->get();
        return Response::json($selecsmk);
    }
    public function seleksiSmkc(Request $request){
        // $data = ($request->all());
        // $sekolah = Sekolah::all();
        $selecsmk = DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah',
        'akreditasi_sekolah','provinsi_sekolah','jurusan_asal')
        ->where('akreditasi_sekolah','=','C')
        ->where(function ($query){
            $query->where('nama_sekolah', 'LIKE' , '%SMK%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMKN%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMKS%');
        })
        ->get();
        return Response::json($selecsmk);
    }
    public function seleksiSma(Request $request){
        // $data = ($request->all());
        // $sekolah = Sekolah::all();
        $selecsmk = DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah',
        'akreditasi_sekolah','provinsi_sekolah','jurusan_asal')
        ->where('akreditasi_sekolah','=','A')
        ->where(function ($query){
            $query->where('nama_sekolah', 'LIKE' , '%SMA%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMAN%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMAS%');
        })
        ->get();
        return Response::json($selecsmk);
    }
    public function seleksiSmab(Request $request){
        // $data = ($request->all());
        // $sekolah = Sekolah::all();
        $selecsmk = DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah',
        'akreditasi_sekolah','provinsi_sekolah','jurusan_asal')
        ->where('akreditasi_sekolah','=','B')
        ->where(function ($query){
            $query->where('nama_sekolah', 'LIKE' , '%SMA%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMAN%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMAS%');
        })
        ->get();
        return Response::json($selecsmk);
    }
    public function seleksiSmac(Request $request){
        // $data = ($request->all());
        // $sekolah = Sekolah::all();
        $selecsmk = DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah',
        'akreditasi_sekolah','provinsi_sekolah','jurusan_asal')
        ->where('akreditasi_sekolah','=','C')
        ->where(function ($query){
            $query->where('nama_sekolah', 'LIKE' , '%SMA%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMAN%')
            ->orWhere('nama_sekolah', 'LIKE' , '%SMAS%');
        })
        ->get();
        return Response::json($selecsmk);
    }
    public function seleksiMan(Request $request){
        // $data = ($request->all());
        // $sekolah = Sekolah::all();
        $selecsmk = DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah',
        'akreditasi_sekolah','provinsi_sekolah','jurusan_asal')
        ->where('akreditasi_sekolah','=','A')
        ->where(function ($query){
            $query->where('nama_sekolah', 'LIKE' , 'MAN%')
            // ->orWhere('nama_sekolah', 'LIKE' , 'MANN%')
            ->orWhere('nama_sekolah', 'LIKE' , 'MAS%');
        })
        ->get();
        return Response::json($selecsmk);
    }
    public function seleksiManb(Request $request){
        // $data = ($request->all());
        // $sekolah = Sekolah::all();
        $selecsmk = DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah',
        'akreditasi_sekolah','provinsi_sekolah','jurusan_asal')
        ->where('akreditasi_sekolah','=','b')
        ->where(function ($query){
            $query->where('nama_sekolah', 'LIKE' , 'MAN%')
            // ->orWhere('nama_sekolah', 'LIKE' , '%MANN%')
            ->orWhere('nama_sekolah', 'LIKE' , 'MAS%');
        })
        ->get();
        return Response::json($selecsmk);
    }
    public function seleksiManc(Request $request){
        // $data = ($request->all());
        // $sekolah = Sekolah::all();
        $selecsmk = DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah',
        'akreditasi_sekolah','provinsi_sekolah','jurusan_asal')
        ->where('akreditasi_sekolah','=','C')
        ->where(function ($query){
            $query->where('nama_sekolah', 'LIKE' , 'MAN%')
            // ->orWhere('nama_sekolah', 'LIKE' , 'MANN%')
            ->orWhere('nama_sekolah', 'LIKE' , 'MAS%');
        })
        ->get();
        return Response::json($selecsmk);
    }

}
