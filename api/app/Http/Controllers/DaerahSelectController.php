<?php

namespace App\Http\Controllers;

use App\Models\DaerahSelect;
use App\Models\SmkSelect;
use App\Models\SmaSelect;
use App\Models\ManSelect;
use App\Models\Cadangan;
use DB;

use Illuminate\Http\Request;

class DaerahSelectController extends Controller
{
    public $model = 'DaerahSelect';
    public $relation = [];

    public function deleteDaerah(){
        DaerahSelect::truncate();
        return response()->json([ 'status' => 'done' ]);

    }

    public function getSelectSumbar (Request $request) {

        // $sekolah = \App\Models\SeleksiDaerah::class;
        // $nilai = \App\Models\Nilai::class;
        // $prov='SUMATERA BARAT';
        // $smk = DB::table('smk_selects')->where('provinsi_sekolah','=',$prov);
        // $sma = DB::table('sma_selects')->where('provinsi_sekolah','=',$prov)
        // ->union($smk);
        // $man = DB::table('man_selects')->where('provinsi_sekolah','=',$prov)
        // ->union($sma)->get();
// dd($man);

        $smk = SmkSelect::where('provinsi_sekolah','SUMATERA BARAT')
        ->where('tahun',date('Y'))
        ->get()->toArray();
        // $smk = SmkSelect::where('provinsi_sekolah','<>','SUMATERA BARAT')->get()->toArray();
        $sma = SmaSelect::where('provinsi_sekolah','SUMATERA BARAT')
        ->where('tahun',date('Y'))
        ->get()->toArray();
        $man = ManSelect::where('provinsi_sekolah','SUMATERA BARAT')
        ->where('tahun',date('Y'))
        ->get()->toArray();
        $tes = array_merge($smk,$sma,$man);


        foreach ($tes as $newValue) {
            // dd($newValue['nomor_pendaftaran']);
            DaerahSelect::create([
                'nomor_pendaftaran' => $newValue['nomor_pendaftaran'],
                'nisn' => $newValue['nisn'],
                'nama_peserta' => $newValue['nama_peserta'],
                'nama_sekolah' => $newValue['nama_sekolah'],
                'provinsi_sekolah' => $newValue['provinsi_sekolah'],
                'akreditasi_sekolah' => $newValue['akreditasi_sekolah'],
                'jurusan_asal' => $newValue['jurusan_asal'],
                'nilai' => $newValue['nilai'],
                'kode_jur' => $newValue['kode_jur'],
                'jur_diterima' => $newValue['jur_diterima'],
                'daftar_bidikmisi' => $newValue['daftar_bidikmisi'],
                // 'nilai' =>$newValue->nilai_total_sesudah
            ]);
        }

        $isi=DaerahSelect::orderBy('nilai','desc')->get('id')->take($request->sumbar);
//mengambil id yang telah masuk seleksi
        $ids = array_map(function ($item) {
            return $item['id'];
        },$isi->toArray());

//id yang masuk ke cadangan
        // $not = DaerahSelect::whereNotIn('id', $ids)->get()->take($request->cadangan);
        // $idsnot = array_map(function($item){
        //     return $item['id'];
        // },$not->toArray());

        // foreach ($not as $newValue) {
        //     Cadangan::create([
        //         'nomor_pendaftaran' => $newValue['nomor_pendaftaran'],
        //         'nisn' => $newValue['nisn'],
        //         'nama_peserta' => $newValue['nama_peserta'],
        //         'nama_sekolah' => $newValue['nama_sekolah'],
        //         'provinsi_sekolah' => $newValue['provinsi_sekolah'],
        //         'akreditasi_sekolah' => $newValue['akreditasi_sekolah'],
        //         'jurusan_asal' => $newValue['jurusan_asal'],
        //         'nilai' => $newValue['nilai'],
        //         'kode_jur' => $newValue['kode_jur'],
        //         'jur_diterima' => $newValue['jur_diterima'],
        //         'daftar_bidikmisi' => $newValue['daftar_bidikmisi'],
        //         // 'nilai' =>$newValue->nilai_total_sesudah
        //     ]);
        // }

        $delet = DaerahSelect::whereNotIn('id', $ids)->delete();



        // $final =  array_merge($ids,$idsnot);
        // $hapus = DaerahSelect::whereNotIn('id', $ids)->get()->take(30);

        // dd($final);
// dd($not->toArray());

//         $coba = DB::raw(`SELECT * FROM smk_selects WHERE provinsi_sekolah='SUMATERA BARAT'
//         UNION SELECT * FROM sma_selects WHERE provinsi_sekolah='SUMATERA BARAT'
//         UNION SELECT * from man_selects where provinsi_sekolah = 'SUMATERA BARAT'`);
// dd($coba);


        return response()->json([ 'status' => 'done' ]);
        // foreach($sekolah as ){

        // }
        // return SmkSelect::join('sma_selects','sma_selects.nisn','=','sma_selects.nisn')
        // ->join('man_selects','.nisn','=','siswas.nisn')
        // ->join('pendaftarans','pendaftarans.nisn','=','siswas.nisn')
        // ->select(
        //     'siswas.nisn',
        //     'siswas.nama_peserta',
        //     'sekolahs.nama_sekolah',
        //     'sekolahs.provinsi_sekolah',
        //     'sekolahs.akreditasi_sekolah',
        //     'sekolahs.jurusan_asal',
        //     'nilais.nilai_total_sesudah',
        //     'sekolahs.provinsi_sekolah',
        //     'pendaftarans.nomor_pendaftaran',
        //     'pendaftarans.kode_jur',
        //     'pendaftarans.jur_diterima',
        //     'pendaftarans.daftar_bidikmisi'
        // )
        // ->where('sekolahs.akreditasi_sekolah','=',$request->akreditasi)
        // ->where('nilais.nilai_total_sesudah','>=',$request->nilai)
        // ->where('nilais.nilai_total_sesudah','>=',$request->jumlah)
        // ->whereNotNull('pendaftarans.kode_jur')
        // ->where(function ($query){
        //     $query->where('sekolahs.nama_sekolah', 'LIKE' , '%SMK%')
        //     ->orWhere('sekolahs.nama_sekolah', 'LIKE' , '%SMKN%')
        //     ->orWhere('sekolahs.nama_sekolah', 'LIKE' , '%SMKS%');
        // })
        // DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah',
        // 'akreditasi_sekolah','provinsi_sekolah','jurusan_asal')
        // ->where('akreditasi_sekolah','=', $request->akreditasi ? $request->akreditasi : null)
        // ->where('nilai','=', '70')
        // ->where(function ($query){
        //     $query->where('nama_sekolah', 'LIKE' , '%SMK%')
        //     ->orWhere('nama_sekolah', 'LIKE' , '%SMKN%')
        //     ->orWhere('nama_sekolah', 'LIKE' , '%SMKS%');
        // })
        // ->orderBy('nilai_total_sesudah','desc') ->get();
        // ->take($request->jumlah)
        // ->take($newValue->jumlah)

    }

    public function getLuarSumbar(Request $request){
        // $smk = SmkSelect::where('provinsi_sekolah','SUMATERA BARAT')->get()->toArray();
        $smk = SmkSelect::where('provinsi_sekolah','<>','SUMATERA BARAT')
        ->where('tahun',date('Y'))
        ->get()->toArray();
        $sma = SmaSelect::where('provinsi_sekolah','<>','SUMATERA BARAT')
        ->where('tahun',date('Y'))
        ->get()->toArray();
        $man = ManSelect::where('provinsi_sekolah','<>','SUMATERA BARAT')
        ->where('tahun',date('Y'))
        ->get()->toArray();
        $tes = array_merge($smk,$sma,$man);


        foreach ($tes as $newValue) {
            DaerahSelect::create([
                'nomor_pendaftaran' => $newValue['nomor_pendaftaran'],
                'nisn' => $newValue['nisn'],
                'nama_peserta' => $newValue['nama_peserta'],
                'nama_sekolah' => $newValue['nama_sekolah'],
                'provinsi_sekolah' => $newValue['provinsi_sekolah'],
                'akreditasi_sekolah' => $newValue['akreditasi_sekolah'],
                'jurusan_asal' => $newValue['jurusan_asal'],
                'nilai' => $newValue['nilai'],
                'kode_jur' => $newValue['kode_jur'],
                'jur_diterima' => $newValue['jur_diterima'],
                'daftar_bidikmisi' => $newValue['daftar_bidikmisi'],
                // 'nilai' =>$newValue->nilai_total_sesudah
            ]);
        }

        $isi=DaerahSelect::orderBy('nilai','desc')->get('id')->take($request->luarsumbar);
//mengambil id yang telah masuk seleksi
        $ids = array_map(function ($item) {
            return $item['id'];
        },$isi->toArray());

        $delet = DaerahSelect::whereNotIn('id', $ids)->delete();
        return response()->json([ 'status' => 'done' ]);

    }

    // public function seleksiDaerahLuar (Request $request){
    //     $selecsmk = $this->getListNilaiSmk($request);

    //     return \Response::json($selecsmk);

    //     // return $insert = $sekolah::create($selecsmk);
    // }

    // public function storeNilaiSeleksi(Request $request) {
    //     $selecsmk = $this->getListNilaiSmk($request);

    //     foreach ($selecsmk as $newValue) {
    //         SmkSelect::create([
    //             'nomor_pendaftaran' => $newValue->nomor_pendaftaran  ,
    //             'nisn' => $newValue->nisn,
    //             'nama_peserta' => $newValue->nama_peserta,
    //             'nama_sekolah' => $newValue->nama_sekolah,
    //             'provinsi_sekolah' => $newValue->provinsi_sekolah,
    //             'akreditasi_sekolah' => $newValue->akreditasi_sekolah,
    //             'jurusan_asal' => $newValue->jurusan_asal,
    //             'nilai' => $newValue->nilai_total_sesudah,
    //             'kode_jur' => $newValue->kode_jur,
    //             'jur_diterima' => $newValue->jur_diterima,
    //             'daftar_bidikmisi' => $newValue->daftar_bidikmisi,
    //             // 'nilai' =>$newValue->nilai_total_sesudah
    //         ]);
    //     }

    //     return response()->json([ 'status' => 'done' ]);
    // }
}
