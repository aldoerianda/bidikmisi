<?php

namespace App\Http\Controllers;

use App\Models\Nilai;
use App\Models\Pendaftaran;
use App\Models\Sekolah;
use App\Models\Siswa;
use Illuminate\Http\Request;

class NilaiController extends Controller
{
    public $model = 'Nilai';
    public $relation = [];

    public function deleteData (){

        Nilai::truncate();
        Siswa::truncate();
        Pendaftaran::truncate();
        Sekolah::truncate();
        return response()->json([ 'status' => 'done' ]);
    }

}
