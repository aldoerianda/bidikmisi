<?php

namespace App\Http\Controllers;

use App\Models\SmkSelect;
use Illuminate\Http\Request;
use App\Models\Siswa;
use App\Models\Sekolah;
use App\Models\SeleksiSmk;
use App\Models\Pendaftaran;
use App\Models\Nilai;

class SmkselectController extends Controller
{
    protected $model = 'SmkSelect';
    public $relation = [];

    public function deleteSmk (){
        SmkSelect::truncate();
        return response()->json([ 'status' => 'done' ]);

    }

    public function getListNilaiSmk ($request) {
        $sekolah = \App\Models\SeleksiSmk::class;
        $nilai = \App\Models\Nilai::class;
        // foreach($sekolah as ){

        // }
        return Siswa::join('nilais','nilais.nisn','=','siswas.nisn')
        ->join('sekolahs','sekolahs.nisn','=','siswas.nisn')
        ->join('pendaftarans','pendaftarans.nisn','=','siswas.nisn')
        ->select(
            'siswas.nisn',
            'siswas.nama_peserta',
            'sekolahs.nama_sekolah',
            'sekolahs.provinsi_sekolah',
            'sekolahs.akreditasi_sekolah',
            'sekolahs.jurusan_asal',
            'nilais.nilai_total_sesudah',
            'sekolahs.provinsi_sekolah',
            'pendaftarans.nomor_pendaftaran',
            'pendaftarans.kode_jur',
            'pendaftarans.jur_diterima',
            'pendaftarans.daftar_bidikmisi'
        )
        ->where('sekolahs.akreditasi_sekolah','=',$request->akreditasi)
        ->where('nilais.nilai_total_sesudah','>=',$request->nilai)
        ->where('siswas.tahun',date('Y'))
        // ->where('nilais.nilai_total_sesudah','>=',$request->jumlah)
        ->whereNotNull('pendaftarans.kode_jur')
        ->where(function ($query){
            $query->where('sekolahs.nama_sekolah', 'LIKE' , '%SMK%')
            ->orWhere('sekolahs.nama_sekolah', 'LIKE' , '%SMKN%')
            ->orWhere('sekolahs.nama_sekolah', 'LIKE' , '%SMKS%');
        })
        // DB::table('sekolahs')->select('nisn','nama_peserta','nama_sekolah',
        // 'akreditasi_sekolah','provinsi_sekolah','jurusan_asal')
        // ->where('akreditasi_sekolah','=', $request->akreditasi ? $request->akreditasi : null)
        // ->where('nilai','=', '70')
        // ->where(function ($query){
        //     $query->where('nama_sekolah', 'LIKE' , '%SMK%')
        //     ->orWhere('nama_sekolah', 'LIKE' , '%SMKN%')
        //     ->orWhere('nama_sekolah', 'LIKE' , '%SMKS%');
        // })
        ->orderBy('nilai_total_sesudah','desc')
        ->take($request->jumlah)
        // ->take($newValue->jumlah)
        ->get();

    }

    public function seleksiNilaiSmk (Request $request){
        $selecsmk = $this->getListNilaiSmk($request);

        return \Response::json($selecsmk);

        // return $insert = $sekolah::create($selecsmk);
    }

    public function storeNilaiSeleksi(Request $request) {
        $selecsmk = $this->getListNilaiSmk($request);

        foreach ($selecsmk as $newValue) {
            SmkSelect::create([
                'nomor_pendaftaran' => $newValue->nomor_pendaftaran  ,
                'nisn' => $newValue->nisn,
                'nama_peserta' => $newValue->nama_peserta,
                'nama_sekolah' => $newValue->nama_sekolah,
                'provinsi_sekolah' => $newValue->provinsi_sekolah,
                'akreditasi_sekolah' => $newValue->akreditasi_sekolah,
                'jurusan_asal' => $newValue->jurusan_asal,
                'nilai' => $newValue->nilai_total_sesudah,
                'kode_jur' => $newValue->kode_jur,
                'jur_diterima' => $newValue->jur_diterima,
                'daftar_bidikmisi' => $newValue->daftar_bidikmisi,
                // 'nilai' =>$newValue->nilai_total_sesudah
            ]);
        }

        return response()->json([ 'status' => 'done' ]);
    }
}
