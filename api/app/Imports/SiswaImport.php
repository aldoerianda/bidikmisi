<?php

namespace App\Imports;

// use App\Siswa;
use Maatwebsite\Excel\Concerns\ToCollection;
use App\Models\Sekolah;
use App\Models\Siswa;
use Illuminate\Support\Collection;
use App\Models\Pendaftaran;
use App\Models\Nilai;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class SiswaImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $rows)
    {
        foreach ($rows as $value)
        {
            // console.log()
            Siswa::create([
                'nisn' => $value[1],
                'nama_peserta' => $value[2],
                'tanggal_lahir' => $value[16],
                'alamat' => $value[17],
                'kecamatan' => $value[18],
                'kabupaten' => $value[19],
                'provinsi' => $value[20],
                'telpon' => $value[21],
            ]);
            Sekolah::create([
                'nisn' => $value[1],
                'nama_peserta' => $value[2],
                'nama_sekolah' => $value[10],
                'kecamatan_sekolah' => $value[11],
                'kota_sekolah' => $value[12],
                'provinsi_sekolah' => $value[13],
                'akreditasi_sekolah' => $value[14],
                'jurusan_asal' => $value[15],
            ]);
            Nilai::create([
                'nisn' => $value[1],
                'nama_peserta' => $value[2],
                'nama_sekolah' => $value[10],
                'nilai_rata' => $value[3],
                'nilai_prestasi' => $value[4],
                'nilai_jurusan' => $value[5],
                'nilai_rangking' => $value[6],
                'nilai_total_sebelum' => $value[7],
                'nilai_total_sesudah' => $value[8],
            ]);
            Pendaftaran::create([
                'nisn' => $value[1],
                'nama_peserta' => $value[2],
                'nomor_pendaftaran' => $value[0],
                'jurusan_asal' => $value[14],
                'kode_pil1' => $value[24],
                'pilihan1' => $value[25],
                'kode_pil2' => $value[26],
                'pilihan2' => $value[27],
                'kode_pil3' => $value[28],
                'pilihan3' => $value[29],
                'kode_jur' => $value[22],
                'jur_diterima' => $value[23],
                'daftar_bidikmisi' => $value[9],
            ]);
            // dd($value);
            // $checkdata = Siswa::where('nisn', $value->nisn)->get();
            // if(count($checkdata)>0){
            // }else{
            //     $siswa = new Siswa();
            //     $siswa->nisn = $value->nisn;
            //     $siswa->nama_peserta = $value->nama_peserta;
            //     $siswa->tanggal_lahir = $value->tanggal_lahir;
            //     $siswa->jenis_kelamin = $value -> jenis_kelamin;
            //     $siswa->alamat = $value->alamat;
            //     $siswa->kecamatan = $value->kecamatan;
            //     $siswa->kabupaten = $value->kabupaten;
            //     $siswa->provinsi = $value->provinsi;
            //     $siswa->telpon = (strln($value['telpon']>14)? '080000000000' : $value['telpon']);
            //     $siswa->save();
            //     $sekolah = new Sekolah();
            //     $sekolah->nisn = $value->nisn;
            //     $sekolah->nama_peserta = $value->nama_peserta;
            //     $sekolah->nama_sekolah = $value->nama_sekolah;
            //     $sekolah->provinsi_sekolah = $value->provinsi_sekolah;
            //     $sekolah->kota_sekolah = $value->kabupaten_kota_sekolah;
            //     $sekolah->akreditasi_sekolah = $value->akreditasi_sekolah;
            //     $sekolah->jurusan_asal = $value->jurusan_asal;
            //     $sekolah->save();
            //     $nilai = new Nilai();
            //     $nilai->nisn = $value->nisn;
            //     $nilai->nama_peserta = $value->nama_peserta;
            //     $nilai->nama_sekolah = $value->nama_sekolah;
            //     $nilai->nilai_rata = $value->nilai_ratarata_rapor;
            //     $nilai->nilai_prestasi = $value->nilai_prestasi;
            //     $nilai->nilai_jurusan = $value->nilai_jurusan;
            //     $nilai->nilai_rangking = $value->nilai_rangking;
            //     $nilai->nilai_total_sebelum = $value->nilai_total_sebelum_normalisasi;
            //     $nilai->nilai_total_sesudah = $value->nilai_total_sesudah_normalisasi;
            //     $nilai->save();
            //     $pendaftaran = new Pendaftaran();
            //     $pendaftaran->nisn = $value->nisn;
            //     $pendaftaran->nama_peserta = $value->nama_peserta;
            //     $pendaftaran->jurusan_asal = $value->jurusan_asal;
            //     $pendaftaran->kode_pil1 = $value->kode_pil1;
            //     $pendaftaran->pilihan1 = $value->pilihan_1;
            //     $pendaftaran->kode_pil2 = $value->kode_pil2;
            //     $pendaftaran->pilihan2 = $value->pilihan_2;
            //     $pendaftaran->kode_pil3 = $value->kode_pil3;
            //     $pendaftaran->pilihan3 = $value->pilihan_3;
            //     $pendaftaran->save();
            //     // Log::info($checkdata);
            // }
        }
    }
}
