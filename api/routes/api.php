<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
// use App\Http\Controllers\SiswaController;
// use App\Http\Controllers\SmkselectController;
// use App\Http\Controllers\NilaiController;
// use App\Http\Controllers\SekolahController;
// use App\Http\Controllers\PendaftaranController;

Route::get('importExport','DataController@importExport');
Route::get('downloadExcel/{type}','DataController@downloadExcel');
Route::post('importExcel',DataController::class . '@importExcel')->name('importExcel');
// Route::post('input',SiswaController::class . '@input')->name('input');
Route::post('input','SiswaImportController@input');

// Route::get('listsmk','SeleksiSmkController@getListSmk');
Route::post('seleksismk','SeleksiSmkController@seleksiSmk');
Route::post('smk','SeleksiSmkController@storeSeleksi');

Route::post('seleksi-nilai-smk','SmkSelectController@seleksiNilaiSmk');
Route::post('nilai-smk','SmkSelectController@storeNilaiSeleksi');

Route::post('seleksi-nilai-sma','SmaSelectController@seleksiNilaiSma');
Route::post('nilai-sma','SmaSelectController@storeNilaiSeleksi');

Route::post('seleksi-nilai-man','ManSelectController@seleksiNilaiMan');
Route::post('nilai-man','ManSelectController@storeNilaiSeleksi');


Route::get('selectall','SiswaImportController@selectAll');

Route::post('sumbar','DaerahSelectController@getSelectSumbar');
Route::post('luarsumbar','DaerahSelectController@getLuarSumbar');

Route::get('ti','ProdiController@getListTI');
Route::get('elektro','ProdiController@getListEc');
Route::get('sipil','ProdiController@getListSipil');
Route::get('mesin','ProdiController@getListMesin');
Route::get('akun','ProdiController@getListAkun');
Route::get('bi','ProdiController@getListBi');
Route::get('adm','ProdiController@getListAdm');
Route::get('allprodi','ProdiController@getListAll');


Route::post('prodi','ProdiSelectController@getSelectJurusan');

Route::get('dataprodi/{kode}','ProdiSelectController@listDataProdi');
Route::get('datajurusan1','ProdiSelectController@listDataJurusan1');
Route::get('datajurusan2','ProdiSelectController@listDataJurusan2');
Route::get('datajurusan3','ProdiSelectController@listDataJurusan3');
Route::get('datajurusan4','ProdiSelectController@listDataJurusan4');
Route::get('datajurusan5','ProdiSelectController@listDataJurusan5');
Route::get('datajurusan6','ProdiSelectController@listDataJurusan6');
Route::get('datajurusan7','ProdiSelectController@listDataJurusan7');







Route::get('datacadangan/{kode}','CadanganController@listDataCadangan');
Route::get('deletedata','NilaiController@deletedata');
Route::get('deletesmk','SmkSelectController@deleteSmk');
Route::get('deletesma','SmaSelectController@deleteSma');
Route::get('deleteman','ManSelectController@deleteMan');
Route::get('deleteprodi/{kode}','ProdiSelectController@deleteProdi');
Route::get('deletedaerah','DaerahSelectController@deleteDaerah');
Route::get('deletecadangan','CadanganController@deleteCadangan');

Route::resource('user', 'UserController');
Route::resource('kajur', 'KajurController');

Route::get('datauser/{email}','UserController@getData');



















// Route::get('smkb','SiswaController@seleksiSmk');
// Route::get('smkc','SiswaController@seleksiSmkc');
// Route::get('sma','SiswaController@seleksiSma');
// Route::get('smab','SiswaController@seleksiSmab');
// Route::get('smac','SiswaController@seleksiSmac');
// Route::get('man','SiswaController@seleksiMan');
// Route::get('manb','SiswaController@seleksiManb');
// Route::get('manc','SiswaController@seleksiManc');











/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group(['middleware' => ['auth:api']], function () {

});
Route::apiResources([
    '/siswa' => SiswaImportController::class,
    '/nilai' => NilaiController::class,
    '/sekolah' => SekolahController::class,
    '/pendaftaran' => PendaftaranController::class,
    '/manselect' => ManSelectController::class,
    '/smkselect' => SmkselectController::class,
    '/smaselect' => SmaSelectController::class,
    '/prodiselect' => ProdiSelectController::class,
    '/cadangan' => CadanganController::class,
    '/daerahselect' => DaerahSelectController::class,








]);
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();

});
