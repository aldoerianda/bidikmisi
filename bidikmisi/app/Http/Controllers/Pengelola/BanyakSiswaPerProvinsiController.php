<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\sekolah;
use Illuminate\Support\Facades\DB;
use PDF;

class BanyakSiswaPerProvinsiController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
    	$sekolahs = DB::table('sekolahs')
        ->select('prov_sklh', DB::raw('count(*) as total'))
        ->groupBy('prov_sklh')
        ->get();
        return view('pengelola.banyaksiswaperprovinsi',compact('sekolahs'));
    }
    public function show(Request $request)
    {
        $tahun = $request->tahun;
        $sekolahs = DB::table('sekolahs')
        ->select('prov_sklh', DB::raw('count(*) as total'))
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->groupBy('prov_sklh')
        ->get();
        $pdf = PDF::loadView('pengelola.banyaksiswaperprovinsishow', compact('sekolahs','tahun'));
        return $pdf->stream('banyaksiswaperprovinsi.pdf');
    }
}
