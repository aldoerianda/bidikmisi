<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\data_catatan;
use App\Model\pengelola\siswa;

class CatatanController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth:pengelola');
    }

    public function index()
    {
    	$data = data_catatan::all();
    	return view('pengelola.catatan.show', compact('data'));
    }
    public function create()
    {
    	$siswa = siswa::all();
        $data = data_catatan::all();
    	return view('pengelola.catatan.catatan', compact('data','siswa'));
    }
    public function store(Request $request)
    {
        $data = new data_catatan;
        $data->nisn=$request->nisn;
        $data->pemahaman1=$request->pemahaman1;
        $data->pemahaman2=$request->pemahaman2;
        $data->harapan1=$request->harapan1;
        $data->harapan2=$request->harapan2;
        $data->catatan=$request->catatan;
        $data->ijazah=$request->ijazah;
        $data->rapor=$request->rapor;
        $data->sertifikat=$request->sertifikat;
        $data->kk=$request->kk;
        $data->surat_peng=$request->surat_peng;
        $data->bpjs=$request->bpjs;
        $data->kis=$request->kis;
        $data->kip=$request->kip;
        $data->ksks=$request->ksks;
        $data->raskin=$request->raskin;
        $data->pbb=$request->pbb;
        $data->bukti_listrik=$request->bukti_listrik;
        $data->surat_data=$request->surat_data;
        $data->penilaian=$request->penilaian;
        $data->save();

        return redirect(route('catatan.index'));
    }
    public function edit($id)
    {
        $data=data_catatan::where('nisn',$id)->first();
        $siswa=siswa::all();
        return view('pengelola.catatan.edit',compact('data','siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = data_catatan::find($id);
        //$pengelolas->nidn=$request->nidn;
        //$pengelolas->username=$request->username;
        //$pengelolas->password=bcrypt($request->password);
        //$pengelolas->level=$request->level;
        $data->update($request->all());

        return redirect(route('catatan.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        data_catatan::where('nisn',$id)->delete();
        return redirect()->back();
    }
}
