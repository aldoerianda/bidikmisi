<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\skorfuzzy;
use Excel;
use App\Model\pengelola\visitasi;

class SkorfuzzyController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:pengelola');
	}
	public function index()
	{
		$fuzzy = skorfuzzy::all();
		return view('pengelola.fuzzy.show',compact('fuzzy'));
	}
	public function indexImport()
	{
		return view('pengelola/skorfuzzy');
	}
	public function Import(Request $request)
	{
		if($request->hasFile('file')){
			$path = $request->file('file')->getRealPath();
			Excel::selectSheetsByIndex(0)->filter('chunk')->load($path)->chunk(250, function($data){
				if(!empty($data) && $data->count()){
					foreach ($data as $key => $value) {
						$checkdata=skorfuzzy::where('nisn', $value->nisn)->get();
						if (count($checkdata)>0) {
						} else{
							$skor = new skorfuzzy();
							$skor->nisn = $value->nisn;
							$skor->nama = $value->nama;
							$skor->alamat = $value->alamat;
							$skor->income = $value->income;
							$skor->dependent = $value->dependent;
							$skor->outeconomic = $value->outeconomic;
							$skor->assessment = $value->assessment;
							$skor->outacademic = $value->outacademic;
							$skor->homeower = $value->homeower;
							$skor->sanitation = $value->sanitation;
							$skor->marital = $value->marital;
							$skor->sourcewater = $value->sourcewater;
							$skor->outhousehold = $value->outhousehold;
							$skor->land = $value->land;
							$skor->house = $value->house;
							$skor->outarea = $value->outarea;
							$skor->outdecision = $value->outdecision;
							$skor->time = $value->time;
							$tgl = ($value->tgl_skrg - 25569) * 86400;
							$skor->tgl_skrg = gmdate("Y-m-d H:i:s", $tgl);
							$skor->durasi = $value->durasi;
							$skor->save();
						}
					}
				}
			});
		}
		return back();
	}
	public function kuota(Request $request)
	{
		$tahun = (int)date('Y');
		$fuzzy = skorfuzzy::select('nisn')->orderBy('outdecision','desc')->skip(0)->take($request->kuota)->whereYear('tgl_skrg',$tahun)->get();
		foreach ($fuzzy as $key => $f) {
			visitasi::insert(array('nisn'=>$f->nisn));
		}
		
		return redirect(route('visitasi.index'));
	}
}
