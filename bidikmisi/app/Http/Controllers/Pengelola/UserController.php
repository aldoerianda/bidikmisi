<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\pengelola;
use App\Model\pengelola\dosen;

class UserController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth:pengelola');
    }
    public function index()
    {
    	$pengelolas = pengelola::all();
    	return view('pengelola.user.show', compact('pengelolas'));
    }
    public function create()
    {
        $data = dosen::all();
    	return view('pengelola.user.user', compact('data'));
    }
    public function store(Request $request)
    {
        $pengelolas = new pengelola;
        $pengelolas->nidn=$request->nidn;
        $pengelolas->username=$request->username;
        $pengelolas->password=bcrypt($request->password);
        $pengelolas->level=$request->level;
        $pengelolas->save();

        return redirect(route('user.index'));
    }
    public function edit($id)
    {
        $pengelola=pengelola::where('id',$id)->first();
        $dosen=dosen::all();
        return view('pengelola.user.edit',compact('pengelola','dosen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pengelolas = pengelola::find($id);
        $pengelolas->nidn=$request->nidn;
        $pengelolas->username=$request->username;
        $pengelolas->password=bcrypt($request->password);
        $pengelolas->level=$request->level;
        $pengelolas->save();

        return redirect(route('user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        pengelola::where('id',$id)->delete();
        return redirect()->back();
    }
}
