<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\berita;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $beritas = berita::all();
        return view('pengelola.berita.show',compact('beritas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('pengelola.berita.berita');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'judul'=>'required',
            'isi'=>'required',
            ]);
        $berita = new berita;
        $berita->judul=$request->judul;
        $berita->isi=$request->isi;
        $berita->slug=str_slug($request->judul);
        $berita->save();

        return redirect(route('berita.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $berita=berita::where('id',$id)->first();

        return view('pengelola.berita.edit',compact('berita'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'judul'=>'required',
            'isi'=>'required',
            ]);

         $berita = berita::find($id);
        $berita->judul=$request->judul;
        $berita->isi=$request->isi;
        $berita->slug=str_slug($request->judul);
        $berita->save();

        return redirect(route('berita.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        berita::where('id',$id)->delete();
        return redirect()->back();
    }
}
