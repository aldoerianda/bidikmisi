<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\dosen;
use Excel;

class DosenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $dosens = dosen::all();
        return view('pengelola.dosen.show',compact('dosens'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('pengelola.dosen.dosen');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //$this->validate($request,[
        //    'nidn'=>'required',
        //    'nama_dosen'=>'required',
        //    'tempat_lahir'=>'required',
        //    'tgl_lahir'=>'required',
        //    'jekel'=>'required',
            // 'no_hp'=>'required',
            // 'alamat'=>'required',
        //    ]);
        $dosen = new dosen;
        $dosen->nidn=$request->nidn;
        $dosen->nama_dosen=$request->nama_dosen;
        $dosen->tempat_lahir=$request->tempat_lahir;
        $dosen->tgl_lahir=$request->tgl_lahir;
        $dosen->jekel=$request->jekel;
        $dosen->no_hp=$request->no_hp;
        $dosen->alamat=$request->alamat;
        $dosen->save();

        return redirect(route('dosen.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dosen=dosen::where('nidn',$id)->first();

        return view('pengelola.dosen.edit',compact('dosen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //$this->validate($request,[
        //    'nidn'=>'required',
        //    'nama_dosen'=>'required',
        //    'tempat_lahir'=>'required',
        //    'tgl_lahir'=>'required',
        //    'jekel'=>'required',
            // 'no_hp'=>'required',
            // 'alamat'=>'required',
        //    ]);

         $dosen = dosen::find($id);
        $dosen->nidn=$request->nidn;
        $dosen->nama_dosen=$request->nama_dosen;
        $dosen->tempat_lahir=$request->tempat_lahir;
        $dosen->tgl_lahir=$request->tgl_lahir;
        $dosen->jekel=$request->jekel;
        $dosen->no_hp=$request->no_hp;
        $dosen->alamat=$request->alamat;
        $dosen->save();

        return redirect(route('dosen.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dosen::where('nidn',$id)->delete();
        return redirect()->back();
    }
    public function Import(Request $request)
    {
        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();
            Excel::filter('chunk')->load($path)->chunk(250, function($data){
                if (!empty($data) && $data->count()) {
                    foreach ($data as $key => $value) {
                        $checkdata=dosen::where('nidn', $value->nidn)->get();
                        if(count($checkdata)>0){
                        }else{
                            $dosen = new dosen();
                            $dosen->nidn = $value->nidn;
                            $dosen->nama_dosen = $value->nama_dosen;
                            $dosen->tempat_lahir = $value->tempat_lahir;
                            $tgl = ($value->tanggal_lahir - 25569) * 86400;
                            $dosen->tgl_lahir = gmdate("Y-m-d H:i:s", $tgl);
                            $dosen->jekel = $value->jenis_kelamin;
                            $dosen->no_hp = $value->no_hp;
                            $dosen->alamat = $value->alamat;
                            $dosen->save();
                        }
                    }
                }
            });
        }
        return back();
    }
    public function Export()
    {
        $data = dosen::all('nidn','nama_dosen','tempat_lahir','tgl_lahir','jekel','no_hp','alamat');
        $Array[] =[];
        $Array[] = ['NIDN', 'Nama Dosen', 'Tempat Lahir', 'Tanggal Lahir', 'Jenis Kelamin', 'No HP','Alamat'];
        foreach ($data as $key => $value) {
            $Array[] = $value->toArray();
        }

        Excel::create('DataDosen', function($excel) use ($Array) {
            $excel->sheet('sheet1', function($sheet) use ($Array) {
                $sheet->fromArray($Array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }
}
