<?php

namespace App\Http\Controllers\Pengelola;

use App\Http\Controllers\Controller;
use App\Model\pengelola\sekolah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SekolahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $sekolahs = sekolah::all();
        return view('pengelola.sekolah.show',compact('sekolahs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('pengelola.sekolah.sekolah');
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nisn'=>'required',
            'npsn'=>'required',
            'nama_sklh'=>'required',
            'prov_sklh'=>'required',
            'kota_sklh'=>'required',
            'tgl_daftar'=>'required',
        ]);

        $sekolah = new sekolah;
        $sekolah->nisn=$request->nisn;
        $sekolah->npsn=$request->npsn;
        $sekolah->nama_sklh=$request->nama_sklh;
        $sekolah->prov_sklh=$request->prov_sklh;
        $sekolah->kota_sklh=$request->kota_sklh;
        $sekolah->tgl_daftar=$request->tgl_daftar;
        $sekolah->save();

        return redirect(route('sekolah.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $sekolahs = DB::table('sekolahs')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        return view('pengelola.sekolah.pertahun',compact('sekolahs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sekolah=sekolah::where('nisn',$id)->first();

        return view('pengelola.sekolah.edit',compact('sekolah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
         'nisn'=>'required',
         'npsn'=>'required',
         'nama_sklh'=>'required',
         'prov_sklh'=>'required',
         'kota_sklh'=>'required',
         'tgl_daftar'=>'required',
     ]);

        $sekolah = sekolah::find($id);
        $sekolah->nisn=$request->nisn;
        $sekolah->npsn=$request->npsn;
        $sekolah->nama_sklh=$request->nama_sklh;
        $sekolah->prov_sklh=$request->prov_sklh;
        $sekolah->kota_sklh=$request->kota_sklh;
        $sekolah->tgl_daftar=$request->tgl_daftar;
        $sekolah->save();

        return redirect(route('sekolah.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        sekolah::where('nisn',$id)->delete();
        return redirect()->back();
    }
}
