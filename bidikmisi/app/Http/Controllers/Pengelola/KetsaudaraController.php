<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\dataket_sau;
use App\Model\pengelola\siswa;

class KetsaudaraController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth:pengelola');
    }

    public function index()
    {
    	$dataketsau = dataket_sau::all();
    	return view('pengelola.dataketsau.show', compact('dataketsau'));
    }
    public function create()
    {
    	$siswa = siswa::all();
        $dataketsau = dataket_sau::all();
    	return view('pengelola.dataketsau.dataketsau', compact('dataketsau','siswa'));
    }
    public function store(Request $request)
    {
        $dataketsau = new dataket_sau;
        $dataketsau->nisn=$request->nisn;
        $dataketsau->nama_sau=$request->nama_sau;
        $dataketsau->umur_sau=$request->umur_sau;
        $dataketsau->pekerjaan_sau=$request->pekerjaan_sau;
        $dataketsau->status_hub=$request->status_hub;
        $dataketsau->save();

        return redirect(route('dataketsau.index'));
    }
    public function edit($id)
    {
        $dataketsau=dataket_sau::where('id_ket',$id)->first();
        $siswa=siswa::all();
        return view('pengelola.dataketsau.edit',compact('dataketsau','siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataketsau = dataket_sau::find($id);
        //$pengelolas->nidn=$request->nidn;
        //$pengelolas->username=$request->username;
        //$pengelolas->password=bcrypt($request->password);
        //$pengelolas->level=$request->level;
        $dataketsau->update($request->all());

        return redirect(route('dataketsau.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dataket_sau::where('id_ket',$id)->delete();
        return redirect()->back();
    }
}
