<?php

namespace App\Http\Controllers\Pengelola;

use App\Http\Controllers\Controller;
use App\Model\pengelola\nilai;
use App\Model\pengelola\pendaftaran;
use App\Model\pengelola\penghasilan_ortu;
use App\Model\pengelola\rumah;
use App\Model\pengelola\sekolah;
use App\Model\pengelola\siswa;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
      $this->middleware('auth:pengelola');
      set_time_limit(0);
    }

    public function index()
    {
      $siswas = siswa::all();
      return view('pengelola.siswa.show',compact('siswas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     return view('pengelola.siswa.siswa');
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request,[
        'nisn'=>'required',
        'nama_siswa'=>'required',
        'tempat_lahir'=>'required',
        'tgl_lahir'=>'required',
        'jekel'=>'required',
            // 'no_hp'=>'required',
            // 'no_telp'=>'required',
        'alamat'=>'required',
        'tgl_daftar'=>'required',
      ]);
      $siswa = new siswa;
      $siswa->nisn=$request->nisn;
      $siswa->nama_siswa=$request->nama_siswa;
      $siswa->tempat_lahir=$request->tempat_lahir;
      $siswa->tgl_lahir=$request->tgl_lahir;
      $siswa->jekel=$request->jekel;
      $siswa->no_hp=$request->no_hp;
      $siswa->no_telp=$request->no_telp;
      $siswa->alamat=$request->alamat;
      $siswa->tgl_daftar=$request->tgl_daftar;
      $siswa->save();
      return redirect(route('siswa.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
      $siswas = DB::table('siswas')
      ->whereYear('tgl_daftar', '=', $request->tahun)
      ->get();
      return view('pengelola.siswa.pertahun',compact('siswas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $siswa=siswa::where('nisn',$id)->first();

      return view('pengelola.siswa.edit',compact('siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response*/
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'nisn'=>'required',
        'nama_siswa'=>'required',
        'tempat_lahir'=>'required',
        'tgl_lahir'=>'required',
        'jekel'=>'required',
            // 'no_hp'=>'required',
            // 'no_telp'=>'required',
        'alamat'=>'required',
        'tgl_daftar'=>'required',
      ]);
      $siswa = siswa::find($id);
      $siswa->nisn=$request->nisn;
      $siswa->nama_siswa=$request->nama_siswa;
      $siswa->tempat_lahir=$request->tempat_lahir;
      $siswa->tgl_lahir=$request->tgl_lahir;
      $siswa->jekel=$request->jekel;
      $siswa->no_hp=$request->no_hp;
      $siswa->no_telp=$request->no_telp;
      $siswa->alamat=$request->alamat;
      $siswa->tgl_daftar=$request->tgl_daftar;
      $siswa->save();
      return redirect(route('siswa.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      siswa::where('nisn',$id)->delete();
      return redirect()->back();
    }

    public function siswaImport(Request $request){
      if($request->hasFile('file')){
        $path = $request->file('file')->getRealPath();
        Excel::filter('chunk')->load($path)->chunk(250, function($data){
          if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
              $checkdata=siswa::where('nisn', $value->nisn)->get();
              if (count($checkdata)>0) {
              } else{
                $siswa = new siswa();
                $siswa->nisn = $value->nisn;
                $siswa->nama_siswa = $value->nama_siswa;
                $siswa->tempat_lahir = $value->tempat_lahir;
                $lahir = ($value->tanggal_lahir - 25569) * 86400;
                $siswa->tgl_lahir = gmdate("Y-m-d H:i:s", $lahir);
                $siswa->jekel = $value->jenis_kelamin;
                $siswa->no_hp = $value['no._handphone'];
                $siswa->no_telp = $value['no._telepon'];
                $siswa->alamat = $value->alamat;
                $daftar = ($value->tanggal_daftar - 25569) * 86400;
                $siswa->tgl_daftar = gmdate("Y-m-d H:i:s",$daftar);
                $siswa->save();
                $sekolah= new sekolah();
                $sekolah->nisn=$value->nisn;
                $sekolah->npsn=$value->npsn;
                $sekolah->nama_sklh=$value->asal_sekolah;
                $sekolah->prov_sklh=$value->provinsi_sekolah;
                $sekolah->kota_sklh=$value->kota_sekolah;
                $sekolah->tgl_daftar = gmdate("Y-m-d H:i:s", $daftar);
                $sekolah->save();
                $nilai = new nilai();
                $nilai->nisn = $value->nisn;
                $nilai->nama_siswa=$value->nama_siswa;
                $nilai->nama_sklh=$value->asal_sekolah;
                $nilai->rang_smst_4 = $value->rangking_smt_4;
                $nilai->ttl_nilai_smst_4 = $value->total_nilai_smt_4;
                $nilai->jmlh_mapel_smst_4 = $value->jumlah_mapel_smt_4;
                $nilai->rang_smst_5 = $value->rangking_smt_5;
                $nilai->ttl_nilai_smst_5 = $value->total_nilai_smt_5;
                $nilai->jmlh_mapel_smst_5 = $value->jumlah_mapel_smt_5;
                $nilai->rang_smst_6 = $value->rangking_smt_6;
                $nilai->ttl_nilai_smst_6 = $value->total_nilai_smt_6;
                $nilai->jmlh_mapel_smst_6 = $value->jumlah_mapel_smt_6;
                $nilai->nilai_unas = $value->nilai_unas;
                $nilai->jmlh_mapel_unas = $value->jumlah_mapel_unas;
                $nilai->pres_ekskul = $value->prestasi_ekstrakurikuler;
                $nilai->tgl_daftar = gmdate("Y-m-d H:i:s", $daftar);
                $nilai->save();
                $pendaftaran = new pendaftaran();
                $pendaftaran->no_pend = $value['no._pendaftaran'];
                $pendaftaran->nisn = $value->nisn;
                $pendaftaran->tgl_daftar = gmdate("Y-m-d H:i:s", $daftar);
                $pendaftaran->jenis_seleksi = $value->jenis_seleksi;
                $pendaftaran->jur_sekolah = $value->jurusan_di_sekolah;
                $pendaftaran->pil_prodi_1 = $value->pilihan_prodi_1;
                $pendaftaran->pil_prodi_2 = $value->pilihan_prodi_2;
                $pendaftaran->save();
                $penghasilan_ortu = new penghasilan_ortu();
                $penghasilan_ortu->nisn = $value->nisn;
                $penghasilan_ortu->nama_ibu_kdg = $value->nama_ibu_kandung;
                $penghasilan_ortu->status_ayah = $value->status_ayah;
                $penghasilan_ortu->status_ibu = $value->status_ibu;
                $penghasilan_ortu->pek_ayah = $value->pekerjaan_ayah;
                $penghasilan_ortu->pek_ibu = $value->pekerjaan_ibu;
                $penghasilan_ortu->peng_ayah = $value->penghasilan_ayah;
                $penghasilan_ortu->peng_ibu = $value->penghasilan_ibu;
                $penghasilan_ortu->jmlh_tanggungan = $value->jumlah_tanggungan;
                $penghasilan_ortu->tgl_daftar = gmdate("Y-m-d H:i:s", $daftar);
                $penghasilan_ortu->save();
                $rumah = new rumah();
                $rumah->nisn = $value->nisn;
                $rumah->kepemilikan = $value->kepemilikan_rumah;
                $rumah->thn_peroleh = $value->tahun_perolehan;
                $rumah->sumber_listrik = $value->sumber_listrik;
                $rumah->luas_tanah = $value->luas_tanah;
                $rumah->luas_bangunan = $value->luas_bangunan;
                $rumah->sumber_air = $value->sumber_air;
                $rumah->mck = $value->mck;
                $rumah->jrk_pusat_kota = $value->jarak_pusat_kota;
                $rumah->tgl_daftar = gmdate("Y-m-d H:i:s", $daftar);
                $rumah->save();
              }
            }
          }
        });
}
return back();
}

public function siswaExport(Request $request) {
  $ds = siswa::join('sekolahs', 'sekolahs.nisn', '=', 'siswas.nisn')
  -> join('penghasilan_ortus', 'penghasilan_ortus.nisn', '=', 'siswas.nisn')
  -> join('rumahs', 'rumahs.nisn', '=', 'siswas.nisn')
  -> join('nilais', 'nilais.nisn', '=', 'siswas.nisn')
  ->select(
    'siswas.nisn', 
    'siswas.nama_siswa', 
    'sekolahs.nama_sklh',
    'sekolahs.prov_sklh',
    'penghasilan_ortus.status_ayah',
    'penghasilan_ortus.status_ibu',
    'penghasilan_ortus.income',
    'penghasilan_ortus.jmlh_tanggungan',
    DB::raw("concat(((nilais.ttl_nilai_smst_4/nilais.jmlh_mapel_smst_4)+(nilais.ttl_nilai_smst_5/nilais.jmlh_mapel_smst_5))/2) as 'rata'"),
    'rumahs.kepemilikan',
    'rumahs.luas_tanah',
    'rumahs.luas_bangunan',
    'rumahs.sumber_air',
    'rumahs.sumber_listrik',  
    'rumahs.mck',
    'nilais.jml_prestasi')
  ->whereYear('siswas.tgl_daftar', '=', $request->tahun)
  ->get();
  $Array = []; 
  $Array[] = ['NISN', 'Nama Siswa','Asal Sekolah', 'Provinsi Sekolah', 'Status Ayah', 'Status Ibu','Income', 'Jumlah Tanggungan', 'Total Nilai', 'Kepemilikan', 'Luas Tanah', 'Luas Bangunan', 'Sumber Air', 'Sumber Listrik', 'MCK', 'Jumlah Prestasi'];
    // Convert each member of the returned collection into an array,
    // and append it to the payments array.
  foreach ($ds as $data) {
    $Array[] = $data->toArray();
  }

    // Generate and return the spreadsheet
  Excel::create('Bidikmisi', function($excel) use ($Array) {

        // Build the spreadsheet, passing in the payments array
    $excel->sheet('sheet1', function($sheet) use ($Array) {
      $sheet->fromArray($Array, null, 'A1', false, false);
    });

  })->download('xlsx');
}
}
