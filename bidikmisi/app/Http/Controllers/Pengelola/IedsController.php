<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IedsController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
    	return view('pengelola/ieds');
    }
}