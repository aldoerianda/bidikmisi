<?php

namespace App\Http\Controllers\Pengelola;

use App\Http\Controllers\Controller;
use App\Model\pengelola\rumah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RumahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $rumahs = rumah::all();
        return view('pengelola.rumah.show',compact('rumahs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('pengelola.rumah.rumah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nisn'=>'required',
            'kepemilikan'=>'required',
            'thn_peroleh'=>'required',
            'sumber_listrik'=>'required',
            'luas_tanah'=>'required',
            'luas_bangunan'=>'required',
            'sumber_air'=>'required',
            'mck'=>'required',
            'jrk_pusat_kota'=>'required',
            'tgl_daftar'=>'required',
            ]);

        $rumah = new rumah;
        $rumah->nisn=$request->nisn;
        $rumah->kepemilikan=$request->kepemilikan;
        $rumah->thn_peroleh=$request->thn_peroleh;
        $rumah->sumber_listrik=$request->sumber_listrik;
        $rumah->luas_tanah=$request->luas_tanah;
        $rumah->luas_bangunan=$request->luas_bangunan;
        $rumah->sumber_air=$request->sumber_air;
        $rumah->mck=$request->mck;
        $rumah->jrk_pusat_kota=$request->jrk_pusat_kota;
        $rumah->tgl_daftar=$request->tgl_daftar;
        $rumah->save();

        return redirect(route('rumah.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rumahs = DB::table('rumahs')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        return view('pengelola.rumah.pertahun',compact('rumahs'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rumah=rumah::where('nisn',$id)->first();

        return view('pengelola.rumah.edit',compact('rumah'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'nisn'=>'required',
            'kepemilikan'=>'required',
            'thn_peroleh'=>'required',
            'sumber_listrik'=>'required',
            'luas_tanah'=>'required',
            'luas_bangunan'=>'required',
            'sumber_air'=>'required',
            'mck'=>'required',
            'jrk_pusat_kota'=>'required',
            'tgl_daftar'=>'required',
            ]);

         $rumah = rumah::find($id);
         $rumah->nisn=$request->nisn;
        $rumah->kepemilikan=$request->kepemilikan;
        $rumah->thn_peroleh=$request->thn_peroleh;
        $rumah->sumber_listrik=$request->sumber_listrik;
        $rumah->luas_tanah=$request->luas_tanah;
        $rumah->luas_bangunan=$request->luas_bangunan;
        $rumah->sumber_air=$request->sumber_air;
        $rumah->mck=$request->mck;
        $rumah->jrk_pusat_kota=$request->jrk_pusat_kota;
        $rumah->tgl_daftar=$request->tgl_daftar;
        $rumah->save();

        return redirect(route('rumah.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        rumah::where('nisn',$id)->delete();
        return redirect()->back();
    }
}
