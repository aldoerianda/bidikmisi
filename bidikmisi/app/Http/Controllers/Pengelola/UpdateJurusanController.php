<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\pendaftaran;
use Illuminate\Support\Facades\DB;

class UpdateJurusanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $pendaftarans = DB::select( DB::raw("SELECT jur_sekolah,count(jur_sekolah) as jumlah FROM pendaftarans group by jur_sekolah"));
    	//$pendaftarans = pendaftaran::select('jur_sekolah')->groupBy('jur_sekolah')->get();
    	return view('pengelola.jurusan.show', compact('pendaftarans'));
    }

    public function edit($id)
    {
    	$pendaftaran=pendaftaran::where('jur_sekolah',$id)->first();
        return view('pengelola.jurusan.edit',compact('pendaftaran'));
    }

    public function update($id, Request $request)
    {
    	$pendaftaran = pendaftaran::where('jur_sekolah',$id)->update(['jur_sekolah'=>$request->jur_sekolah]);
        return redirect(route('jurusan.index'));
    }
}
