<?php

namespace App\Http\Controllers\Pengelola;

use App\Http\Controllers\Controller;
use App\Model\pengelola\penghasilan_ortu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Penghasilan_OrtuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:pengelola');
        set_time_limit(0);
    }
    
    public function index()
    {
        $penghasilan_ortus = penghasilan_ortu::all();

        $dataKonversiAyah = [];
        $dataKonversiIbu= [];
        $i = 0;
        foreach ($penghasilan_ortus as $data) {
            switch ($data->peng_ayah) {
                case '< Rp. 250.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case '> Rp. 10.000.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 10250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 1.000.001 - Rp. 1.250.000':
                    $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 1250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 1.250.001 - Rp. 1.500.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 1500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 1.500.001 - Rp. 1.750.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 1750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 1.750.001 - Rp. 2.000.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 2000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 2.000.001 - Rp. 2.250.000':
                    $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 2250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 2.250.001 - Rp. 2.500.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 2500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 2.500.001 - Rp. 2.750.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 2750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 2.750.001 - Rp. 3.000.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 250.001 - Rp. 500.000':
                    $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 3.000.001 - Rp. 3.250.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 3.250.001 - Rp. 3.500.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 3.500.001 - Rp. 3.750.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 3.750.001 - Rp. 4.000.000':
                    $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 4000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 4.000.001 - Rp. 4.250.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 4250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 4.250.001 - Rp. 4.500.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 4500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 4.500.001 - Rp. 4.750.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 4750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 4.750.001 - Rp. 5.000.000':
                    $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 5000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 5.000.001 - Rp. 5.250.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 5250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 5.250.001 - Rp. 5.500.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 5500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 5.500.001 - Rp. 5.750.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 5750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 5.750.001 - Rp. 6.000.000':
                    $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 6000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 500.001 - Rp. 750.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 6.000.001 - Rp. 6.250.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 6250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 6.250.001 - Rp. 6.500.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 6500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 6.500.001 - Rp. 6.750.000':
                    $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 7000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 6.750.001 - Rp. 7.000.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 6750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                 case 'Rp. 7.000.001 - Rp. 7.250.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 7250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 7.250.001 - Rp. 7.500.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 7500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 7.500.001 - Rp. 7.750.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 7750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 7.750.001 - Rp. 8.000.000':
                    $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 8000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 750.001 - Rp. 1.000.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 1000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 8.000.001 - Rp. 8.250.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 8250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 8.250.001 - Rp. 8.500.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 8500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 8.500.001 - Rp. 8.750.000':
                    $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 8750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 8.750.001 - Rp. 9.000.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 9000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 9.000.001 - Rp. 9.250.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 9250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 9.250.001 - Rp. 9.500.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 9500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 9.500.001 - Rp. 9.750.000':
                    $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 9750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Rp. 9.750.000 - Rp. 10.000.000':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 10000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                case 'Tnisnak Berpenghasilan':
                    $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 0 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
                default:
                    $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 0 , 'tanggungan' => $data->jmlh_tanggungan]);
                    break;
            }
            switch ($data->peng_ibu) {
                case '< Rp. 250.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 250000]);
                    break;
                case '> Rp. 10.000.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 10250000]);
                    break;
                case 'Rp. 1.000.001 - Rp. 1.250.000':
                    $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 1250000]);
                    break;
                case 'Rp. 1.250.001 - Rp. 1.500.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 1500000]);
                    break;
                case 'Rp. 1.500.001 - Rp. 1.750.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 1750000]);
                    break;
                case 'Rp. 1.750.001 - Rp. 2.000.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 2000000]);
                    break;
                case 'Rp. 2.000.001 - Rp. 2.250.000':
                    $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 2250000]);
                    break;
                case 'Rp. 2.250.001 - Rp. 2.500.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 2500000]);
                    break;
                case 'Rp. 2.500.001 - Rp. 2.750.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 2750000]);
                    break;
                case 'Rp. 2.750.001 - Rp. 3.000.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3000000]);
                    break;
                case 'Rp. 250.001 - Rp. 500.000':
                    $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 500000]);
                    break;
                case 'Rp. 3.000.001 - Rp. 3.250.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3250000]);
                    break;
                case 'Rp. 3.250.001 - Rp. 3.500.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3500000]);
                    break;
                case 'Rp. 3.500.001 - Rp. 3.750.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3750000]);
                    break;
                case 'Rp. 3.750.001 - Rp. 4.000.000':
                    $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 4000000]);
                    break;
                case 'Rp. 4.000.001 - Rp. 4.250.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 4250000]);
                    break;
                case 'Rp. 4.250.001 - Rp. 4.500.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 4500000]);
                    break;
                case 'Rp. 4.500.001 - Rp. 4.750.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 4750000]);
                    break;
                case 'Rp. 4.750.001 - Rp. 5.000.000':
                    $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 5000000]);
                    break;
                case 'Rp. 5.000.001 - Rp. 5.250.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 5250000]);
                    break;
                case 'Rp. 5.250.001 - Rp. 5.500.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 5500000]);
                    break;
                case 'Rp. 5.500.001 - Rp. 5.750.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 5750000]);
                    break;
                case 'Rp. 5.750.001 - Rp. 6.000.000':
                    $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 6000000]);
                    break;
                case 'Rp. 500.001 - Rp. 750.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 750000]);
                    break;
                case 'Rp. 6.000.001 - Rp. 6.250.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 6250000]);
                    break;
                case 'Rp. 6.250.001 - Rp. 6.500.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 6500000]);
                    break;
                case 'Rp. 6.500.001 - Rp. 6.750.000':
                    $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 7000000]);
                    break;
                case 'Rp. 6.750.001 - Rp. 7.000.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 6750000]);
                    break;
                 case 'Rp. 7.000.001 - Rp. 7.250.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 7250000]);
                    break;
                case 'Rp. 7.250.001 - Rp. 7.500.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 7500000]);
                    break;
                case 'Rp. 7.500.001 - Rp. 7.750.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 7750000]);
                    break;
                case 'Rp. 7.750.001 - Rp. 8.000.000':
                    $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 8000000]);
                    break;
                case 'Rp. 750.001 - Rp. 1.000.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 1000000]);
                    break;
                case 'Rp. 8.000.001 - Rp. 8.250.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 8250000]);
                    break;
                case 'Rp. 8.250.001 - Rp. 8.500.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 8500000]);
                    break;
                case 'Rp. 8.500.001 - Rp. 8.750.000':
                    $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 8750000]);
                    break;
                case 'Rp. 8.750.001 - Rp. 9.000.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 9000000]);
                    break;
                case 'Rp. 9.000.001 - Rp. 9.250.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 9250000]);
                    break;
                case 'Rp. 9.250.001 - Rp. 9.500.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 9500000]);
                    break;
                case 'Rp. 9.500.001 - Rp. 9.750.000':
                    $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 9750000]);
                    break;
                case 'Rp. 9.750.000 - Rp. 10.000.000':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 10000000]);
                    break;
                case 'Tnisnak Berpenghasilan':
                    $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 0]);
                    break;
                default:
                    $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 0]);
                    break;
            }
            $i++;
        }
        $income= [];
        $ratabiaya=[];
                for ($i =0; $i    <  count($dataKonversiAyah); $i++) { 
                    $income[$i] = json_decode($dataKonversiAyah[$i])->value + json_decode($dataKonversiIbu[$i])->value;
                    if(json_decode($dataKonversiAyah[$i])->tanggungan==0){
                        $ratabiaya[$i] = $income[$i];
                    }
                    else{
                        $ratabiaya[$i] = $income[$i] / json_decode($dataKonversiAyah[$i])->tanggungan;
                    }
                    
                }
                for ($r=0; $r < count($income); $r++) { 
                    penghasilan_ortu::find(json_decode($dataKonversiAyah[$r])->nisn)->update([
                            'income' => $income[$r],
                            'rata_biaya' => $ratabiaya[$r] 
                    ]);
                }
            
        return view('pengelola.penghasilan_ortu.show',compact('penghasilan_ortus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('pengelola.penghasilan_ortu.penghasilan_ortu');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nisn'=>'required',
            'nama_ibu_kdg'=>'required',
            'status_ayah'=>'required',
            'status_ibu'=>'required',
            'pek_ayah'=>'required',
            'pek_ibu'=>'required',
            'peng_ayah'=>'required',
            'peng_ibu'=>'required',
            'jmlh_tanggungan'=>'required',
            'tgl_daftar'=>'required',
            ]);

        $penghasilan_ortu = new penghasilan_ortu;
        $penghasilan_ortu->nisn=$request->nisn;
        $penghasilan_ortu->nama_ibu_kdg=$request->nama_ibu_kdg;
        $penghasilan_ortu->status_ayah=$request->status_ayah;
        $penghasilan_ortu->status_ibu=$request->status_ibu;
        $penghasilan_ortu->pek_ayah=$request->pek_ayah;
        $penghasilan_ortu->pek_ibu=$request->pek_ibu;
        $penghasilan_ortu->peng_ayah=$request->peng_ayah;
        $penghasilan_ortu->peng_ibu=$request->peng_ibu;
        $penghasilan_ortu->jmlh_tanggungan=$request->jmlh_tanggungan;
        $penghasilan_ortu->tgl_daftar=$request->tgl_daftar;
        $penghasilan_ortu->save();

        return redirect(route('penghasilan_ortu.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(Request $request)
    {
        $penghasilan_ortus = DB::table('penghasilan_ortus')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        return view('pengelola.penghasilan_ortu.pertahun',compact('penghasilan_ortus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $penghasilan_ortu=penghasilan_ortu::where('nisn',$id)->first();

        return view('pengelola.penghasilan_ortu.edit',compact('penghasilan_ortu'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'nisn'=>'required',
            'nama_ibu_kdg'=>'required',
            'status_ayah'=>'required',
            'status_ibu'=>'required',
            'pek_ayah'=>'required',
            'pek_ibu'=>'required',
            'peng_ayah'=>'required',
            'peng_ibu'=>'required',
            'jmlh_tanggungan'=>'required',
            'tgl_daftar'=>'required',
            ]);

         $penghasilan_ortu = penghasilan_ortu::find($id);
        $penghasilan_ortu->nisn=$request->nisn;
        $penghasilan_ortu->nama_ibu_kdg=$request->nama_ibu_kdg;
        $penghasilan_ortu->status_ayah=$request->status_ayah;
        $penghasilan_ortu->status_ibu=$request->status_ibu;
        $penghasilan_ortu->pek_ayah=$request->pek_ayah;
        $penghasilan_ortu->pek_ibu=$request->pek_ibu;
        $penghasilan_ortu->peng_ayah=$request->peng_ayah;
        $penghasilan_ortu->peng_ibu=$request->peng_ibu;
        $penghasilan_ortu->jmlh_tanggungan=$request->jmlh_tanggungan;
        $penghasilan_ortu->tgl_daftar=$request->tgl_daftar;
        $penghasilan_ortu->save();

        return redirect(route('penghasilan_ortu.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        penghasilan_ortu::where('nisn',$id)->delete();
        return redirect()->back();
    }



}
