<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\nilai;
use PDF;

class PrestasiEkskulController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $nilais = nilai::where('pres_ekskul','!=', '-')->get();
        return view('pengelola.prestasiekskul',compact('nilais'));
    }
    public function show(Request $request)
    {
        $tahun = $request->tahun;
        $nilais = nilai::where('pres_ekskul','!=', '-')
        ->whereYear('tgl_daftar', '=', $request->tahun)
		->get();
        $pdf = PDF::loadView('pengelola.prestasiekskulshow', compact('nilais','tahun'));
        return $pdf->stream('prestasiekskul.pdf');
    }
}
