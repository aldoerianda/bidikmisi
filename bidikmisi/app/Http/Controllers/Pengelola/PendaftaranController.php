<?php

namespace App\Http\Controllers\Pengelola;

use App\Http\Controllers\Controller;
use App\Model\pengelola\pendaftaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PendaftaranController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $pendaftarans = pendaftaran::all();
        return view('pengelola.pendaftaran.show',compact('pendaftarans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('pengelola.pendaftaran.pendaftaran');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'no_pend'=>'required',
            'nisn'=>'required',
            // 'tgl_daftar'=>'required',
            'jenis_seleksi'=>'required',
            'jur_sekolah'=>'required',
            'pil_prodi_1'=>'required',
            'pil_prodi_2'=>'required',
            ]);
        $pendaftaran = new pendaftaran;
        $pendaftaran->no_pend=$request->no_pend;
        $pendaftaran->nisn=$request->nisn;
        $pendaftaran->tgl_daftar=$request->tgl_daftar;
        $pendaftaran->jenis_seleksi=$request->jenis_seleksi;
        $pendaftaran->jur_sekolah=$request->jur_sekolah;
        $pendaftaran->pil_prodi_1=$request->pil_prodi_1;
        $pendaftaran->pil_prodi_2=$request->pil_prodi_2;
        $pendaftaran->save();

        return redirect(route('pendaftaran.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $pendaftarans = DB::table('pendaftarans')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        return view('pengelola.pendaftaran.pertahun',compact('pendaftarans'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pendaftaran=pendaftaran::where('id',$id)->first();

        return view('pengelola.pendaftaran.edit',compact('pendaftaran'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'no_pend'=>'required',
            'nisn'=>'required',
            // 'tgl_daftar'=>'required',
            'jenis_seleksi'=>'required',
            'jur_sekolah'=>'required',
            'pil_prodi_1'=>'required',
            'pil_prodi_2'=>'required',
            ]);

         $pendaftaran = pendaftaran::find($id);
       $pendaftaran->no_pend=$request->no_pend;
        $pendaftaran->nisn=$request->nisn;
        $pendaftaran->tgl_daftar=$request->tgl_daftar;
        $pendaftaran->jenis_seleksi=$request->jenis_seleksi;
        $pendaftaran->jur_sekolah=$request->jur_sekolah;
        $pendaftaran->pil_prodi_1=$request->pil_prodi_1;
        $pendaftaran->pil_prodi_2=$request->pil_prodi_2;
        $pendaftaran->save();

        return redirect(route('pendaftaran.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        pendaftaran::where('id',$id)->delete();
        return redirect()->back();
    }



}
