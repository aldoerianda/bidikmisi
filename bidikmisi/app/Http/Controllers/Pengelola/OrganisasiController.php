<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\data_organisasi;
use App\Model\pengelola\siswa;

class OrganisasiController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth:pengelola');
    }

    public function index()
    {
    	$data = data_organisasi::all();
    	return view('pengelola.organisasi.show', compact('data'));
    }
    public function create()
    {
    	$siswa = siswa::all();
        $data = data_organisasi::all();
    	return view('pengelola.organisasi.organisasi', compact('data','siswa'));
    }
    public function store(Request $request)
    {
        $data = new data_organisasi;
        $data->nisn=$request->nisn;
        $data->temp_orga=$request->temp_orga;
        $data->nama_orga=$request->nama_orga;
        $data->jabatan=$request->jabatan;
        $data->status=$request->status;
        $data->save();

        return redirect(route('organisasi.index'));
    }
    public function edit($id)
    {
        $data=data_organisasi::where('id_organisasi',$id)->first();
        $siswa=siswa::all();
        return view('pengelola.organisasi.edit',compact('data','siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = data_organisasi::find($id);
        //$pengelolas->nidn=$request->nidn;
        //$pengelolas->username=$request->username;
        //$pengelolas->password=bcrypt($request->password);
        //$pengelolas->level=$request->level;
        $data->update($request->all());

        return redirect(route('organisasi.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        data_organisasi::where('id_organisasi',$id)->delete();
        return redirect()->back();
    }
}
