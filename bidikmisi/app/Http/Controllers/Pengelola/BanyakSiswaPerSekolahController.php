<?php

namespace App\Http\Controllers\Pengelola;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\pengelola\sekolah;
use Illuminate\Support\Facades\DB;
use PDF;

class BanyakSiswaPerSekolahController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
    	$sekolahs = DB::table('sekolahs')
        ->select('nama_sklh', DB::raw('count(*) as total'))
        ->groupBy('nama_sklh')
        ->get();
        return view('pengelola.banyaksiswapersekolah',compact('sekolahs'));
    }
    public function show(Request $request)
    {
        $tahun = $request->tahun;
        $sekolahs = DB::table('sekolahs')
        ->select('nama_sklh', DB::raw('count(*) as total'))
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->groupBy('nama_sklh')
        ->get();
        $pdf = PDF::loadView('pengelola.banyaksiswapersekolahshow', compact('sekolahs','tahun'));
        return $pdf->stream('banyaksiswapersekolah.pdf');
    }
    	
}
