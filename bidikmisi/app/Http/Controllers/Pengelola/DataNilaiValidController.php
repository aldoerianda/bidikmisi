<?php

namespace App\Http\Controllers\Pengelola;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\pengelola\nilai;
use App\Model\pengelola\rumah;
use App\Model\pengelola\siswa;
use App\Model\pengelola\pendaftaran;
use App\Model\pengelola\penghasilan_ortu;
use App\Model\pengelola\sekolah;
use PDF;

function getpres($p){
    $press=0;
    if($p=='-'){
      $press=0;
    } elseif(strpos($p,'5.')){
      $press=5;
    } elseif(strpos($p,'4.')){
      $press=4;
    }elseif(strpos($p,'3.')){
      $press=3;
    } elseif(strpos($p,'2.')){
      $press=2;
    } else{
      $press=1;
    }
    return $press;
}
class DataNilaiValidController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pengelola');
        set_time_limit(0);
    }
    public function index()
    {
        $nilai = nilai::all();
    	$nilais = DB::table('nilais')
        ->select('nisn','pres_ekskul','nama_siswa','ttl_nilai_smst_4','jmlh_mapel_smst_4','ttl_nilai_smst_5','jmlh_mapel_smst_5','tgl_daftar',
        	DB::raw('(((ttl_nilai_smst_4/jmlh_mapel_smst_4)+(ttl_nilai_smst_5/jmlh_mapel_smst_5))/2) as rata'))
  //       ->where(DB::raw('(((ttl_nilai_smst_4/jmlh_mapel_smst_4)+(ttl_nilai_smst_5/jmlh_mapel_smst_5))/2)'),'>', 0)
		// ->where(DB::raw('(((ttl_nilai_smst_4/jmlh_mapel_smst_4)+(ttl_nilai_smst_5/jmlh_mapel_smst_5))/2)'),'<=', 100)
        ->get();

        foreach ($nilai as $key => $value) {
            $value->jml_prestasi=getpres($value->pres_ekskul);
            $value->save();
        }
        return view('pengelola.validasinilai',compact('nilais'));
    }
    public function show(Request $request)
    {
        $tahun = $request->tahun;
        $nilais = DB::table('nilais')
        ->select('nisn','nama_siswa','ttl_nilai_smst_4','jmlh_mapel_smst_4','ttl_nilai_smst_5','jmlh_mapel_smst_5','tgl_daftar',
            DB::raw('(((ttl_nilai_smst_4/jmlh_mapel_smst_4)+(ttl_nilai_smst_5/jmlh_mapel_smst_5))/2) as rata'))
  //       ->where(DB::raw('(((ttl_nilai_smst_4/jmlh_mapel_smst_4)+(ttl_nilai_smst_5/jmlh_mapel_smst_5))/2)'),'>', 0)
        // ->where(DB::raw('(((ttl_nilai_smst_4/jmlh_mapel_smst_4)+(ttl_nilai_smst_5/jmlh_mapel_smst_5))/2)'),'<=', 100)
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();

        $pdf = PDF::loadView('pengelola.validasinilaishow', compact('nilais','tahun'));
        return $pdf->stream('validasinilai.pdf');
    }
    public function vld(){
        $data=nilai::all();
        foreach ($data as $key => $value) {
            if($value->jmlh_mapel_smst_5 == 0 || $value->jmlh_mapel_smst_4==0){
                nilai::find($value->nisn)->delete();
                    rumah::where('nisn',$value->nisn)->delete();
                    siswa::where('nisn',$value->nisn)->delete();
                    pendaftaran::where('nisn',$value->nisn)->delete();
                    sekolah::where('nisn',$value->nisn)->delete();
                    penghasilan_ortu::where('nisn',$value->nisn)->delete();
                    //$r->delete();
                    //$s->delete();
                    //$p->delete();
                    //$sek->delete();
                    //$po->delete();
                    //$del->delete();
            }else{
                $n=(($value->ttl_nilai_smst_4/$value->jmlh_mapel_smst_4)+($value->ttl_nilai_smst_5/$value->jmlh_mapel_smst_5))/2;
                if($n==0 || $n>100){
                    $del=nilai::find($value->nisn);
                    $r=rumah::where('nisn',$value->nisn)->first();
                    $s=siswa::where('nisn',$value->nisn)->first();
                    $n=nilai::where('nisn',$value->nisn)->first();
                    $p=pendaftaran::where('nisn',$value->nisn)->first();
                    $sek=sekolah::where('nisn',$value->nisn)->first();
                    $po=penghasilan_ortu::where('nisn',$value->nisn)->first();
                    $r->delete();
                    $s->delete();
                    $n->delete();
                    $p->delete();
                    $sek->delete();
                    $po->delete();
                    $del->delete();
                }
            }
        }
        return redirect('pengelola/validasinilai');
    }
}
