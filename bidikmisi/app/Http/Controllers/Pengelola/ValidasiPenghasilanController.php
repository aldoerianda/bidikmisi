<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\penghasilan_ortu;
use App\Model\pengelola\siswa;
use App\Model\pengelola\nilai;
use App\Model\pengelola\rumah;
use App\Model\pengelola\pendaftaran;
use App\Model\pengelola\sekolah;
use Illuminate\Support\Facades\DB;

class ValidasiPenghasilanController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:pengelola');
        set_time_limit(0);
    }
    public function index()
    {
    	//$penghasilans = penghasilan_ortu::all();
    	$penghasilans = DB::table('penghasilan_ortus as a')
        ->leftjoin('siswas as b','b.nisn', '=', 'a.nisn')
        ->select('a.*', 'b.nama_siswa')
        ->get();

        $dataKonversiAyah = [];
        $dataKonversiIbu= [];
        $i = 0;
        foreach ($penghasilans as $data) {
            switch ($data->peng_ayah) {
                case '< Rp. 250.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case '> Rp. 10.000.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 10250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 1.000.001 - Rp. 1.250.000':
                $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 1250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 1.250.001 - Rp. 1.500.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 1500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 1.500.001 - Rp. 1.750.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 1750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 1.750.001 - Rp. 2.000.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 2000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 2.000.001 - Rp. 2.250.000':
                $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 2250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 2.250.001 - Rp. 2.500.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 2500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 2.500.001 - Rp. 2.750.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 2750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 2.750.001 - Rp. 3.000.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 250.001 - Rp. 500.000':
                $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 3.000.001 - Rp. 3.250.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 3.250.001 - Rp. 3.500.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 3.500.001 - Rp. 3.750.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 3.750.001 - Rp. 4.000.000':
                $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 4000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 4.000.001 - Rp. 4.250.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 4250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 4.250.001 - Rp. 4.500.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 4500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 4.500.001 - Rp. 4.750.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 4750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 4.750.001 - Rp. 5.000.000':
                $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 5000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 5.000.001 - Rp. 5.250.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 5250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 5.250.001 - Rp. 5.500.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 5500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 5.500.001 - Rp. 5.750.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 5750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 5.750.001 - Rp. 6.000.000':
                $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 6000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 500.001 - Rp. 750.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 6.000.001 - Rp. 6.250.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 6250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 6.250.001 - Rp. 6.500.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 6500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 6.500.001 - Rp. 6.750.000':
                $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 7000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 6.750.001 - Rp. 7.000.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 6750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 7.000.001 - Rp. 7.250.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 7250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 7.250.001 - Rp. 7.500.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 7500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 7.500.001 - Rp. 7.750.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 7750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 7.750.001 - Rp. 8.000.000':
                $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 8000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 750.001 - Rp. 1.000.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 1000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 8.000.001 - Rp. 8.250.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 8250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 8.250.001 - Rp. 8.500.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 8500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 8.500.001 - Rp. 8.750.000':
                $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 8750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 8.750.001 - Rp. 9.000.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 9000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 9.000.001 - Rp. 9.250.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 9250000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 9.250.001 - Rp. 9.500.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 9500000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 9.500.001 - Rp. 9.750.000':
                $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 9750000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Rp. 9.750.000 - Rp. 10.000.000':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 10000000 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                case 'Tidak Berpenghasilan':
                $dataKonversiAyah[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 0 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
                default:
                $dataKonversiAyah[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 0 , 'tanggungan' => $data->jmlh_tanggungan]);
                break;
            }
            switch ($data->peng_ibu) {
                case '< Rp. 250.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 250000]);
                break;
                case '> Rp. 10.000.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 10250000]);
                break;
                case 'Rp. 1.000.001 - Rp. 1.250.000':
                $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 1250000]);
                break;
                case 'Rp. 1.250.001 - Rp. 1.500.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 1500000]);
                break;
                case 'Rp. 1.500.001 - Rp. 1.750.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 1750000]);
                break;
                case 'Rp. 1.750.001 - Rp. 2.000.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 2000000]);
                break;
                case 'Rp. 2.000.001 - Rp. 2.250.000':
                $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 2250000]);
                break;
                case 'Rp. 2.250.001 - Rp. 2.500.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 2500000]);
                break;
                case 'Rp. 2.500.001 - Rp. 2.750.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 2750000]);
                break;
                case 'Rp. 2.750.001 - Rp. 3.000.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3000000]);
                break;
                case 'Rp. 250.001 - Rp. 500.000':
                $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 500000]);
                break;
                case 'Rp. 3.000.001 - Rp. 3.250.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3250000]);
                break;
                case 'Rp. 3.250.001 - Rp. 3.500.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3500000]);
                break;
                case 'Rp. 3.500.001 - Rp. 3.750.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 3750000]);
                break;
                case 'Rp. 3.750.001 - Rp. 4.000.000':
                $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 4000000]);
                break;
                case 'Rp. 4.000.001 - Rp. 4.250.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 4250000]);
                break;
                case 'Rp. 4.250.001 - Rp. 4.500.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 4500000]);
                break;
                case 'Rp. 4.500.001 - Rp. 4.750.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 4750000]);
                break;
                case 'Rp. 4.750.001 - Rp. 5.000.000':
                $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 5000000]);
                break;
                case 'Rp. 5.000.001 - Rp. 5.250.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 5250000]);
                break;
                case 'Rp. 5.250.001 - Rp. 5.500.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 5500000]);
                break;
                case 'Rp. 5.500.001 - Rp. 5.750.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 5750000]);
                break;
                case 'Rp. 5.750.001 - Rp. 6.000.000':
                $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 6000000]);
                break;
                case 'Rp. 500.001 - Rp. 750.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 750000]);
                break;
                case 'Rp. 6.000.001 - Rp. 6.250.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 6250000]);
                break;
                case 'Rp. 6.250.001 - Rp. 6.500.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 6500000]);
                break;
                case 'Rp. 6.500.001 - Rp. 6.750.000':
                $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 7000000]);
                break;
                case 'Rp. 6.750.001 - Rp. 7.000.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 6750000]);
                break;
                case 'Rp. 7.000.001 - Rp. 7.250.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 7250000]);
                break;
                case 'Rp. 7.250.001 - Rp. 7.500.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 7500000]);
                break;
                case 'Rp. 7.500.001 - Rp. 7.750.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 7750000]);
                break;
                case 'Rp. 7.750.001 - Rp. 8.000.000':
                $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 8000000]);
                break;
                case 'Rp. 750.001 - Rp. 1.000.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 1000000]);
                break;
                case 'Rp. 8.000.001 - Rp. 8.250.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 8250000]);
                break;
                case 'Rp. 8.250.001 - Rp. 8.500.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 8500000]);
                break;
                case 'Rp. 8.500.001 - Rp. 8.750.000':
                $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 8750000]);
                break;
                case 'Rp. 8.750.001 - Rp. 9.000.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 9000000]);
                break;
                case 'Rp. 9.000.001 - Rp. 9.250.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 9250000]);
                break;
                case 'Rp. 9.250.001 - Rp. 9.500.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 9500000]);
                break;
                case 'Rp. 9.500.001 - Rp. 9.750.000':
                $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 9750000]);
                break;
                case 'Rp. 9.750.000 - Rp. 10.000.000':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 10000000]);
                break;
                case 'Tidak Berpenghasilan':
                $dataKonversiIbu[$i] = json_encode([ 'nisn' => $data->nisn , 'value' => 0]);
                break;
                default:
                $dataKonversiIbu[$i] =  json_encode([ 'nisn' => $data->nisn , 'value' => 0]);
                break;
            }
            $i++;
        }
        $income= [];
        $ratabiaya=[];
        for ($i =0; $i    <  count($dataKonversiAyah); $i++) { 
            $income[$i] = json_decode($dataKonversiAyah[$i])->value + json_decode($dataKonversiIbu[$i])->value;
            if(json_decode($dataKonversiAyah[$i])->tanggungan==0){
                $ratabiaya[$i] = $income[$i];
            }
            else{
                $ratabiaya[$i] = $income[$i] / json_decode($dataKonversiAyah[$i])->tanggungan;
            }
        }
        for ($r=0; $r < count($income); $r++) { 
            penghasilan_ortu::find(json_decode($dataKonversiAyah[$r])->nisn)->update([
                'income' => $income[$r],
                'rata_biaya' => $ratabiaya[$r] 
            ]);
        }

        return view('pengelola.validasipenghasilan', compact('penghasilans'));
    }
    public function vld(Request $request)
    {
       $data = penghasilan_ortu::all();
    	//var_dump($data);
       foreach ($data as $key => $value) {
          if($value->income > $request->pendapatan || $value->rata_biaya > $request->rata){
             pendaftaran::where('nisn',$value->nisn)->delete();
             rumah::where('nisn',$value->nisn)->delete();
             siswa::where('nisn',$value->nisn)->delete();
             nilai::where('nisn',$value->nisn)->delete();
             penghasilan_ortu::where('nisn',$value->nisn)->delete();
             sekolah::where('nisn',$value->nisn)->delete();
         }
     }
     return redirect('pengelola/validasipenghasilan');
 }
}
