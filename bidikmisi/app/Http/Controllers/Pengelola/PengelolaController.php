<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\pengelola;

class PengelolaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $pengelolas = pengelola::all();
        return view('pengelola.pengelola.show',compact('pengelolas'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('pengelola.pengelola.pengelola');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required',
            'nama'=>'required',
            'tempat_lahir'=>'required',
            'tgl_lahir'=>'required',
            'jekel'=>'required',
            'no_hp'=>'required',
            'alamat'=>'required',
            ]);
        $pengelola = new pengelola;
        $pengelola->email=$request->email;
        $pengelola->password=$request->password;
        $pengelola->nama=$request->nama;
        $pengelola->tempat_lahir=$request->tempat_lahir;
        $pengelola->tgl_lahir=$request->tgl_lahir;
        $pengelola->jekel=$request->jekel;
        $pengelola->no_hp=$request->no_hp;
        $pengelola->alamat=$request->alamat;
        $pengelola->save();

        return redirect(route('pengelola.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengelola=pengelola::where('id',$id)->first();

        return view('pengelola.pengelola.edit',compact('pengelola'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'email'=>'required',
            'password'=>'required',
            'nama'=>'required',
            'tempat_lahir'=>'required',
            'tgl_lahir'=>'required',
            'jekel'=>'required',
            'no_hp'=>'required',
            'alamat'=>'required',
            ]);

         $pengelola = pengelola::find($id);
       $pengelola->email=$request->email;
        $pengelola->password=$request->password;
        $pengelola->nama=$request->nama;
        $pengelola->tempat_lahir=$request->tempat_lahir;
        $pengelola->tgl_lahir=$request->tgl_lahir;
        $pengelola->jekel=$request->jekel;
        $pengelola->no_hp=$request->no_hp;
        $pengelola->alamat=$request->alamat;
        $pengelola->save();

        return redirect(route('pengelola.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        pengelola::where('id',$id)->delete();
        return redirect()->back();
    }
}
