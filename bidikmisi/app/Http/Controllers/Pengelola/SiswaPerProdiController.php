<?php

namespace App\Http\Controllers\Pengelola;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use PDF;

class SiswaPerProdiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
    	$pendaftarans = DB::table('pendaftarans')
        ->select('nisn', 'tgl_daftar', 'pil_prodi_1', 'pil_prodi_2' )
        ->get();
        return view('pengelola.siswaperprodi',compact('pendaftarans'));
    }
    public function show(Request $request)
    {
        $tahun = $request->tahun;
        $prodi = $request->prodi;
        $pendaftarans = DB::table('pendaftarans')
        ->select('nisn', 'tgl_daftar', 'pil_prodi_1', 'pil_prodi_2')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->where(function ($query) {
                $query->where('pil_prodi_1', '=', request()->prodi)
                      ->orWhere('pil_prodi_2', '=', request()->prodi);
            })
        ->get();
        $pdf = PDF::loadView('pengelola.siswaperprodishow', compact('pendaftarans','tahun','prodi'));
        return $pdf->stream('siswaperprodi.pdf');
    }
}
