<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\data_foto;

class DatafotoController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth:pengelola');
    }

    public function index()
    {
    	$datafoto = data_foto::all();
    	return view('pengelola.datafoto.show', compact('datafoto'));
    }
  
    public function destroy($id)
    {
        data_foto::where('id_foto',$id)->delete();
        return redirect()->back();
    }
}
