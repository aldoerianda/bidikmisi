<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\sekolah;
use Illuminate\Support\Facades\DB;
use PDF;

class BanyakSiswaPerKotaController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $showprov = DB::table('sekolahs')
        ->select('prov_sklh')
        ->groupBy('prov_sklh')
        ->get();
    	$sekolahs = DB::table('sekolahs')
        ->select('kota_sklh', DB::raw('count(*) as total'))
        ->groupBy('kota_sklh')
        ->get();
        return view('pengelola.banyaksiswaperkota',compact('sekolahs','showprov'));
    }

    public function show(Request $request){
        $prov_sklh = $request->prov_sklh;
        $tahun = $request->tahun;
        $sekolahs = DB::table('sekolahs')
        ->select('kota_sklh', DB::raw('count(*) as total'))
        ->where('prov_sklh', '=', $request->prov_sklh)
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->groupBy('kota_sklh')
        ->get();
        $pdf = PDF::loadView('pengelola.banyaksiswaperkotashow', compact('sekolahs','tahun','prov_sklh'));
        return $pdf->stream('banyaksiswaperkota.pdf');
    }


}
