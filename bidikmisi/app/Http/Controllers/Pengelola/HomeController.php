<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\pengelola\sekolah;
use App\Model\pengelola\siswa;
use App\Model\pengelola\visitasi;
use App\Model\pengelola\dosen;

class HomeController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth:pengelola');
	}
	public function index()
	{
		$year = (int)date('Y');
		$datasmk = sekolah::where('nama_sklh','like','SMK%')->whereYear('tgl_daftar', '=' , $year)->count(); 
		$datasma = sekolah::where('nama_sklh','like','SMA%')->whereYear('tgl_daftar', '=' , $year)->count(); 
		$datama = sekolah::where('nama_sklh','like','MA%')->whereYear('tgl_daftar', '=' , $year)->count(); 

		//$year = (int)date('Y');
		$data = DB::table('pendaftarans')
		->select(
			DB::raw('pil_prodi_1 as pil_prodi_1'),
			DB::raw('count(*) as number'))
		->whereYear('tgl_daftar', '=', $year)
		->groupBy('pil_prodi_1')
		->get();
		$array[] = ['Pil Prodi 1', 'Number'];
		foreach($data as $key => $value)
		{
			$array[++$key] = [$value->pil_prodi_1, $value->number];
		}

		$sekolahs = DB::table('sekolahs')
		->select(DB::raw('prov_sklh as prov_sklh'),
			DB::raw('count(*) as number'))
		->whereYear('tgl_daftar', '=', $year)
		->groupBy('prov_sklh')
		->get();
		$array2[] = ['Provinsi Sekolah', 'Number'];
		foreach($sekolahs as $key => $value)
		{
			$array2[++$key] = [$value->prov_sklh, $value->number];
		}

		$sekolah = DB::table('sekolahs')
        ->select(DB::raw('kota_sklh as kota_sklh'),
        DB::raw('count(*) as number'))
        ->where('prov_sklh', '=', 'sumatera barat')
        ->whereYear('tgl_daftar', '=', $year)
        ->groupBy('kota_sklh')
        ->get();
        $array3[] = ['Kota Sekolah', 'Number'];
     foreach($sekolah as $key => $value)
     {
      $array3[++$key] = [$value->kota_sklh, $value->number];
     }
		$pil_prodi_1 = json_encode($array);
		$prov_sklh = json_encode($array2);
		$kota_sklh = json_encode($array3);

		$siswa = siswa::whereYear('tgl_daftar', '=' , $year)->count();
		$validasi = visitasi::whereYear('tgl_visit', '=' , $year)->count();
		$dosen = dosen::all()->count();
		return view('pengelola/home',compact('pil_prodi_1','datasmk','datasma','datama','prov_sklh','kota_sklh','siswa','validasi','dosen'));
	}
}
