<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Model\pengelola\visitasi;
use App\Model\pengelola\dosen;
use App\Model\pengelola\siswa;
use Excel;


class VisitasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    
    public function index()
    {
        //$visitasis = visitasi::all();
        $visitasis = DB::table('visitasis as a')
        ->leftjoin('dosens as b','b.nidn', '=', 'a.nidn')
        ->leftjoin('siswas as c','c.nisn', '=', 'a.nisn')
        ->leftjoin('dosens as d', 'd.nidn', '=', 'a.nidn2')
        ->select('a.*', 'b.nama_dosen as nama_dosen1','d.nama_dosen as nama_dosen2','c.nama_siswa','c.alamat')
        ->get();
        return view('pengelola.visitasi.show',compact('visitasis'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dosens = dosen::all();
        $siswas = siswa::all();
        return view('pengelola.visitasi.visitasi' , compact('dosens', 'siswas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nisn'=>'required',
            'nidn'=>'required',
            'nidn2'=>'required',
            'tgl_visit'=>'required',
            // 'keterangan'=>'required',
        ]);
        $siswa = siswa::find($request->nisn);
        $siswa->id_visitor1 = $request->nidn;
        $siswa->id_visitor2 = $request->nidn2;
        $siswa->save();
        $visitasi = new visitasi;
        $visitasi->nisn=$request->nisn;
        $visitasi->nidn=$request->nidn;
        $visitasi->nidn=$request->nidn2;
        $visitasi->tgl_visit=$request->tgl_visit;
        $visitasi->keterangan=$request->keterangan;
        $visitasi->save();

        return redirect(route('visitasi.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $visitasi=DB::table('visitasis as a')
        ->leftjoin('siswas as b','b.nisn','=','a.nisn')
        ->leftjoin('dosens as c','c.nidn','=','a.nidn')
        ->leftjoin('dosens as d','d.nidn','=','a.nidn2')
        ->select('a.*','b.nama_siswa','c.nama_dosen as nama_dosen1','d.nama_dosen as nama_dosen2','b.alamat')
        ->where('a.id',$id)
        ->first();
        $dosens = dosen::all();
        $siswas = siswa::all();
        //dd($dosens);

        return view('pengelola.visitasi.edit',compact('siswas','dosens','visitasi'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'nisn'=>'required',
           'nidn'=>'required',
           'nidn2'=>'required',
           'tgl_visit'=>'required',
            // 'keterangan'=>'required',
       ]);
        $siswa = siswa::find($request->nisn);
        $siswa->id_visitor1 = $request->nidn;
        $siswa->id_visitor2 = $request->nidn2;
        $siswa->save();
        $visitasi = visitasi::find($id);
        $visitasi->nisn=$request->nisn;
        $visitasi->nidn=$request->nidn;
        $visitasi->nidn2=$request->nidn2;
        $visitasi->tgl_visit=$request->tgl_visit;
        $visitasi->keterangan=$request->keterangan;
        $visitasi->save();

        return redirect(route('visitasi.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        visitasi::where('id',$id)->delete();
        return redirect()->back();
    }

    public function Import(Request $request)
    {
        if($request->hasFile('file')){
            $path = $request->file('file')->getRealPath();
            Excel::filter('chunk')->load($path)->chunk(250, function($data){
                if (!empty($data) && $data->count()) {
                    foreach ($data as $key => $value) {
                        $checkdata = visitasi::where('nisn', $value->nisn)->get();
                        if(count($checkdata)>0){
                            $tgl = ($value->tanggal_visit - 25569) * 86400;
                            $visitasi = visitasi::where('nisn', $value->nisn)->update(['nisn'=>$value->nisn,'nidn'=>$value->nidn_dosen_1,'nidn2'=>$value->nidn_dosen_2, 'tgl_visit'=>gmdate("Y-m-d H:i:s", $tgl)]);
                            
                        }else{
                            $visitasi = new visitasi();
                            $visitasi->nisn = $value->nisn;
                            $visitasi->nidn = $value->nidn_dosen_1;
                            $visitasi->nidn2 = $value->nidn_dosen_2;
                            $tgl = ($value->tanggal_visit - 25569) * 86400;
                            $visitasi->tgl_visit = gmdate("Y-m-d H:i:s", $tgl);
                            $visitasi->save();
                        }
                    }
                }
            });
        }
        return back();
    }

    public function Export()
    {
        
        $visitasis = DB::select(DB::raw("SELECT a.nisn,c.nama_siswa,a.nidn,b.nama_dosen as nama_dosen1, a.nidn2,d.nama_dosen as nama_dosen2,DATE_FORMAT(a.tgl_visit, '%d/%m/%Y'),c.alamat FROM visitasis a left join dosens b on b.nidn=a.nidn left join siswas c on c.nisn=a.nisn left join dosens d on d.nidn = a.nidn2"));
        $Array = [];
        $Array[] = ['NISN', 'Nama Siswa','NIDN Dosen 1', 'Nama Dosen 1', 'NIDN Dosen 2', 'Nama Dosen 2', 'Tanggal Visit', 'Alamat'];
        foreach ($visitasis as $value) {
            $Array[] = collect($value)->toArray();
        }

        Excel::create('DataVisitasi', function($excel) use ($Array) {
            $excel->sheet('sheet1', function($sheet) use ($Array) {
                $sheet->fromArray($Array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }
}
