<?php
namespace App\Http\Controllers\Pengelola;
use App\Http\Controllers\Controller;
use App\Model\pengelola\nilai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
function getpres($p){
    $press=0;
    if($p=='-'){
      $press=0;
    } elseif(strpos($p,'5.')){
      $press=5;
    } elseif(strpos($p,'4.')){
      $press=4;
    }elseif(strpos($p,'3.')){
      $press=3;
    } elseif(strpos($p,'2.')){
      $press=2;
    } else{
      $press=1;
    }
    return $press;
}
class NilaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:pengelola');
        set_time_limit(0);
    }
    
    public function index()
    {
        $nilais = nilai::all();
        foreach ($nilais as $key => $value) {
            $value->jml_prestasi=getpres($value->pres_ekskul);
            $value->save();
        }
        return view('pengelola.nilai.show',compact('nilais'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('pengelola.nilai.nilai');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nisn'=>'required',
            'nama_siswa'=>'required',
            'nama_sklh'=>'required',
            // 'rang_smst_4'=>'required',
            // 'ttl_nilai_smst_4'=>'required',
            // 'jmlh_mapel_smst_4'=>'required',
            // 'rang_smst_5'=>'required',
            // 'ttl_nilai_smst_5'=>'required',
            // 'jmlh_mapel_smst_5'=>'required',
            // 'rang_smst_6'=>'required',
            // 'ttl_nilai_smst_6'=>'required',
            // 'jmlh_mapel_smst_6'=>'required',
            // 'nilai_unas'=>'required',
            // 'jmlh_mapel_unas'=>'required',
            // 'pres_ekskul'=>'required',
            'tgl_daftar'=>'required',
            ]);
        $nilai = new nilai;
        $nilai->nisn=$request->nisn;
        $nilai->nama_siswa=$request->nama_siswa;
        $nilai->nama_sklh=$request->nama_sklh;
        $nilai->rang_smst_4=$request->rang_smst_4;
        $nilai->ttl_nilai_smst_4=$request->ttl_nilai_smst_4;
        $nilai->jmlh_mapel_smst_4=$request->jmlh_mapel_smst_4;
        $nilai->rang_smst_5=$request->rang_smst_5;
        $nilai->ttl_nilai_smst_5=$request->ttl_nilai_smst_5;
        $nilai->jmlh_mapel_smst_5=$request->jmlh_mapel_smst_5;
        $nilai->rang_smst_6=$request->rang_smst_6;
        $nilai->ttl_nilai_smst_6=$request->ttl_nilai_smst_6;
        $nilai->jmlh_mapel_smst_6=$request->jmlh_mapel_smst_6;
        $nilai->nilai_unas=$request->nilai_unas;
        $nilai->jmlh_mapel_unas=$request->jmlh_mapel_unas;
        $nilai->pres_ekskul=$request->pres_ekskul;
        $nilai->tgl_daftar=$request->tgl_daftar;
        $nilai->save();

        return redirect(route('nilai.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $nilais = DB::table('nilais')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        return view('pengelola.nilai.pertahun',compact('nilais'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nilai=nilai::where('nisn',$id)->first();

        return view('pengelola.nilai.edit',compact('nilai'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
           'nisn'=>'required',
            'nama_siswa'=>'required',
            'nama_sklh'=>'required',
            // 'rang_smst_4'=>'required',
            // 'ttl_nilai_smst_4'=>'required',
            // 'jmlh_mapel_smst_4'=>'required',
            // 'rang_smst_5'=>'required',
            // 'ttl_nilai_smst_5'=>'required',
            // 'jmlh_mapel_smst_5'=>'required',
            // 'rang_smst_6'=>'required',
            // 'ttl_nilai_smst_6'=>'required',
            // 'jmlh_mapel_smst_6'=>'required',
            // 'nilai_unas'=>'required',
            // 'jmlh_mapel_unas'=>'required',
            // 'pres_ekskul'=>'required',
            'tgl_daftar'=>'required',
            ]);

         $nilai = nilai::find($id);
        $nilai->nisn=$request->nisn;
        $nilai->nama_siswa=$request->nama_siswa;
        $nilai->nama_sklh=$request->nama_sklh;
        $nilai->rang_smst_4=$request->rang_smst_4;
        $nilai->ttl_nilai_smst_4=$request->ttl_nilai_smst_4;
        $nilai->jmlh_mapel_smst_4=$request->jmlh_mapel_smst_4;
        $nilai->rang_smst_5=$request->rang_smst_5;
        $nilai->ttl_nilai_smst_5=$request->ttl_nilai_smst_5;
        $nilai->jmlh_mapel_smst_5=$request->jmlh_mapel_smst_5;
        $nilai->rang_smst_6=$request->rang_smst_6;
        $nilai->ttl_nilai_smst_6=$request->ttl_nilai_smst_6;
        $nilai->jmlh_mapel_smst_6=$request->jmlh_mapel_smst_6;
        $nilai->nilai_unas=$request->nilai_unas;
        $nilai->jmlh_mapel_unas=$request->jmlh_mapel_unas;
        $nilai->pres_ekskul=$request->pres_ekskul;
        $nilai->tgl_daftar=$request->tgl_daftar;
        $nilai->save();

        return redirect(route('nilai.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        nilai::where('nisn',$id)->delete();
        return redirect()->back();
    }
}
