<?php

namespace App\Http\Controllers\Pengelola\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Model\pengelola\pengelola;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    
        protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    public function showLoginForm()
    {
        return view('pengelola.login');
    }
    public function login(Request $request)
    {
        $this->validateLoginUsername($request);
        if ($this->attemptLoginUsername($request)) {
            //return $this->sendLoginResponse($request);
            if ($request->level=='Pengelola') {
                return redirect('/pengelola/home');
            }else if($request->level=='Visitor'){
                return redirect('/dosen/home');
            }
        }
            return $this->sendFailedLoginUsernameResponse($request);
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

     public function __construct()
    {
        $this->middleware('guest:pengelola')->except('logout');
    }

    protected function guard()
    {
        return Auth::guard('pengelola');
    }

}
