<?php

namespace App\Http\Controllers\Pengelola;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\pengelola\nilai;
use App\Model\pengelola\rumah;
use App\Model\pengelola\siswa;
use App\Model\pengelola\pendaftaran;
use App\Model\pengelola\penghasilan_ortu;
use App\Model\pengelola\sekolah;
use Illuminate\Support\Facades\DB;
use PDF;

class ValidasiJurusanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pengelola');
        set_time_limit(0);
    }
    public function index()
    {
        $pendaftarans = DB::table('pendaftarans')
        ->select('no_pend','nisn','jur_sekolah','pil_prodi_1','pil_prodi_2','tgl_daftar')
        ->get();
        return view('pengelola.validasijurusan',compact('pendaftarans'));
    }
    public function show(Request $request)
    {
        $tahun = $request->tahun;
        $pendaftarans = DB::table('pendaftarans')
        ->select('no_pend','nisn','jur_sekolah','pil_prodi_1','pil_prodi_2','tgl_daftar')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        $pdf = PDF::loadView('pengelola.validasijurusanshow', compact('pendaftarans','tahun'));
        return $pdf->stream('validasijurusan.pdf');
    }
    public function vld(){
        $data=pendaftaran::all();
        foreach ($data as $key => $value) {
            if(check($value->jur_sekolah,$value->pil_prodi_1,$value->pil_prodi_2)=='Tidak Valid'){
                pendaftaran::find($value->id)->delete();
                rumah::where('nisn',$value->nisn)->delete();
                siswa::where('nisn',$value->nisn)->delete();
                nilai::where('nisn',$value->nisn)->delete();
                penghasilan_ortu::where('nisn',$value->nisn)->delete();
                sekolah::where('nisn',$value->nisn)->delete();
                //$r->delete();
                //$s->delete();
                //$n->delete();
                //$po->delete();
                //$sek->delete();
                //$d->delete();
            }
        }
        return redirect('pengelola/validasijurusan');
    }
}
