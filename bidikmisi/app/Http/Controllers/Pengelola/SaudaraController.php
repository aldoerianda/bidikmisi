<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\data_sau;
use App\Model\pengelola\siswa;

class SaudaraController extends Controller
{
    public function __construct()
    {
    	$this->middleware('auth:pengelola');
    }

    public function index()
    {
    	$datasau = data_sau::all();
    	return view('pengelola.datasau.show', compact('datasau'));
    }
    public function create()
    {
    	$siswa = siswa::all();
        $datasau = data_sau::all();
    	return view('pengelola.datasau.datasau', compact('datasau','siswa'));
    }
    public function store(Request $request)
    {
        $datasau = new data_sau;
        $datasau->nisn=$request->nisn;
        $datasau->jml_saubiaya=$request->jml_saubiaya;
        $datasau->jml_saukandung=$request->jml_saukandung;
        $datasau->jml_sautiri=$request->jml_sautiri;
        $datasau->anak_ke=$request->anak_ke;
        $datasau->sum_biaya=$request->sum_biaya;
        $datasau->tmp_tinggal=$request->tmp_tinggal;
        $datasau->notelp=$request->notelp;
        $datasau->notelp_sau=$request->notelp_sau;
        $datasau->hub=$request->hub;
        $datasau->status=$request->status;
        $datasau->save();

        return redirect(route('datasau.index'));
    }
    public function edit($id)
    {
        $datasau=data_sau::where('nisn',$id)->first();
        $siswa=siswa::all();
        return view('pengelola.datasau.edit',compact('datasau','siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $datasau = data_sau::find($id);
        //$pengelolas->nidn=$request->nidn;
        //$pengelolas->username=$request->username;
        //$pengelolas->password=bcrypt($request->password);
        //$pengelolas->level=$request->level;
        $datasau->update($request->all());

        return redirect(route('datasau.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        data_sau::where('nisn',$id)->delete();
        return redirect()->back();
    }
}
