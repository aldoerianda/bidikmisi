<?php

namespace App\Http\Controllers\Pengelola;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\visitor;

class VisitorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
}
