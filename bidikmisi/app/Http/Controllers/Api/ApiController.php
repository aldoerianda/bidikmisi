<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\pengelola;
use App\Model\pengelola\dosen;
use App\Model\pengelola\siswa;
use App\Model\pengelola\pendaftaran;
use App\Model\pengelola\penghasilan_ortu;
use App\Model\pengelola\sekolah;
use App\Model\pengelola\visitor;
use App\Model\pengelola\visitasi;
use App\Model\pengelola\data_sau;
use App\Model\pengelola\dataket_sau;
use App\Model\pengelola\data_catatan;
use App\Model\pengelola\data_organisasi;
use App\Model\pengelola\data_foto;
use App\Model\pengelola\rumah;
use App\Model\pengelola\nilai;
use JWTAuth;
use Validator;
use Config;

class ApiController extends Controller
{
    //public function __construct()
    //{
    //    $this->middleware('auth:api', [ 'except' => [ 'login' ] ] );
    //}
    public function login(Request $request)
    {   
        Config::set('jwt.user', '\App\Model\pengelola\pengelola');
        Config::set('auth.providers.users.model', \App\Model\pengelola\pengelola::class);
        $credentials = $request->only('username', 'password','level');
        $rules = [
            'username' => 'required',
            'password' => 'required',
            'level' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json([
                'status' => 'error', 
                'message' => $validator->messages()
            ]);
        }
        try {
            // Attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status' => 'error', 
                    'message' => 'We can`t find an account with this credentials.'
                ], 401);
            }else{
                $pengelola = pengelola::all()->where('username',$request['username'])->first();
                $dosen = dosen::all()->where('nidn',$pengelola->nidn)->first();
                $visitasi = visitasi::all()->where('nidn', $pengelola->nidn)->first();
                return response()->json([
                    'id' => $pengelola->id,
                    'nidn' => $pengelola->nidn,
                    'username' => $pengelola->username,
                    'password' => $pengelola->password,
                    'nama_dosen' => $dosen->nama_dosen,
                    'no_hp' => $dosen->no_hp,
                    'nisn' => $visitasi->nisn,
                    'keterangan' => $visitasi->keterangan,
                    'status' => 'success', 
                    'message' => 'Login success',
                    'token' => $token
                // You can add more details here as per you requirment.
                ]);
            }
        } catch (JWTException $e) {
            // Something went wrong with JWT Auth.
            return response()->json([
                'status' => 'error', 
                'message' => 'Failed to login, please try again.'
            ], 500);
        }
        // All good so return the token
        
    }
    public function loginPengelola(Request $request)
    {   
        Config::set('jwt.user', '\App\Model\pengelola\pengelola');
        Config::set('auth.providers.users.model', \App\Model\pengelola\pengelola::class);
        $credentials = $request->only('email', 'password');
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails()) {
            return response()->json([
                'status' => 'error', 
                'message' => $validator->messages()
            ]);
        }
        try {
            // Attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'status' => 'error', 
                    'message' => 'We can`t find an account with this credentials.'
                ], 401);
            }
        } catch (JWTException $e) {
            // Something went wrong with JWT Auth.
            return response()->json([
                'status' => 'error', 
                'message' => 'Failed to login, please try again.'
            ], 500);
        }
        // All good so return the token
        return response()->json([
            'status' => 'success',
            'token' => $token
                // You can add more details here as per you requirment.
        ]);
    }
    
    public function indexPengelola()
    {
        $pengelolas = pengelola::all();
        return response()->json(['data'=>$pengelolas]);
    }

    public function storePengelola(Request $request)
    {
        try{
            $pengelolas = Pengelola::create($request->all());
            return response()->json(['sukses'=>true]);
        }catch(\Exception $e){
            return response()->json(['sukses'=>false,'error'=>'error'.$e]);
        }
    }

    public function updatePengelola(Request $request, $id)
    {
        try{
            $pengelolas = Pengelola::find($id);
            $data['email'] = $request->email;
            $data['password'] = bcrypt($request->password);
            $data['nama'] = $request->nama;
            $pengelolas->update($data);
            return response()->json(['sukses'=>true]);
        }catch(\Exception $e){
            return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
        }
    }

    public function deletePengelola(Request $request, $id)
    {
        try{
            $pengelolas = Pengelola::find($id);
            $pengelolas->delete();
            return response()->json(['sukses'=>true]);
        }catch(\Exception $e){
            return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
        }
    }
    public function indexDosen()
    {
        $dosens = dosen::all();
        return Response()->json(['data'=>$dosens]);
    }

    public function storeDosen(Request $request)
    {
        try{
            $dosens = dosen::create($request->all());
            return response()->json(['sukses'=>true]);
        }catch(\Exception $e){
            return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
        }
    }

    public function updateDosen(Request $request, $id)
    {
        try{
            $dosens = dosen::find($id);
            $data['nidn'] = $request->nidn;
            $data['nama_dosen'] = $request->nama_dosen;
            //$data['tempat_lahir'] = $request->tempat_lahir;
            //$data['tgl_lahir'] = $request->tgl_lahir;
            //$data['jekel'] = $request->jekel;
            $data['no_hp'] = $request->no_hp;
            //$data['alamat'] = $request->alamat;
            $dosens->update($data);
            return response()->json('success');
        }catch(\Exception $e){
            return response()->json(['failed'=> $e]);
        }
    }

    public function deleteDosen(Request $request, $id)
    {
        try{
            $dosens = dosen::find($id);
            $dosens->delete();
            return response()->json(['sukses'=>true]);
        }catch(\Exception $e){
            return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
        }
    }
    public function indexSiswa()
    {
        $siswas = siswa::all();
        return response()->json($siswas);
    }

    public function storeSiswa(Request $request)
    {
      try{
        $siswas = siswa::create($request->all());
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}

public function updateSiswa(Request $request, $id)
{
  try{
    $siswas = siswa::find($id);
        //$data['id_visitor1'] = $request->id_visitor1;
        //$data['id_visitor2'] = $request->id_visitor2;
    $data['nisn'] = $request->nis;
    $data['nama_siswa'] = $request->namadiri;
    $data['tempat_lahir'] = $request->ttl;
    $data['tgl_lahir'] = $request->tgl;
    $data['agama'] = $request->agama;
    $data['jekel'] = $request->jk;
    $data['no_hp'] = $request->hp;
    $data['no_telp'] = $request->nohp;
    $data['alamat'] = $request->alamat;
        //$data['tgl_daftar'] = $request->tgl_daftar;
    $data['email'] = $request->email;
        //$data['latitudermh'] = $request->latitudermh;
        //$data['longitudermh'] = $request->longitudermh;
    $data['status'] = $request->status;
    $siswas->update($data);
    return response()->json('success');
}catch(\Exception $e){
    return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
}
}

public function deleteSiswa(Request $request, $id)
{
  try{
    $siswas = siswa::find($id);
    $siswas->delete();
    return response()->json(['sukses'=>true]);
}catch(\Exception $e){
    return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
}

}
public function indexPendaftaran()
{
    $pendaftarans = pendaftaran::all();
    return response()->json($pendaftarans);
}
public function storePendaftaran(Request $request)
{
    try{
        $pendaftarans = pendaftaran::create($request->all());
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function updatePendaftaran(Request $request, $id)
{
    try{
        $pendaftarans = pendaftaran::find($id);
        $data['no_pend'] = $request->no_pend;
        $data['nisn'] = $request->nisn;
        $data['tgl_daftar'] = $request->tgl_daftar;
        $data['jenis_seleksi'] = $request->jenis_seleksi;
        $data['jur_sekolah'] = $request->jur_sekolah;
        $data['pil_prodi_1'] = $request->pil_prodi_1;
        $data['pil_prodi_2'] = $request->pil_prodi_2;
        $pendaftarans->update($data);
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function deletePendaftaran(Request $request, $id)
{
    try{
        $pendaftarans = pendaftaran::find($id);
        $pendaftarans->delete();
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function indexSekolah()
{
    $sekolahs = sekolah::all();
    return response()->json($sekolahs);
}
public function storeSekolah(Request $request)
{
    try{
        $sekolahs = sekolah::create($request->all());
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function updateSekolah(Request $request, $id)
{
    try{
        $sekolahs = sekolah::find($id);
        $data['nisn'] = $request->nis;
        $data['npsn'] = $request->npsn;
        $data['nama_sklh'] = $request->asalsekolah;
        $data['akreditasi'] = $request->akreditasi;
            //$data['prov_sklh'] = $request->prov_sklh;
            //$data['kota_sklh'] = $request->kota_sklh;
        $data['alamat_se'] = $request->alamatse;
        $data['thn_lulus'] = $request->thnlulus;
        $data['jurusan'] = $request->jurusan;
        $data['prog_keahlian'] = $request->prog;
        $data['nama_kepsek'] = $request->namakepsek;
        $data['nohp_kepsek'] = $request->nohpkepsek;
        $data['notelp_se'] = $request->telpse;
        $data['fax'] = $request->fax;
        $data['website'] = $request->web;
        $data['email_se'] = $request->emailse;
            //$data['latitudesekolah'] = $request->latitudesekolah;
            //$data['longisekolah'] = $request->longisekolah;
            //$data['tgl_daftar'] = $request->tgl_daftar;
        $data['status'] = $request->status;
        $sekolahs->update($data);
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function deleteSekolah(Request $request, $id)
{
    try{
        $sekolahs = sekolah::find($id);
        $sekolahs->delete();
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}

public function indexVisitasi()
{
    $visitasis = visitasi::all();
    return response()->json($visitasis);
}
public function storeVisitasi(Request $request)
{
    try{
        $visitasis = visitasi::create($request->all());
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function updateVisitasi(Request $request, $id)
{
    try{
        $visitasis = visitasi::find($id);
        $data['nisn'] = $request->nisn;
        $data['nidn'] = $request->nidn;
        $data['tgl_visit'] = $request->tgl_visit;
        $data['keterangan'] = $request->keterangan;
        $visitasis->update($data);
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function deleteVisitasi(Request $request, $id)
{
    try{
        $visitasis = visitasi::find($id);
        $visitasis->delete();
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function indexVisitor()
{
    $visitors = visitor::all();
    return response()->json($visitors);
}
public function storeVisitor(Request $request)
{
    try{
        $visitors = visitor::create($request->all());
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function updateVisitor(Request $request,$id)
{
    try{
        $visitors = visitor::find($id);
        $data['username'] = $request->username;
        $data['password'] = $request->password;
        $data['nidn'] = $request->nidn;
        $visitors->update($data);
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function deleteVisitor(Request $request, $id)
{
    try{
        $visitors = visitor::find($id);
        $visitors->delete();
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function indexPenghasilanOrtu()
{
    $penghasilan_ortus = penghasilan_ortu::all();
    return response()->json($penghasilan_ortus);
}
public function storePenghasilanOrtu(Request $request)
{
    try{
        $penghasilan_ortus = penghasilan_ortu::create($request->all());
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function updatePenghasilanOrtu(Request $request, $id)
{
    try{
        $penghasilan_ortus = penghasilan_ortu::find($id);
        $data['nisn'] = $request->nis;
        $data['nama_ibu_kdg'] = $request->namaibu;
        $data['nama_ayah'] = $request->namaayah;
        $data['status_ayah'] = $request->kondisiayah;
        $data['status_ibu'] = $request->kondisiibu;
        $data['usia_ayah'] = $request->usiaayah;
        $data['usia_ibu'] = $request->usiaibu;
        $data['pend_ayah'] = $request->pendayah;
        $data['pend_ibu'] = $request->pendibu;
        $data['pek_ayah'] = $request->pekerayah;
        $data['pek_ibu'] = $request->pekeribu;
        $data['peng_ayah'] = $request->pengayah;
        $data['peng_ibu'] = $request->pengibu;
        $data['income'] = $request->jumpeng;
        $data['jmlh_tanggungan'] = $request->jumtang;
        $data['rata_biaya'] = $request->rata;
            //$data['tgl_daftar'] = $request->tgl_daftar;
        $data['status_nikah'] = $request->statusnikah;
        $data['hub_ibu'] = $request->statusibu;
        $data['hub_ayah'] = $request->statusayah;
        $data['status'] = $request->status;
        $penghasilan_ortus->update($data);
        return response()->json('success');
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function deletePenghasilanOrtu(Request $request, $id)
{
    try{
        $penghasilan_ortus = penghasilan_ortu::find($id);
        $penghasilan_ortus->delete();
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function indexNilai()
{
    $nilais = nilai::all();
    return response()->json($nilais);
}
public function storeNilai(Request $request)
{
    try{
        $nilais = nilai::create($request->all());
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function updateNilai(Request $request, $id)
{
    try{
        $nilais = nilai::find($id);
        $data['nisn'] = $request->nis;
            //$data['nama_siswa'] = $request->nama_siswa;
            //$data['nama_sklh'] = $request->nama_sklh;
        $data['rang_sem1'] = $request->peringkat1;
        $data['nilai_rata_sem1'] = $request->nilairata1;
        $data['jml_siswakelas_sem1'] = $request->jumse1;
        $data['jml_kelasju_sem1'] = $request->jumkeju1;
        $data['jml_kelasse_sem1'] = $request->jumkese1;
        $data['rang_sem2'] = $request->peringkat2;
        $data['nilai_rata_sem2'] = $request->nilairata2;
        $data['jml_siswakelas_sem2'] = $request->jumse2;
        $data['jml_kelasju_sem2'] = $request->jumkeju2;
        $data['jml_kelasse_sem2'] = $request->jumkese2;
        $data['rang_sem3'] = $request->peringkat3;
        $data['nilai_rata_sem3'] = $request->nilairata3;
        $data['jml_siswakelas_sem3'] = $request->jumse3;
        $data['jml_kelasju_sem3'] = $request->jumkeju3;
        $data['jml_kelasse_sem3'] = $request->jumkese3;
        $data['rang_smst_4'] = $request->peringkat4;
            //$data['ttl_nilai_smst_4'] = $request->ttl_nilai_smst_4;
            //$data['jmlh_mapel_smst_4'] = $request->jmlh_mapel_smst_4;
        $data['nilai_rata_sem4'] = $request->nilairata4;
        $data['jml_siswakelas_sem4'] = $request->jumse4;
        $data['jml_kelasju_sem4'] = $request->jumkeju4;
        $data['jml_kelasse_sem4'] = $request->jumkese4;
        $data['rang_smst_5'] = $request->peringkat5;
            //$data['ttl_nilai_smst_5'] = $request->ttl_nilai_smst_5;
            //$data['jmlh_mapel_smst_5'] = $request->jmlh_mapel_smst_5;
        $data['nilai_rata_sem5'] = $request->nilairata5;
        $data['jml_siswakelas_sem5'] = $request->jumse5;
        $data['jml_kelasju_sem5'] = $request->jumkeju5;
        $data['jml_kelasse_sem5'] = $request->jumkese5;
        $data['rang_smst_6'] = $request->peringkat6;
            ///$data['ttl_nilai_smst_6'] = $request->ttl_nilai_smst_6;
            //$data['jmlh_mapel_smst_6'] = $request->jmlh_mapel_smst_6;
        $data['nilai_rata_sem6'] = $request->nilairata6;
        $data['jml_siswakelas_sem6'] = $request->jumse6;
        $data['jml_kelasju_sem6'] = $request->jumkeju6;
        $data['jml_kelasse_sem6'] = $request->jumkese6;
            //$data['nilai_unas'] = $request->nilai_unas;
            //$data['jmlh_mapel_unas'] = $request->jmlh_mapel_unas;
        $data['pres_ekskul'] = $request->prestasi;
        $data['jml_prestasi'] = $request->jp;
            //$data['tgl_daftar'] = $request->tgl_daftar;
        $data['status'] = $request->status;
        $nilais->update($data);
        return response()->json('success');
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function deleteNilai(Request $request, $id)
{
    try{
        $nilais = nilai::find($id);
        $nilais->delete();
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function indexRumah()
{
    $rumahs = rumah::all();
    return response()->json($rumahs);
}
public function storeRumah(Request $request)
{
    try{
        $rumahs = rumah::create($request->all());
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function updateRumah(Request $request, $id)
{
    try{
        $rumahs = rumah::find($id);
        $data['nisn'] = $request->nis;
        $data['kepemilikan'] = $request->statusrumah;
        $data['kondisi_rmh'] = $request->kondisirumah;
        $data['thn_peroleh'] = $request->thnperolehan;
        $data['sumber_listrik'] = $request->sumberlistrik;
        $data['luas_tanah'] = $request->ltp;
        $data['luas_bangunan'] = $request->lb;
            //$data['sumber_air'] = $request->sumber_air;
            //$data['mck'] = $request->mck;
        $data['jrk_pusat_kota'] = $request->kota;
        $data['jarak_rmh_sekolah'] = $request->rumah;
        $data['sarana'] = $request->sarana;
            //$data['tgl_daftar'] = $request->tgl_daftar;
        $data['jml_sawah'] = $request->sawah;
        $data['jml_kebun'] = $request->lk;
        $data['jml_kolom'] = $request->kolom;
        $data['jml_kerbau'] = $request->kerbau;
        $data['jml_sapi'] = $request->sapi;
        $data['jml_kuda'] = $request->kuda;
        $data['jml_kambing'] = $request->kd;
        $data['jml_ayam'] = $request->ayam;
        $data['jml_itik'] = $request->itik;
        $data['jml_mobil'] = $request->mobil;
        $data['jml_motor'] = $request->motor;
        $data['kendaraan_lain'] = $request->kendaraanlain;
        $data['pendapat'] = $request->pendapat;
        $data['status'] = $request->status;
        $rumahs->update($data);
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function deleteRumah(Request $request, $id)
{
    try{
        $rumahs = rumah::find($id);
        $rumahs->delete();
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function indexDatasau()
{
    $datasau = data_sau::all();
    return response()->json($datasau);
}
public function storeDatasau(Request $request)
{
    try{
        $datasau = data_sau::create($request->all());
        return response()->json('success');
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function updateDatasau(Request $request,$id)
{
    try{
        $datasau = data_sau::find($id);
        $data['nisn'] = $request->nis;
        $data['jml_saubiaya'] = $request->jumsau;
        $data['jml_saukandung'] = $request->jumsauka;
        $data['jml_sautiri'] = $request->jumsauti;
        $data['anak_ke'] = $request->anakke;
        $data['sum_biaya'] = $request->sumber;
        $data['tmp_tinggal'] = $request->tinggal;
        $data['notelp'] = $request->seluler;
        $data['notelp_sau'] = $request->telpsau;
        $data['hub'] = $request->hubsau;
        $data['status'] = $request->status;
        $datasau->update($data);
        return response()->json('success');
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function deleteDatasau(Request $request, $id)
{
    try{
        $datasau = data_sau::find($id);
        $datasau->delete();
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function indexDataketsau()
{
    $dataketsau = dataket_sau::all();
    return response()->json($dataketsau);
}
public function storeDataketsau(Request $request)
{
    try{
        $dataketsau = dataket_sau::create($request->all());
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function updateDataketsau(Request $request,$id)
{
    try{
        $dataketsau = dataket_sau::find($id);
        $data['nisn'] = $request->nis;
        $data['nama_sau'] = $request->namasaudara;
        $data['umur_sau'] = $request->umursaudara;
        $data['pekerjaan_sau'] = $request->pekerjaansaudara;
        $data['status_hub'] = $request->hub_sau;
        $dataketsau->update($data);
        return response()->json('success');
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function deleteDataketsau(Request $request, $id)
{
    try{
        $dataketsau = dataket_sau::find($id);
        $dataketsau->delete();
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function indexCatatan()
{
    $datacatatan = data_catatan::all();
    return response()->json($datacatatan);
}
public function storeCatatan(Request $request)
{
    try{
        $datacatatan = data_catatan::create($request->all());
        return response()->json(['idnya'=>$request->nisn]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function updateCatatan(Request $request,$id)
{
    try{
        $datacatatan = data_catatan::find($id);
        $data['nisn'] = $request->nis;
        $data['pemahaman1'] = $request->pemahaman1;
        $data['pemahaman2'] = $request->pemahaman2;
        $data['harapan1'] = $request->harapan1;
        $data['harapan2'] = $request->harapan2;
        $data['catatan'] = $request->ctt;
        $data['ijazah'] = $request->ijazah;
        $data['rapor'] = $request->rapor;
        $data['sertifikat'] = $request->sertifikat;
        $data['kk'] = $request->kk;
        $data['surat_peng'] = $request->suratpeng;
        $data['bpjs'] = $request->bpjs;
        $data['kis'] = $request->kis;
        $data['kip'] = $request->kip;
        $data['ksks'] = $request->ksks;
        $data['raskin'] =$request->raskin;
        $data['pbb'] = $request->pbb;
        $data['bukti_listrik'] = $request->listrik;
        $data['surat_data'] = $request->suratasli;
        $data['penilain'] = $request->penilain;
        $data['status'] = $request->status;
        $datacatatan->update($data);
        return response()->json('success');
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function deleteCatatan(Request $request, $id)
{
    try{
        $datacatatan = data_catatan::find($id);
        $datacatatan->delete();
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function indexDatafoto()
{
    $datafoto = data_foto::all();
    return response()->json($datafoto);
}
public function storeDatafoto(Request $request)
{
    try{
        $datafoto = data_foto::create($request->all());
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function updateDatafoto(Request $request,$id)
{
    try{
        $datafoto = data_foto::find($id);
        $data['nisn'] = $request->nisn;
        $data['nama_foto'] = $request->nama_foto;
        $data['extensi'] = $request->extensi;
        $datafoto->update($data);
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function deleteDatafoto(Request $request, $id)
{
    try{
        $datafoto = data_foto::find($id);
        $datafoto->delete();
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function indexOrganisasi()
{
    $dataorga = data_organisasi::all();
    return response()->json($dataorga);
}
public function storeOrganisasi(Request $request)
{
    try{
        $dataorga = data_organisasi::create($request->all());
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function updateOrganisasi(Request $request,$id)
{
    try{
        $dataorga = data_organisasi::find($id);
        $data['nisn'] = $request->nis;
        $data['temp_orga'] = $request->temp_orga;
        $data['nama_orga'] = $request->namaorga;
        $data['jabatan'] = $request->jabatan;
        $data['status'] = $request->status;
        $dataorga->update($data);
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
public function deleteOrganisasi(Request $request, $id)
{
    try{
        $dataorga = data_organisasi::find($id);
        $dataorga->delete();
        return response()->json(['sukses'=>true]);
    }catch(\Exception $e){
        return response()->json(['sukses'=>false, 'error'=>'error'.$e]);
    }
}
}
