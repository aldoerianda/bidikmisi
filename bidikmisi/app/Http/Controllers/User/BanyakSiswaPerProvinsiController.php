<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\pengelola\sekolah;
use Illuminate\Support\Facades\DB;

class BanyakSiswaPerProvinsiController extends Controller
{
    public function index()
    {
        $sekolahs = DB::table('sekolahs')
        ->select(DB::raw('prov_sklh as prov_sklh'),
        DB::raw('count(*) as number'))
        ->groupBy('prov_sklh')
        ->get();
        // return view('user.banyaksiswa_persekolah',compact('sekolahs'));
    $array[] = ['Provinsi Sekolah', 'Number'];
     foreach($sekolahs as $key => $value)
     {
      $array[++$key] = [$value->prov_sklh, $value->number];
     }
     return view('user.banyaksiswa_perprovinsi')->with('prov_sklh', json_encode($array));
    }
    public function show(Request $request)
    {
        $sekolahs = DB::table('sekolahs')
        ->select(DB::raw('prov_sklh as prov_sklh'),
        DB::raw('count(*) as number'))
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->groupBy('prov_sklh')
        ->get();
    $array[] = ['Provinsi Sekolah', 'Number'];
     foreach($sekolahs as $key => $value)
     {
      $array[++$key] = [$value->prov_sklh, $value->number];
     }
     return view('user.banyaksiswa_perprovinsishow')->with('prov_sklh', json_encode($array));
    }
        
}

