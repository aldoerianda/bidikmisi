<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Model\pengelola\berita;
use Illuminate\Http\Request;


class HomeController extends Controller
{
	public function index()
	{
		$show = berita::orderBy('id', 'desc')->paginate(2);
    	return view('user.home',compact('show'));
	}
	public function show($slug)
	{
		$berita = berita::where('slug', $slug)->first();
		return view('user.berita', compact('berita'));
	}
}
