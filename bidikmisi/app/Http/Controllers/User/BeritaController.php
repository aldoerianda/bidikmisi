<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BeritaController extends Controller
{
    public function berita(berita $berita)
    {
    	return view('user.berita',compact('berita'));
    }
}
