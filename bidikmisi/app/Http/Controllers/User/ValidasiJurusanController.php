<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ValidasiJurusanController extends Controller
{
    public function index()
    {
        $pendaftarans = DB::table('pendaftarans')
        ->select('no_pend','nisn','jur_sekolah','pil_prodi_1','pil_prodi_2','tgl_daftar')
        ->get();
        return view('user.validasi_jurusan',compact('pendaftarans'));
    }
    public function show(Request $request)
    {
        $pendaftarans = DB::table('pendaftarans')
        ->select('no_pend','nisn','jur_sekolah','pil_prodi_1','pil_prodi_2','tgl_daftar')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        return view('user.validasi_jurusanshow',compact('pendaftarans'));
    }
}
