<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class LaravelGoogleGraph2Controller extends Controller
{
     function index()
    {
     $data = DB::table('pendaftarans')
       ->select(
        DB::raw('pil_prodi_2 as pil_prodi_2'),
        DB::raw('count(*) as number'))
       ->groupBy('pil_prodi_2')
       ->get();
     $array[] = ['Pil Prodi 1', 'Number'];
     foreach($data as $key => $value)
     {
      $array[++$key] = [$value->pil_prodi_2, $value->number];
     }
     return view('user.googlepiechart2')->with('pil_prodi_2', json_encode($array));
    }
    function show(Request $request)
    {
     $data = DB::table('pendaftarans')
       ->select(
        DB::raw('pil_prodi_2 as pil_prodi_2'),
        DB::raw('count(*) as number'))
       ->whereYear('tgl_daftar', '=', $request->tahun)
       ->groupBy('pil_prodi_2')
       ->get();
     $array[] = ['Pil Prodi 1', 'Number'];
     foreach($data as $key => $value)
     {
      $array[++$key] = [$value->pil_prodi_2, $value->number];
     }
     return view('user.googlepiechart2show')->with('pil_prodi_2', json_encode($array));
    }
}
