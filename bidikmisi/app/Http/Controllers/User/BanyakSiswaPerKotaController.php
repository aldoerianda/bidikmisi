<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\user\sekolah;
use Illuminate\Support\Facades\DB;

class BanyakSiswaPerKotaController extends Controller
{
    public function index()
    {
        $showprov = DB::table('sekolahs')
        ->select('prov_sklh')
        ->groupBy('prov_sklh')
        ->get();
    	$sekolahs = DB::table('sekolahs')
        ->select(DB::raw('kota_sklh as kota_sklh'),
        DB::raw('count(*) as number'))
        ->groupBy('kota_sklh')
        ->get();
        // return view('user.banyaksiswa_perkota',compact('sekolahs','showprov'));
    $array[] = ['Kota Sekolah', 'Number'];
     foreach($sekolahs as $key => $value)
     {
      $array[++$key] = [$value->kota_sklh, $value->number];
     }
     return view('user.banyaksiswa_perkota',compact('sekolahs','showprov'))->with('kota_sklh', json_encode($array));
    }

    public function show(Request $request){
        $sekolahs = DB::table('sekolahs')
        ->select(DB::raw('kota_sklh as kota_sklh'),
        DB::raw('count(*) as number'))
        ->where('prov_sklh', '=', $request->prov_sklh)
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->groupBy('kota_sklh')
        ->get();
        // return view('user.banyaksiswa_perkotashow',compact('sekolahs'));
        $array[] = ['Kota Sekolah', 'Number'];
     foreach($sekolahs as $key => $value)
     {
      $array[++$key] = [$value->kota_sklh, $value->number];
     }
     return view('user.banyaksiswa_perkotashow')->with('kota_sklh', json_encode($array));
    }
}
