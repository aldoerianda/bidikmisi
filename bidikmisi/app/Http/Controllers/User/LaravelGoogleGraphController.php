<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class LaravelGoogleGraphController extends Controller
{
     function index()
    {
     $data = DB::table('pendaftarans')
       ->select(
        DB::raw('pil_prodi_1 as pil_prodi_1'),
        DB::raw('count(*) as number'))
       ->groupBy('pil_prodi_1')
       ->get();
     $array[] = ['Pil Prodi 1', 'Number'];
     foreach($data as $key => $value)
     {
      $array[++$key] = [$value->pil_prodi_1, $value->number];
     }
     return view('user.googlepiechart')->with('pil_prodi_1', json_encode($array));
    }
    function show(Request $request)
    {
     $data = DB::table('pendaftarans')
       ->select(
        DB::raw('pil_prodi_1 as pil_prodi_1'),
        DB::raw('count(*) as number'))
       ->whereYear('tgl_daftar', '=', $request->tahun)
       ->groupBy('pil_prodi_1')
       ->get();
     $array[] = ['Pil Prodi 1', 'Number'];
     foreach($data as $key => $value)
     {
      $array[++$key] = [$value->pil_prodi_1, $value->number];
     }
     return view('user.googlepiechartshow')->with('pil_prodi_1', json_encode($array));
    }
}
