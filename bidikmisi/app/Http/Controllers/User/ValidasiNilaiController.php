<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ValidasiNilaiController extends Controller
{
    public function index()
    {
    	$nilais = DB::table('nilais')
        ->select('nisn','nama_siswa','ttl_nilai_smst_4','jmlh_mapel_smst_4','ttl_nilai_smst_5','jmlh_mapel_smst_5','tgl_daftar',
        	DB::raw('(((ttl_nilai_smst_4/jmlh_mapel_smst_4)+(ttl_nilai_smst_5/jmlh_mapel_smst_5))/2) as rata'))
  //       ->where(DB::raw('(((ttl_nilai_smst_4/jmlh_mapel_smst_4)+(ttl_nilai_smst_5/jmlh_mapel_smst_5))/2)'),'>', 0)
		// ->where(DB::raw('(((ttl_nilai_smst_4/jmlh_mapel_smst_4)+(ttl_nilai_smst_5/jmlh_mapel_smst_5))/2)'),'<=', 100)
        ->get();

        return view('user.validasi_nilai',compact('nilais'));
    }
    public function show(Request $request)
    {
        $nilais = DB::table('nilais')
        ->select('nisn','nama_siswa','ttl_nilai_smst_4','jmlh_mapel_smst_4','ttl_nilai_smst_5','jmlh_mapel_smst_5','tgl_daftar',
            DB::raw('(((ttl_nilai_smst_4/jmlh_mapel_smst_4)+(ttl_nilai_smst_5/jmlh_mapel_smst_5))/2) as rata'))
  //       ->where(DB::raw('(((ttl_nilai_smst_4/jmlh_mapel_smst_4)+(ttl_nilai_smst_5/jmlh_mapel_smst_5))/2)'),'>', 0)
        // ->where(DB::raw('(((ttl_nilai_smst_4/jmlh_mapel_smst_4)+(ttl_nilai_smst_5/jmlh_mapel_smst_5))/2)'),'<=', 100)
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();

        return view('user.validasi_nilaishow',compact('nilais'));
    }
}
