<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\pengelola\sekolah;
use Illuminate\Support\Facades\DB;

class BanyakSiswaPerSekolahController extends Controller
{
    public function index()
    {
    	$sekolahs = DB::table('sekolahs')
        ->select(DB::raw('nama_sklh as nama_sklh'),
        DB::raw('count(*) as number'))
        ->groupBy('nama_sklh')
        ->get();
        // return view('user.banyaksiswa_persekolah',compact('sekolahs'));
    $array[] = ['Asal Sekolah', 'Number'];
     foreach($sekolahs as $key => $value)
     {
      $array[++$key] = [$value->nama_sklh, $value->number];
     }
     return view('user.banyaksiswa_persekolah')->with('nama_sklh', json_encode($array));
    }
    public function show(Request $request)
    {
        $sekolahs = DB::table('sekolahs')
        ->select(DB::raw('nama_sklh as nama_sklh'),
        DB::raw('count(*) as number'))
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->groupBy('nama_sklh')
        ->get();
    $array[] = ['Asal Sekolah', 'Number'];
     foreach($sekolahs as $key => $value)
     {
      $array[++$key] = [$value->nama_sklh, $value->number];
     }
     return view('user.banyaksiswa_persekolahshow')->with('nama_sklh', json_encode($array));
    }
    	
}

