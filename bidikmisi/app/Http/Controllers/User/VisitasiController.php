<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Model\pengelola\visitasi;
use App\Model\pengelola\dosen;
use App\Model\pengelola\siswa;

class VisitasiController extends Controller
{
     public function __construct()
    {
       // $this->middleware('auth:user');
    }
    public function index()
    {
        $visitasis = DB::table('visitasis')
        ->join('dosens','dosens.nidn', '=', 'visitasis.nidn')
        ->join('siswas','siswas.nisn', '=', 'visitasis.nisn')
        ->select('visitasis.*', 'dosens.nama_dosen','siswas.nama_siswa')
         ->get();
        return view('user.jadwal_visitasi',compact('visitasis'));
    }
    public function show($id)
    {
       
    }
}
