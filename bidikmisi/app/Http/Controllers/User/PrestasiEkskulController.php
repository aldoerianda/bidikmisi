<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\pengelola\nilai;

class PrestasiEkskulController extends Controller
{
    public function index()
    {
        $nilais = nilai::where('pres_ekskul','!=', '-')->get();
        return view('user.prestasi_ekskul',compact('nilais'));
    }
    public function show(Request $request)
    {
        $nilais = nilai::where('pres_ekskul','!=', '-')
        ->whereYear('tgl_daftar', '=', $request->tahun)
		->get();
        return view('user.prestasi_ekskulshow',compact('nilais'));
    }
}

