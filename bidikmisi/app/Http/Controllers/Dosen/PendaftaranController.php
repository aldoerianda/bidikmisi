<?php

namespace App\Http\Controllers\Dosen;

use App\Http\Controllers\Controller;
use App\Model\pengelola\pendaftaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class PendaftaranController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $user = Auth::user()->nidn;
        $pendaftarans = DB::select( DB::raw("SELECT * FROM pendaftarans WHERE nisn IN(SELECT nisn FROM siswas WHERE id_visitor1=$user OR id_visitor2=$user )"));
        //$pendaftarans = pendaftaran::all();
        return view('dosen.data-pendaftaran',compact('pendaftarans'));
    }
    public function show(Request $request)
    {
        $pendaftarans = DB::table('pendaftarans')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        return view('dosen.data-pendaftaran-show',compact('pendaftarans'));
    }
}
