<?php

namespace App\Http\Controllers\Dosen;

use App\Http\Controllers\Controller;
use App\Model\pengelola\nilai;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class NilaiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $user = Auth::user()->nidn;
        $nilais = DB::select( DB::raw("SELECT * FROM nilais WHERE nisn IN(SELECT nisn FROM siswas WHERE id_visitor1=$user OR id_visitor2=$user )"));
        //$nilais = nilai::all();
        return view('dosen.data-nilai',compact('nilais'));
    }
    public function show(Request $request)
    {
        $nilais = DB::table('nilais')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        return view('dosen.data-nilai-show',compact('nilais'));
    }
   
}
