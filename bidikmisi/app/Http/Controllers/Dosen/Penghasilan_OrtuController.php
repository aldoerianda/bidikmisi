<?php

namespace App\Http\Controllers\Dosen;

use App\Http\Controllers\Controller;
use App\Model\pengelola\penghasilan_ortu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class Penghasilan_OrtuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $user = Auth::user()->nidn;
        $penghasilan_ortus = DB::select( DB::raw("SELECT * FROM penghasilan_ortus WHERE nisn IN(SELECT nisn FROM siswas WHERE id_visitor1=$user OR id_visitor2=$user )"));
        //$penghasilan_ortus = penghasilan_ortu::all();
        return view('dosen.data-penghasilan_ortu',compact('penghasilan_ortus'));
    }
    public function show(Request $request)
    {
        $penghasilan_ortus = DB::table('penghasilan_ortus')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        return view('dosen.data-penghasilan_ortu-show',compact('penghasilan_ortus'));
    }
}
