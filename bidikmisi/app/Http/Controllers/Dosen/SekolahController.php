<?php

namespace App\Http\Controllers\Dosen;

use App\Http\Controllers\Controller;
use App\Model\pengelola\sekolah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class SekolahController extends Controller
{
   public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $user = Auth::user()->nidn;
        $sekolahs = DB::select( DB::raw("SELECT * FROM sekolahs WHERE nisn IN(SELECT nisn FROM siswas WHERE id_visitor1=$user OR id_visitor2=$user )"));
        //$sekolahs = sekolah::all();
        return view('dosen.data-sekolah',compact('sekolahs'));
    }
   public function show(Request $request)
    {
        $sekolahs = DB::table('sekolahs')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        return view('dosen.data-sekolah-show',compact('sekolahs'));
    }
}
