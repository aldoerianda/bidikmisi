<?php

namespace App\Http\Controllers\Dosen;

use App\Http\Controllers\Controller;
use App\Model\pengelola\rumah;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class RumahController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $user = Auth::user()->nidn;
        $rumahs = DB::select( DB::raw("SELECT * FROM rumahs WHERE nisn IN(SELECT nisn FROM siswas WHERE id_visitor1=$user OR id_visitor2=$user )"));
        //$rumahs = rumah::all();
        return view('dosen.data-rumah',compact('rumahs'));
    }
    public function show(Request $request)
    {
        $rumahs = DB::table('rumahs')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        return view('dosen.data-rumah-show',compact('rumahs'));
    }
}
