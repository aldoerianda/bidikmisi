<?php

namespace App\Http\Controllers\dosen\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dosen/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
   

    public function showLoginForm()
    {
        return view('dosen.login');
    }
    public function login(Request $request)
    {
        $this->validateLoginUsername($request);
        if ($this->attemptLoginUsername($request)) {
            return $this->sendLoginResponse($request);
        }
            return $this->sendFailedLoginUsernameResponse($request);
    }
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

     public function __construct()
    {
        $this->middleware('guest:dosen')->except('logout');
    }

    protected function guard()
    {
        return Auth::guard('dosen');
    }

}
