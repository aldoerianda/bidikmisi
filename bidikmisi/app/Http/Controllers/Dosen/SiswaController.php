<?php

namespace App\Http\Controllers\Dosen;

use App\Http\Controllers\Controller;
use App\Model\pengelola\siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }

    public function index()
    {
        $user = Auth::user()->nidn;
        $siswas = siswa::where('id_visitor1',$user)->orWhere('id_visitor2',$user)->get();
        return view('dosen.data-siswa',compact('siswas'));
    }

    public function show(Request $request)
    {
        $siswas = DB::table('siswas')
        ->whereYear('tgl_daftar', '=', $request->tahun)
        ->get();
        return view('dosen.data-siswa-show',compact('siswas'));
    }
}
