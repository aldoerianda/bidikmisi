<?php

namespace App\Http\Controllers\Dosen;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Model\pengelola\visitasi;
use Auth;

class VisitasiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:pengelola');
    }
    public function index()
    {
        $user = Auth::user()->nidn;
        $visitasis = DB::table('visitasis as a')
        ->leftjoin('dosens as b','b.nidn', '=', 'a.nidn')
        ->leftjoin('siswas as c','c.nisn', '=', 'a.nisn')
        ->leftjoin('dosens as d', 'd.nidn', '=', 'a.nidn2')
        ->select('a.*', 'b.nama_dosen as nama_dosen1','d.nama_dosen as nama_dosen2','c.nama_siswa','c.alamat')
        ->where('c.id_visitor1',$user)
        ->orWhere('c.id_visitor2', $user)
        ->get();
        return view('dosen.data-visitasi',compact('visitasis'));
    }
    public function show($id)
    {
       
    }
}
