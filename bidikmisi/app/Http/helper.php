<?php
	function check($jurusan, $prodi1, $prodi2){
		$p1=0;
		$p2=0;
		switch ($jurusan) {
			case 'IPA':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '22401 - D3 Teknik Sipil':$p1=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p1=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p1=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p1=1;break;
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p1=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p1=1;break;
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '62401 - D3 Akuntansi':$p1=1;break;
						case '62301 - D4 Akuntansi':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '22401 - D3 Teknik Sipil':$p2=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p2=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p2=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p2=1;break;
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p2=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p2=1;break;
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '62401 - D3 Akuntansi':$p2=1;break;
						case '62301 - D4 Akuntansi':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'IPS':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '62401 - D3 Akuntansi':$p1=1;break;
						case '62301 - D4 Akuntansi':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '62401 - D3 Akuntansi':$p2=1;break;
						case '62301 - D4 Akuntansi':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'ALAT MESIN PERTANIAN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'KONTROL MEKANIK':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'KONTROL PROSES':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'KONTRUKSI BADAN PESAWAT UDARA':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'KONTRUKSI RANGKA PESAWAT UDARA':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PEMESINAN PESAWAT UDARA':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '62401 - D3 Akuntansi':$p1=1;break;
						case '62301 - D4 Akuntansi':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '62401 - D3 Akuntansi':$p2=1;break;
						case '62301 - D4 Akuntansi':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK ALAT BERAT':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK DAN MANAJEMEN INDUSTRI':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '22401 - D3 Teknik Sipil':$p1=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p1=1;break;
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '22401 - D3 Teknik Sipil':$p2=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p2=1;break;
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK DAN MANAJEMEN PRODUKSI':
					switch ($prodi1) {
						case '22401 - D3 Teknik Sipil':$p1=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p1=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p1=1;break;
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '22401 - D3 Teknik Sipil':$p2=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p2=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p2=1;break;
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK ENERGI BIOMASSA':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK ENERGI HIDRO':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK ENERGI SURYA DAN ANGIN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK FABRIKASI LOGAM':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK GAMBAR MESIN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK GAMBAR RANCANG BANGUN KAPAL':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK INDUSTRI':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK INSTALASI PEMESINAN KAPAL':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK KAPAL NIAGA':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK KENDARAAN RINGAN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK KETENAGALISTRIKAN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK KIMIA':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK KONTRUKSI KAPAL BAJA':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK KONTRUKSI KAPAL FIBERGLASS':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK KONTRUKSI KAPAL KAYU':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK MEKATRONIKA':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK MESIN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK OTOMASI INDUSTRI':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK OTOMOTIF':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK OTOMOTIF ALAT BERAT':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK OTOMOTIF KENDARAAN RINGAN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK OTOMOTIF SEPEDA MOTOR':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK OTROTONIK':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PEMBANGKIT TENAGA LISTRIK':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PEMBORAN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PEMBORAN MINYAK':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PEMBORAN MINYAK DAN GAS':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PEMELIHARAAN MEKANIK INDUSTRI':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PEMESINAN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PENDINGIN DAN TATA UDARA':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PENGECORAN LOGAM':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PENGELASAN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PENGELASAN KAPAL':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PENGOLAHAN MINYAK DAN GAS':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PENGOLAHAN MINYAK, GAS DAN PETROKIMIA':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PERBAIKAN BODI OTOMOTIF':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PERKAPALAN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PERMINYAKAN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PLAMBING DAN SANITASI':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PRODUKSI PERMINYAKAN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK SEPEDA MOTOR':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIKA KAPAL NIAGA':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIKA KAPAL PENANGKAP IKAN':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNOLOGI PESAWAT UDARA':
					switch ($prodi1) {
						case '21401 - D3 Teknik Mesin':$p1=1;break;
						case '21307 - D4 Teknik Manufaktur':$p1=1;break;
						case '21413 - D3 Teknik Alat Berat':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '21401 - D3 Teknik Mesin':$p2=1;break;
						case '21307 - D4 Teknik Manufaktur':$p2=1;break;
						case '21413 - D3 Teknik Alat Berat':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'GEOLOGI PERTAMBANGAN':
					switch ($prodi1) {
						case '22401 - D3 Teknik Sipil':$p1=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p1=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p1=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '22401 - D3 Teknik Sipil':$p2=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p2=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p2=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK FURNITUR':
					switch ($prodi1) {
						case '22401 - D3 Teknik Sipil':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '22401 - D3 Teknik Sipil':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK GAMBAR BANGUNAN':
					switch ($prodi1) {
						case '22401 - D3 Teknik Sipil':$p1=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p1=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p1=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '22401 - D3 Teknik Sipil':$p2=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p2=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p2=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK KONSTRUKSI BAJA':
					switch ($prodi1) {
						case '22401 - D3 Teknik Sipil':$p1=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p1=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p1=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '22401 - D3 Teknik Sipil':$p2=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p2=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p2=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK KONSTRUKSI BATU DAN BETON':
					switch ($prodi1) {
						case '22401 - D3 Teknik Sipil':$p1=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p1=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p1=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '22401 - D3 Teknik Sipil':$p2=1;break;
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p2=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p2=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK KONSTRUKSI KAYU':
					switch ($prodi1) {
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p1=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p1=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p2=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p2=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK SURVEI DAN PEMETAAN':
					switch ($prodi1) {
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p1=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p1=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '22301 - D4 Perancangan Jalan dan Jembatan':$p2=1;break;
						case '22302 - D4 Manajemen Rekayasa Konstruksi':$p2=1;break;
						case '22304 - D4 Teknik Perencanaan Irigasi Dan Rawa':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'ELEKTRONIKA PESAWAT UDARA':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'KELISTRIKAN PESAWAT UDARA':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PEMELIHARAAN DAN PERBAIKAN INSTRUMEN ELEKTRONIKA':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PEMELIHARAAN DAN PERBAIKAN INSTRUMEN ELEKTRONIKA PESAWAT UDARA':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'REKAYASA PERANGKAT LUNAK':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK ELEKTRONIKA':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK ELEKTRONIKA INDUSTRI':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK ELEKTRONIKA KOMUNIKASI':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p1=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p2=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK KOMPUTER DAN INFORMATIKA':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p1=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p2=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK KOMPUTER DAN JARINGAN':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p1=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p1=1;break;
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p2=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p2=1;break;
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK TELEKOMUNIKASI':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p1=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p2=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK TRANSMISI TELEKOMUNIKASI':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p1=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p2=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNOLOGI INFORMASI DAN KOMUNIKASI':
					switch ($prodi1) {
						case '20401 - D3 Teknik Elektronika':$p1=1;break;
						case '20301 - D4 Teknik Elektronika':$p1=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p1=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20401 - D3 Teknik Elektronika':$p2=1;break;
						case '20301 - D4 Teknik Elektronika':$p2=1;break;
						case '20402 - D3 Teknik Telekomunikasi':$p2=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK AUDIO VIDEO':
					switch ($prodi1) {
						case '20402 - D3 Teknik Telekomunikasi':$p1=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20402 - D3 Teknik Telekomunikasi':$p2=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK BROADCASTING':
					switch ($prodi1) {
						case '20402 - D3 Teknik Telekomunikasi':$p1=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20402 - D3 Teknik Telekomunikasi':$p2=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK TRANSMISI':
					switch ($prodi1) {
						case '20402 - D3 Teknik Telekomunikasi':$p1=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p1=1;break;
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20402 - D3 Teknik Telekomunikasi':$p2=1;break;
						case '20302 - D4 Teknik Telekomunikasi':$p2=1;break;
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'INSTRUMENTASI INDUSTRI':
					switch ($prodi1) {
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'KELISTRIKAN KAPAL':
					switch ($prodi1) {
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'KELISTRIKAN PESAWAT UDARA':
					switch ($prodi1) {
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PEMELIHARAAN DAN PERBAIKAN MOTOR DAN RANGKA PESAWAT UDARA':
					switch ($prodi1) {
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK DISTRIBUSI TENAGA LISTRIK':
					switch ($prodi1) {
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK INSTALASI PEMANFAATAN TENAGA LISTRIK':
					switch ($prodi1) {
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK INSTALASI TENAGA LISTRIK':
					switch ($prodi1) {
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK JARINGAN TENAGA LISTRIK':
					switch ($prodi1) {
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK TRANSMISI TENAGA LISTRIK':
					switch ($prodi1) {
						case '20403 - D3 Teknik Listrik':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '20403 - D3 Teknik Listrik':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK JARINGAN AKSES':
					switch ($prodi1) {
						case '56401 - D3 Teknik Komputer':$p1=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '56401 - D3 Teknik Komputer':$p2=1;break;
						case '58301 - D4 Teknologi Rekayasa Perangkat Lunak':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'ANIMASI':
					switch ($prodi1) {
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'DESAIN KOMUNIKASI VISUAL':
					switch ($prodi1) {
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'MULTIMEDIA':
					switch ($prodi1) {
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PRODUKSI DAN PENYIARAN PROGRAM RADIO':
					switch ($prodi1) {
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PRODUKSI DAN PENYIARAN PROGRAM PERTELEVISIAN':
					switch ($prodi1) {
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PRODUKSI DAN PENYIARAN PROGRAM RADIO DAN PERTELEVISIAN':
					switch ($prodi1) {
						case '57401 - D3 Manajemen Informatika':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '57401 - D3 Manajemen Informatika':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'ADMINISTRASI':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '62401 - D3 Akuntansi':$p1=1;break;
						case '62301 - D4 Akuntansi':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '62401 - D3 Akuntansi':$p2=1;break;
						case '62301 - D4 Akuntansi':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'ADMINISTRASI PERKANTORAN':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '62401 - D3 Akuntansi':$p1=1;break;
						case '62301 - D4 Akuntansi':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '62401 - D3 Akuntansi':$p2=1;break;
						case '62301 - D4 Akuntansi':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'AGRIBISNIS ANEKA TERNAK':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'AGRIBISNIS DAN AGROINDUSTRI':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'AKUNTANSI':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '62401 - D3 Akuntansi':$p1=1;break;
						case '62301 - D4 Akuntansi':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '62401 - D3 Akuntansi':$p2=1;break;
						case '62301 - D4 Akuntansi':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'BISNIS DAN MANAJEMEN':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '62401 - D3 Akuntansi':$p1=1;break;
						case '62301 - D4 Akuntansi':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '62401 - D3 Akuntansi':$p2=1;break;
						case '62301 - D4 Akuntansi':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'KEUANGAN':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '62401 - D3 Akuntansi':$p1=1;break;
						case '62301 - D4 Akuntansi':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '62401 - D3 Akuntansi':$p2=1;break;
						case '62301 - D4 Akuntansi':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PEMASARAN':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '62401 - D3 Akuntansi':$p1=1;break;
						case '62301 - D4 Akuntansi':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '62401 - D3 Akuntansi':$p2=1;break;
						case '62301 - D4 Akuntansi':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PENGAWASAN MUTU':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PENGAWASAN MUTU HASIL PERTANIAN':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PENGAWASAN MUTU HASIL PERTANIAN DAN PERIKANAN':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PERBANKAN':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '62401 - D3 Akuntansi':$p1=1;break;
						case '62301 - D4 Akuntansi':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '62401 - D3 Akuntansi':$p2=1;break;
						case '62301 - D4 Akuntansi':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PERBANKAN SYARIAH':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '62401 - D3 Akuntansi':$p1=1;break;
						case '62301 - D4 Akuntansi':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '62401 - D3 Akuntansi':$p2=1;break;
						case '62301 - D4 Akuntansi':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PRODUKSI GRAFIKA':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TATA NIAGA':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK DAN MANAJEMEN PERGUDANGAN':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'TEKNIK PERGUDANGAN':
					switch ($prodi1) {
						case '63411 - D3 Administrasi Bisnis':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '63411 - D3 Administrasi Bisnis':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'BAHASA':
					switch ($prodi1) {
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'AKOMODASI PERHOTELAN':
					switch ($prodi1) {
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PARIWISATA':
					switch ($prodi1) {
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'PATISERI':
					switch ($prodi1) {
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			case 'USAHA PERJALANAN WISATA':
					switch ($prodi1) {
						case '93401 - D3 Usaha Perjalanan Wisata':$p1=1;break;
						case '79402 - D3 Bahasa Inggris':$p1=1;break;
						default:$p1=0;break;
					}
					switch ($prodi2) {
						case '93401 - D3 Usaha Perjalanan Wisata':$p2=1;break;
						case '79402 - D3 Bahasa Inggris':$p2=1;break;
						default:$p2=0;break;
					}
				break;
			default:break;
		}
		if ($p1==1 && $p2==1) {
			$p='Valid';
		} else {
			$p='Tidak Valid';
		}
		return $p;
	}
	
?>