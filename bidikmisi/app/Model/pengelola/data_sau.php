<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class data_sau extends Model
{
    protected $table = 'data_sau';
    protected $primaryKey = 'nisn';
    protected $fillable = [
    	'nisn', 'jml_saubiaya', 'jml_saukandung', 'jml_sautiri','anak_ke','sum_biaya', 'tmp_tinggal', 'notelp', 'notelp_sau', 'hub', 'status'
    ];
}
