<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class rumah extends Model
{
	protected $primaryKey = 'nisn';
    protected $fillable = ['nisn','kepemilikan','kondisi_rmh','thn_peroleh','sumber_listrik','luas_tanah','luas_bangunan','sumber_air','mck','jrk_pusat_kota','jarak_rmh_sekolah','sarana','tgl_daftar','jml_sawah','jml_kebun','jml_kolom','jml_kerbau','jml_sapi','jml_kuda','jml_kambing','jml_ayam','jml_itik','jml_mobil','jml_motor','kendaraan_lain','pendapat','status'];
   
}
