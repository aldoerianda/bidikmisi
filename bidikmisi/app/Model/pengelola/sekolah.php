<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class sekolah extends Model
{
	protected $primaryKey = 'nisn';
    protected $fillable = ['nisn', 'npsn', 'nama_sklh', 'akreditasi', 'prov_sklh', 'kota_sklh', 'alamat_se', 'thn_lulus', 'jurusan', 'prog_keahlian', 'nama_kepsek', 'nohp_kepsek', 'notelp_se', 'fax', 'website', 'email_se', 'latitudesekolah','longisekolah', 'tgl_daftar', 'status'];
}
