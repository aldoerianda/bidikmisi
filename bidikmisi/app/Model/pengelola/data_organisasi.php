<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class data_organisasi extends Model
{
    protected $table = 'data_organisasi';
    protected $primaryKey = 'id_organisasi';
    protected $fillable = [
    	'nisn', 'temp_orga', 'nama_orga','jabatan'
    ];
}
