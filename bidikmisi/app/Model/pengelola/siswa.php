<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class siswa extends Model
{
	protected $primaryKey = 'nisn';
	protected $fillable = [
		'id_visitor1', 'id_visitor2', 'nisn', 'nama_siswa', 'tempat_lahir', 'tgl_lahir', 'agama', 'jekel', 'alamat', 'tgl_daftar', 'email', 'latitudermh', 'longitudermh', 'status', 'no_hp', 'no_telp'];
}
