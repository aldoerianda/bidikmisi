<?php

namespace App\Model\pengelola;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class visitor extends Authenticatable implements JWTSubject
{
	use Notifiable;

    protected $table ='visitor';
    protected $primaryKey = 'id_visitor';
    protected $fillable = [
        'username', 'password', 'nidn',
    ];
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
