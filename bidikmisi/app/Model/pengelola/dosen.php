<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class dosen extends Authenticatable
{
	use Notifiable;
	protected $primaryKey ='nidn';
	protected $fillable = [
		'nidn', 'nama_dosen', 'username','password','tempat_lahir', 'tgl_lahir', 'jekel', 'no_hp', 'alamat'];

		protected $hidden = ['password', 'remember_token',
	];

}
