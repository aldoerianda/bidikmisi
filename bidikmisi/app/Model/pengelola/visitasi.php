<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class visitasi extends Model
{
    protected $fillable = ['nisn', 'nidn','nidn2', 'tgl_visit', 'keterangan'];
}
