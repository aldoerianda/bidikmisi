<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class penghasilan_ortu extends Model
{
	protected $primaryKey = 'nisn';
    protected $fillable = [
    		'nisn','nama_ibu_kdg','nama_ayah','status_ayah','status_ibu','usia_ayah','usia_ibu','pend_ayah','pend_ibu','pek_ayah','pek_ibu','peng_ayah','peng_ibu','income','jmlh_tanggungan','rata_biaya','tgl_daftar','status_nikah','hub_ibu','hub_ayah','status'
    ];
}
