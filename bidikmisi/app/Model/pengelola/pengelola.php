<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Tymon\JWTAuth\Contracts\JWTSubject;

class pengelola extends Authenticatable implements JWTSubject
{
    use Notifiable;
    protected $table = 'data_login';
    protected $fillable = [
    	'username', 'password', 'nidn', 'level'
    ];
   protected $hidden = ['password', 'remember_token',
	];
	public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
