<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class data_catatan extends Model
{
    protected $table = 'data_catatan';
    protected $primaryKey = 'nisn';
    protected $fillable = [
    	'nisn', 'pemahaman1', 'pemahaman2', 'harapan1','harapan2','catatan', 'ijazah', 'rapor', 'sertifikat', 'kk', 'surat_peng','bpjs','kis','kip','ksks','raskin','pbb','bukti_listrik','surat_data','penilaian','status'
    ];
}
