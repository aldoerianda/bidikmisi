<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class dataket_sau extends Model
{
    protected $table = 'dataket_sau';
    protected $primaryKey = 'id_ket';
    protected $fillable = [
    	'nisn', 'nama_sau', 'umur_sau','pekerjaan_sau', 'status_hub'
    ];
}
