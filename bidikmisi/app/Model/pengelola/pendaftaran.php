<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class pendaftaran extends Model
{
    protected $fillable = ['no_pend', 'nisn', 'tgl_daftar', 'jenis_seleksi', 'jur_sekolah', 'pil_prodi_1', 'pil_prodi_2'];
}
