<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class data_foto extends Model
{
    protected $table = 'data_foto';
    protected $primaryKey = 'id_foto';
    protected $fillable = [
    	'nisn', 'nama_foto', 'extensi'
    ];
}
