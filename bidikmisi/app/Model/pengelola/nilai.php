<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class nilai extends Model
{
	protected $primaryKey = 'nisn';
    protected $fillable = ['nisn','nama_siswa','nama_sklh','rang_sem1','nilai_rata_sem1','jml_siswakelas_sem1','jml_kelasju_sem1','jml_kelasse_sem1','rang_sem2','nilai_rata_sem2','jml_siswakelas_sem2','jml_kelasju_sem2','jml_kelasse_sem2','rang_sem3','nilai_rata_sem3','jml_siswakelas_sem3','jml_kelasju_sem3','jml_kelasse_sem3','rang_smst_4','ttl_nilai_smst_4','jmlh_mapel_smst_4','nilai_rata_sem4','jml_siswakelas_sem4','jml_kelasju_sem4','jml_kelasse_sem4','rang_smst_5','ttl_nilai_smst_5','jmlh_mapel_smst_5','nilai_rata_sem5','jml_siswakelas_sem5','jml_kelasju_sem5','jml_kelasse_sem5','rang_smst_6','ttl_nilai_smst_6','jmlh_mapel_smst_6','nilai_rata_sem6','jml_kelasju_sem6','jml_kelasse_sem6','nilai_unas','jmlh_mapel_unas','pres_ekskul','jml_prestasi','tgl_daftar','status'];
 
}
