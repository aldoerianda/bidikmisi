<?php

namespace App\Model\pengelola;

use Illuminate\Database\Eloquent\Model;

class skorfuzzy extends Model
{
    protected $table = 'skor_fuzzy';
    protected $primaryKey = 'nisn';
    protected $fillable = [
    	'nisn','nama','alamat','income','dependent','outeconomic','assessment','achievment','outacademic','homeower','sanitation','marital','sourcewater','outhousehold','land','house','outarea','outdecision','time','tgl_skrg','durasi',
    ];
}
