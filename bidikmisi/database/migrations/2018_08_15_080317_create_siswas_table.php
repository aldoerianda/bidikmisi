<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nisn',15)->unique();
            $table->string('nama_siswa',50);
            $table->string('tempat_lahir',30);
            $table->date('tgl_lahir');
            $table->string('jekel',10);
            $table->string('no_hp',15);
            $table->string('no_telp',15);
            $table->text('alamat');
            $table->datetime('tgl_daftar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
