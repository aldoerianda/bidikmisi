<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRumahsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rumahs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nisn',15);
            $table->string('kepemilikan',25);
            $table->year('thn_peroleh');
            $table->string('sumber_listrik',20);
            $table->string('luas_tanah',20);
            $table->string('luas_bangunan',20);
            $table->string('sumber_air',20);
            $table->string('mck',50);
            $table->integer('jrk_pusat_kota');
            $table->datetime('tgl_daftar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rumahs');
    }
}
