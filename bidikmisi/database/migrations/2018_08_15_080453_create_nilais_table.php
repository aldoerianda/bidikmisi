<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNilaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilais', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nisn',15);
            $table->string('nama_siswa',100);
            $table->string('nama_sklh',100);
            $table->integer('rang_smst_4');
            $table->float('ttl_nilai_smst_4');
            $table->integer('jmlh_mapel_smst_4');
            $table->integer('rang_smst_5');
            $table->float('ttl_nilai_smst_5');
            $table->integer('jmlh_mapel_smst_5');
            $table->string('rang_smst_6',20);
            $table->string('ttl_nilai_smst_6',20);
            $table->string('jmlh_mapel_smst_6',20);
            $table->string('nilai_unas',20);
            $table->string('jmlh_mapel_unas',20);
            $table->text('pres_ekskul');
            $table->datetime('tgl_daftar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilais');
    }
}
