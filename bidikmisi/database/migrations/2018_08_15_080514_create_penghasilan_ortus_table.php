<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePenghasilanOrtusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penghasilan_ortus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nisn',15);
            $table->string('nama_ibu_kdg',50);
            $table->string('status_ayah',20);
            $table->string('status_ibu',20);
            $table->string('pek_ayah',25);
            $table->string('pek_ibu',25);
            $table->string('peng_ayah',50);
            $table->string('peng_ibu',50);
            $table->integer('jmlh_tanggungan');
            $table->datetime('tgl_daftar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penghasilan_ortus');
    }
}
