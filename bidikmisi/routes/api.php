<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$api = app('Dingo\Api\Routing\Router');
$api->version('v1', ['namespace' => 'App\Http\Controllers'], function($api) {
	$api->post('login', 'Api\ApiController@login');
	//$api->post('loginPengelola', 'Api\ApiController@loginPengelola');

	$api->get('pengelolas', 'Api\ApiController@indexPengelola');
	$api->post('pengelolas', 'Api\ApiController@storePengelola');
	$api->put('pengelolas/{id}', 'Api\ApiController@updatePengelola');
	$api->delete('pengelolas/{id}', 'Api\ApiController@deletePengelola');

	$api->get('dosens', 'Api\ApiController@indexDosen');
	$api->post('dosens', 'Api\ApiController@storeDosen');
	$api->put('dosens/{nidn}', 'Api\ApiController@updateDosen');
	$api->delete('dosens/{nidn}', 'Api\ApiController@deleteDosen');

	$api->get('siswas', 'Api\ApiController@indexSiswa');
	$api->post('siswas', 'Api\ApiController@storeSiswa');
	$api->put('siswas/{nisn}', 'Api\ApiController@updateSiswa');
	$api->delete('siswas/{nisn}', 'Api\ApiController@deleteSiswa');

	$api->get('pendaftarans', 'Api\ApiController@indexPendaftaran');
	$api->post('pendaftarans', 'Api\ApiController@storePendaftaran');
	$api->put('pendaftarans/{id}', 'Api\ApiController@updatePendaftaran');
	$api->delete('pendaftarans/{id}', 'Api\ApiController@deletePendaftaran');

	$api->get('sekolahs', 'Api\ApiController@indexSekolah');
	$api->post('sekolahs', 'Api\ApiController@storeSekolah');
	$api->put('sekolahs/{nisn}', 'Api\ApiController@updateSekolah');
	$api->delete('sekolahs/{nisn}', 'Api\ApiController@deleteSekolah');

	$api->get('visitasis', 'Api\ApiController@indexVisitasi');
	$api->post('visitasis', 'Api\ApiController@storeVisitasi');
	$api->put('visitasis/{id}', 'Api\ApiController@updateVisitasi');
	$api->delete('visitasis/{id}', 'Api\ApiController@deleteVisitasi');

	$api->get('visitors', 'Api\ApiController@indexVisitor');
	$api->post('visitors', 'Api\ApiController@storeVisitor');
	$api->put('visitors/{id_visitor}', 'Api\ApiController@updateVisitor');
	$api->delete('visitors/{id_visitor}', 'Api\ApiController@deleteVisitor');

	$api->get('penghasilan_ortus', 'Api\ApiController@indexPenghasilanOrtu');
	$api->post('penghasilan_ortus', 'Api\ApiController@storePenghasilanOrtu');
	$api->put('penghasilan_ortus/{nisn}', 'Api\ApiController@updatePenghasilanOrtu');
	$api->delete('penghasilan_ortus/{nisn}', 'Api\ApiController@deletePenghasilanOrtu');

	$api->get('nilais', 'Api\ApiController@indexNilai');
	$api->post('nilais', 'Api\ApiController@storeNilai');
	$api->put('nilais/{nisn}', 'Api\ApiController@updateNilai');
	$api->delete('nilais/{nisn}', 'Api\ApiController@deleteNilai');

	$api->get('rumahs', 'Api\ApiController@indexRumah');
	$api->post('rumahs', 'Api\ApiController@storeRumah');
	$api->put('rumahs/{nisn}', 'Api\ApiController@updateRumah');
	$api->delete('rumahs/{nisn}', 'Api\ApiController@deleteRumah');

	$api->get('datasau', 'Api\ApiController@indexDatasau');
	$api->post('datasau', 'Api\ApiController@storeDatasau');
	$api->put('datasau/{nisn}', 'Api\ApiController@updateDatasau');
	$api->delete('datasau/{nisn}', 'Api\ApiController@deleteDatasau');

	$api->get('dataketsau', 'Api\ApiController@indexDataketsau');
	$api->post('dataketsau', 'Api\ApiController@storeDataketsau');
	$api->put('dataketsau/{id_ket}', 'Api\ApiController@updateDataketsau');
	$api->delete('dataketsau/{id_ket}', 'Api\ApiController@deleteDataketsau');

	$api->get('datacatatan', 'Api\ApiController@indexCatatan');
	$api->post('datacatatan', 'Api\ApiController@storeCatatan');
	$api->put('datacatatan/{nisn}', 'Api\ApiController@updateCatatan');
	$api->delete('datacatatan/{nisn}', 'Api\ApiController@deleteCatatan');

	$api->get('datafoto', 'Api\ApiController@indexDatafoto');
	$api->post('datafoto', 'Api\ApiController@storeDatafoto');
	$api->put('datafoto/{id_foto}', 'Api\ApiController@updateDatafoto');
	$api->delete('datafoto/{id_foto}', 'Api\ApiController@deleteDatafoto');

	$api->get('dataorganisasi', 'Api\ApiController@indexOrganisasi');
	$api->post('dataorganisasi', 'Api\ApiController@storeOrganisasi');
	$api->put('dataorganisasi/{id_organisasi}', 'Api\ApiController@updateOrganisasi');
	$api->delete('dataorganisasi/{id_organisasi}', 'Api\ApiController@deleteOrganisasi');
});