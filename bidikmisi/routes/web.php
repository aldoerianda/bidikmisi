<?php
//User Routes
Route::group(['namespace'=>'User'],function(){
	Route::get('/','HomeController@index')->name('user.home');
	Route::get('berita/{slug}','HomeController@show')->name('berita');
	Route::resource('user/jadwal_visitasi','VisitasiController');
	Route::resource('user/banyaksiswa_persekolah','BanyakSiswaPerSekolahController');
	Route::post('user/banyaksiswa_persekolahshow','BanyakSiswaPerSekolahController@show')->name('banyaksiswa_persekolah.show');
	Route::resource('user/banyaksiswa_perkota','BanyakSiswaPerKotaController');
	Route::post('user/banyaksiswa_perkotashow','BanyakSiswaPerKotaController@show')->name('banyaksiswa_perkota.show');
	Route::resource('user/banyaksiswa_perprovinsi','BanyakSiswaPerProvinsiController');
	Route::post('user/banyaksiswa_perprovinsishow','BanyakSiswaPerProvinsiController@show')->name('banyaksiswa_perprovinsi.show');
	Route::resource('user/laravelgooglechart','LaravelGoogleGraphController');
	Route::post('user/laravelgooglechartshow','LaravelGoogleGraphController@show')->name('laravelgooglechart.show');
	Route::resource('user/laravelgooglechart2','LaravelGoogleGraph2Controller');
	Route::post('user/laravelgooglechart2show','LaravelGoogleGraph2Controller@show')->name('laravelgooglechart2.show');
	Route::resource('user/prestasi_ekskul','PrestasiEkskulController');
	Route::post('user/prestasi_ekskulshow','PrestasiEkskulController@show')->name('prestasi_ekskul.show');
	Route::resource('user/validasi_nilai','ValidasiNilaiController');
	Route::post('user/validasi_nilaishow','ValidasiNilaiController@show')->name('validasi_nilai.show');
	Route::resource('user/validasi_jurusan','ValidasiJurusanController');
	Route::post('user/validasi_jurusanshow','ValidasiJurusanController@show')->name('validasi_jurusan.show');
});
//Dosen Route
Route::group(['namespace'=>'Dosen'],function(){
	Route::get('dosen/home','HomeController@index')->name('dosen.home');
	Route::resource('dosen/data-dosen','DosenController');
	Route::resource('dosen/data-nilai','NilaiController');
	Route::post('dosen/data-nilai-show','NilaiController@show')->name('data-nilai.show');
	Route::resource('dosen/data-pendaftaran','PendaftaranController');
	Route::post('dosen/data-pendaftaran-show','PendaftaranController@show')->name('data-pendaftaran.show');
	Route::resource('dosen/data-penghasilan_ortu','Penghasilan_OrtuController');
	Route::post('dosen/data-penghasilan_ortu-show','Penghasilan_OrtuController@show')->name('data-penghasilan_ortu.show');
	Route::resource('dosen/data-rumah','RumahController');
	Route::post('dosen/data-rumah-show','RumahController@show')->name('data-rumah.show');
	Route::resource('dosen/data-sekolah','SekolahController');
	Route::post('dosen/data-sekolah-show','SekolahController@show')->name('data-sekolah.show');
	Route::resource('dosen/data-siswa','SiswaController');
	Route::post('dosen/data-siswa-show','SiswaController@show')->name('data-siswa.show');
	Route::resource('dosen/data-visitasi','VisitasiController');

	 //Auth Dosen
	//Route::get('/signinvisitor', 'Auth\LoginController@showLoginForm')->name('signinvisitor');
	//Route::post('/signinvisitor', 'Auth\LoginController@login');
	//Route::post('logout','Auth\LoginController@logout')->name('logout');
});
//Pengelola atau Admin Route
Route::group(['namespace'=>'Pengelola'],function(){
	Route::get('pengelola/home','HomeController@index')->name('pengelola.home');
	Route::get('pengelola/ieds','IedsController@index')->name('pengelola.ieds');
	Route::get('pengelola/skorfuzzy','SkorfuzzyController@index')->name('pengelola.skorfuzzy');
	Route::get('pengelola/importfuzzy','SkorfuzzyController@indexImport')->name('pengelola.importfuzzy');
	Route::post('pengelola/kuotafuzzy','SkorfuzzyController@kuota')->name('pengelola.kuotafuzzy');
	Route::resource('pengelola/dosen','DosenController');
	Route::resource('pengelola/user','UserController');
	Route::resource('pengelola/datasau','SaudaraController');
	Route::resource('pengelola/dataketsau','KetsaudaraController');
	Route::resource('pengelola/datafoto','DatafotoController');
	Route::resource('pengelola/organisasi','OrganisasiController');
	Route::resource('pengelola/catatan','CatatanController');
	Route::resource('pengelola/berita','BeritaController');
	Route::resource('pengelola/dosen','DosenController');
	Route::resource('pengelola/nilai','NilaiController');
	Route::post('pengelola/nilaipertahun','NilaiController@show')->name('nilai.pertahun');
	Route::resource('pengelola/pendaftaran','PendaftaranController');
	Route::post('pengelola/pendaftaranpertahun','PendaftaranController@show')->name('pendaftaran.pertahun');
	Route::resource('pengelola/pengelola','PengelolaController');
	Route::resource('pengelola/penghasilan_ortu','Penghasilan_OrtuController');
	Route::post('pengelola/penghasilan_ortupertahun','Penghasilan_OrtuController@show')->name('penghasilan_ortu.pertahun');
	Route::resource('pengelola/rumah','RumahController');
	Route::post('pengelola/rumahpertahun','RumahController@show')->name('rumah.pertahun');
	Route::resource('pengelola/sekolah','SekolahController');
	Route::post('pengelola/sekolahpertahun','SekolahController@show')->name('sekolah.pertahun');
	Route::resource('pengelola/siswa','SiswaController');
	Route::post('pengelola/siswapertahun','SiswaController@show')->name('siswa.pertahun');
	Route::resource('pengelola/visitasi','VisitasiController');
	Route::resource('pengelola/prestasiekskul','PrestasiEkskulController');
	Route::post('pengelola/prestasiekskulshow','PrestasiEkskulController@show')->name('prestasiekskul.show');
	Route::resource('pengelola/banyaksiswapersekolah','BanyakSiswaPerSekolahController');
	Route::post('pengelola/banyaksiswapersekolahshow','BanyakSiswaPerSekolahController@show')->name('banyaksiswapersekolah.show');
	Route::resource('pengelola/banyaksiswaperprovinsi','BanyakSiswaPerProvinsiController');
	Route::post('pengelola/banyaksiswaperprovinsishow','BanyakSiswaPerProvinsiController@show')->name('banyaksiswaperprovinsi.show');
	Route::resource('pengelola/siswaperprodi','SiswaPerProdiController');
	Route::post('pengelola/siswaperprodishow','SiswaPerProdiController@show')->name('siswaperprodi.show');
	Route::resource('pengelola/banyaksiswaperkota','BanyakSiswaPerKotaController');
	Route::post('pengelola/banyaksiswaperkotashow','BanyakSiswaPerKotaController@show')->name('banyaksiswaperkota.show');
	Route::resource('pengelola/validasinilai','DataNilaiValidController');
	Route::post('pengelola/validasinilaishow','DataNilaiValidController@show')->name('validasinilai.show');
	Route::resource('pengelola/datanilaitidakvalid','DataNilaiTidakValidController');
	Route::resource('pengelola/laravel_google_chart', 'LaravelGoogleGraphController');
	Route::post('pengelola/laravel_google_chartshow','LaravelGoogleGraphController@show')->name('laravel_google_chart.show');
	Route::resource('pengelola/laravel_google_chart2', 'LaravelGoogleGraph2Controller');
	Route::post('pengelola/laravel_google_chart2show','LaravelGoogleGraph2Controller@show')->name('laravel_google_chart2.show');
	Route::resource('pengelola/validasijurusan','ValidasiJurusanController');
	Route::post('pengelola/validasijurusanshow','ValidasiJurusanController@show')->name('validasijurusan.show');

	Route::resource('pengelola/validasipenghasilan','ValidasiPenghasilanController');

	Route::get('pengelola/validatejurusan/validasi','ValidasiJurusanController@vld');
	Route::post('pengelola/validpenghasilan', 'ValidasiPenghasilanController@vld');
	Route::get('pengelola/validatenilai/validasi','DataNilaiValidController@vld');
	Route::resource('pengelola/jurusan', 'UpdateJurusanController');
	
	//Import Export Data Routes
	Route::get('pengelola/iofile','ImportExportController@index')->name('pengelola.importexport');
	Route::post('importsiswa', 'SiswaController@siswaImport')->name('siswa.import');
	Route::get('exportsiswa', 'SiswaController@siswaExport')->name('siswa.export');
	Route::post('importdosen', 'DosenController@Import')->name('dosen.import');
	Route::get('exportdosen', 'DosenController@Export')->name('dosen.export');
	Route::post('importvisitasi', 'VisitasiController@Import')->name('visitasi.import');
	Route::get('exportvisitasi', 'VisitasiController@Export')->name('visitasi.export');
	Route::post('importfuzzy','SkorfuzzyController@Import')->name('fuzzy.import');

	//Auth Pengelola
	Route::get('/signin', 'Auth\LoginController@showLoginForm')->name('signin');
	Route::post('/signin', 'Auth\LoginController@login');
	Route::post('logout','Auth\LoginController@logout')->name('logout');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
