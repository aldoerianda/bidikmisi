@extends('dosen.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful animated fadeIn">
        <div class="panel-heading">
          <h3 class="panel-title">Data Siswa</h3>
        </div>
        <div class="panel-body">
          <form action="{{ route('data-siswa.show') }}" method="post" class="form-horizontal">
            {{ csrf_field()}}
            <div class="form-group">
              <div class="col-md-2">
                <br><select name="tahun" class="form-control select">
                  <option value="">== Pilih Tahun ==</option>
                  {{ $now = Carbon\carbon::now()->year }}
                  @for ($i = 2017; $i <= $now ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                  @endfor
                </select>              
              </div>
            </div>
            <input type="submit" class="btn btn-success" name="Cari" value="Cari">
          </form>
        </div>
        <!-- /.box-header -->
        <div class="panel-body table-responsive">
          <table class="table datatable">
            <thead>
              <tr>
                <th>No.</th>
                <th>NISN</th>
                <th>Nama Siswa</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>JK</th>
                <th>No. HP</th>
                <th>No. Telp</th>
                <th>Alamat</th>
                <th>Tanggal Daftar</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($siswas as $siswa)
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $siswa->nisn }}</td>
                <td>{{ $siswa->nama_siswa }}</td>
                <td>{{ $siswa->tempat_lahir }}</td>
                <td>{{ $siswa->tgl_lahir }}</td>
                <td>{{ $siswa->jekel }}</td>
                <td>{{ $siswa->no_hp }}</td>
                <td>{{ $siswa->no_telp }}</td>
                <td>{{ $siswa->alamat }}</td>
                <td>{{ $siswa->tgl_daftar }}</td>
              </tr>
              @endforeach
            </tbody>
          </table> 
        </div>
      </div>

    </div>
  </div>    <!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
@endsection