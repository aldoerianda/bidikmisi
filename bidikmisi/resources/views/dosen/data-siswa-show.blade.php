@extends('dosen.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          
          <div class="panel panel-colorful animated zoomIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Siswa</h3>
            </div>
            <!-- /.box-header -->
            <div class="panel-body table-responsive">
              <table class="table datatable">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NISN</th>
                  <th>Nama Siswa</th>
                  <th>Tempat Lahir</th>
                  <th>Tanggal Lahir</th>
                  <th>JK</th>
                  <th>No. HP</th>
                  <th>No. Telp</th>
                  <th>Alamat</th>
                  <th>Tanggal Daftar</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($siswas as $siswa)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $siswa->nisn }}</td>
                    <td>{{ $siswa->nama_siswa }}</td>
                    <td>{{ $siswa->tempat_lahir }}</td>
                    <td>{{ $siswa->tgl_lahir }}</td>
                    <td>{{ $siswa->jekel }}</td>
                    <td>{{ $siswa->no_hp }}</td>
                    <td>{{ $siswa->no_telp }}</td>
                    <td>{{ $siswa->alamat }}</td>
                    <td>{{ $siswa->tgl_daftar }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
      <div class="panel-footer">
        <a href='{{route('data-siswa.index')}}' class="btn btn-warning">Kembali</a>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection