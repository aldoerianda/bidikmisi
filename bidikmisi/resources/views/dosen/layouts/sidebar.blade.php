<!-- START PAGE SIDEBAR -->
<div class="page-sidebar">
  <!-- START X-NAVIGATION -->
  <ul class="x-navigation">
    <li class="xn-logo">
      <a href="{{ route('pengelola.home')}}"><span>SI BM PNP</span></a>
      <a href="#" class="x-navigation-control"></a>
    </li>
    <li class="xn-profile">
      <a href="#" class="profile-mini">
        <img src="{{asset('img/imgpeng.png')}}" alt="Profile"/>
      </a>
      <div class="profile">
        <div class="profile-image">
          <img src="{{asset('img/imgpeng.png')}}" alt="Profile"/>
        </div>
        <div class="profile-data">
          <div class="profile-data-name">Visitor</div>
          <div class="profile-data-title">Bidikmisi Politeknik Negeri Padang</div>
        </div>
      </div>                                                                        
    </li>
    <li>
      <a href="{{ route('dosen.home')}}">
        <span class="fa fa-desktop"></span>
        <span class="xn-text">Beranda</span>
      </a>
    </li>
    <li class="xn-openable">
      <a href="#">
        <span class="fa fa-fw fa-eye"></span>
        <span class="xn-text">Lihat Data</span>
      </a>
      <ul>
        <li><a href="{{ route('data-dosen.index')}}"><i class="xn-text"></i>Data Dosen</a></li>
          <li><a href="{{ route('data-siswa.index')}}"><i class="xn-text"></i>Data Siswa</a></li>
          <li><a href="{{ route('data-sekolah.index')}}"><i class="xn-text"></i>Data Sekolah</a></li>
          <li><a href="{{ route('data-rumah.index')}}"><i class="xn-text"></i>Data Rumah</a></li>
          <li><a href="{{ route('data-penghasilan_ortu.index')}}"><i class="xn-text"></i>Data Penghasilan Orangtua</a></li>
          <li><a href="{{ route('data-pendaftaran.index')}}"><i class="xn-text"></i>Data Pendaftaran</a></li>
          <li><a href="{{ route('data-nilai.index')}}"><i class="xn-text"></i>Data Nilai</a></li>
          <li><a href="{{ route('data-visitasi.index')}}"><i class="xn-text"></i>Data Visitasi</a></li>
     </ul>
   </li>
  <li class="xn-title">Session</li>
  <li>
    <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span><span>Logout</span></a>
  </li>
</ul>
<!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->
