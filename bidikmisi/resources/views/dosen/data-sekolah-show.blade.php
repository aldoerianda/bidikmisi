@extends('dosen.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful">
            <div class="panel-heading">
              <h3 class="panel-title">Data Sekolah</h3>
            </div>
            <!-- /.box-header -->
            <div class="panel-body">
              <table class="table datatable">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NISN</th>
                  <th>NPSN</th>
                  <th>Asal Sekolah</th>
                  <th>Provinsi Sekolah</th>
                  <th>Kota Sekolah</th>
                  <th>Tanggal Daftar</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($sekolahs as $sekolah)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $sekolah->nisn }}</td>
                    <td>{{ $sekolah->npsn }}</td>
                    <td>{{ $sekolah->nama_sklh }}</td>
                    <td>{{ $sekolah->prov_sklh }}</td>
                    <td>{{ $sekolah->kota_sklh }}</td>
                    <td>{{ $sekolah->tgl_daftar }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
      <div class="panel-footer">
        <a href='{{route('data-sekolah.index')}}' class="btn btn-warning">Kembali</a>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection