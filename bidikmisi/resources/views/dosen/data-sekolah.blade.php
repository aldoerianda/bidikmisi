@extends('dosen.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Data Sekolah</h3>
        </div>
        <div class="panel-body">
          <form action="{{ route('data-sekolah.show') }}" method="post" class="form-horizontal">
            {{ csrf_field()}}
            <div class="form-group">
              <div class="col-md-2">
                <br><select name="tahun" class="form-control">
                  <option value="">== Pilih Tahun ==</option>
                  {{ $now = Carbon\carbon::now()->year }}
                  @for ($i = 2017; $i <= $now ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                  @endfor
                </select>              
              </div>
            </div>
            <input type="submit" class="btn btn-success" name="Cari" value="Cari">
          </form>
        </div>
        <!-- /.box-header -->
        <div class="panel-body table-responsive">
          <table class="table datatable">
            <thead>
              <tr>
                <th>No.</th>
                <th>NISN</th>
                <th>NPSN</th>
                <th>Asal Sekolah</th>
                <th>Provinsi Sekolah</th>
                <th>Kota Sekolah</th>
                <th>Tanggal Daftar</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($sekolahs as $sekolah)
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $sekolah->nisn }}</td>
                <td>{{ $sekolah->npsn }}</td>
                <td>{{ $sekolah->nama_sklh }}</td>
                <td>{{ $sekolah->prov_sklh }}</td>
                <td>{{ $sekolah->kota_sklh }}</td>
                <td>{{ $sekolah->tgl_daftar }}</td>
              </tr>
              @endforeach
            </tbody>
          </table> 
        </div>
      </div>

    </div>
  </div>
</section>

</section>
<!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
@endsection