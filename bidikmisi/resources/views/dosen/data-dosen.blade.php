@extends('dosen.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <!-- Main content -->
      <div class="row">
        <div class="col-md-12">
          
          <div class="panel panel-colorful animated zoomIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Dosen</h3>
            </div>
            <!-- /.box-header -->
            <div class="panel-body table-responsive">
              <table class="table datatable">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NIDN</th>
                  <th>Nama Dosen</th>
                  <th>Tempat Lahir</th>
                  <th>Tanggal Lahir</th>
                  <th>JK</th>{{-- 
                  <th>No. HP</th>
                  <th>Alamat</th> --}}
                </tr>
                </thead>
                <tbody>
                  @foreach ($dosens as $dosen)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $dosen->nidn }}</td>
                    <td>{{ $dosen->nama_dosen }}</td>
                    <td>{{ $dosen->tempat_lahir }}</td>
                    <td>{{ $dosen->tgl_lahir }}</td>
                    <td>{{ $dosen->jekel }}</td>
                    {{-- <td>{{ $dosen->no_hp }}</td>
                    <td>{{ $dosen->alamat }}</td> --}}
              
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection