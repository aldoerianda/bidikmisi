@extends('dosen.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Rumah</h3>
            </div>
            <!-- /.box-header -->
            <div class="panel-body table-responsive">
              <table class="table datatable">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NISN</th>
                  <th>Kepemilikan</th>
                  <th>Tahun Peroleh</th>
                  <th>Sumber Listrik</th>
                  <th>Luas Tanah</th>
                  <th>Luas Bangunan</th>
                  <th>Sumber Air</th>
                  <th>MCK</th>
                  <th>Jarak Pusat Kota</th>
                  <th>Tanggal Daftar</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($rumahs as $rumah)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $rumah->nisn }}</td>
                    <td>{{ $rumah->kepemilikan }}</td>
                    <td>{{ $rumah->thn_peroleh }}</td>
                    <td>{{ $rumah->sumber_listrik }}</td>
                    <td>{{ $rumah->luas_tanah }}</td>
                    <td>{{ $rumah->luas_bangunan }}</td>
                    <td>{{ $rumah->sumber_air }}</td>
                    <td>{{ $rumah->mck }}</td>
                    <td>{{ $rumah->jrk_pusat_kota }}</td>
                    <td>{{ $rumah->tgl_daftar }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
      <div class="panel-footer">
        <a href='{{route('data-rumah.index')}}' class="btn btn-warning">Kembali</a>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection