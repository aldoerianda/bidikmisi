@extends('dosen.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">

      <div class="panel panel-colorful animated zoomIn">
        <div class="panel-heading">
          <h3 class="panel-title">Data Visitasi</h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body">
          <table class="table datatable">
            <thead>
              <tr>
                <th>No.</th>
                <th>NISN</th>
                <th>Nama Siswa</th>
                <th>Nama Dosen 1</th>
                <th>Nama Dosen 2</th>
                <th>Alamat</th>
                <th>Tanggal Visit</th>
                {{-- <th>Keterangan</th> --}}
              </tr>
            </thead>
            <tbody>
              @foreach ($visitasis as $data)
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $data->nisn }}</td>
                <td>{{ $data->nama_siswa }}</td>
                <td>{{ $data->nama_dosen1 }}</td>
                <td>{{ $data->nama_dosen2 }}</td>
                <td>{{ $data->alamat }}</td>
                <td>{{ $data->tgl_visit }}</td>
                {{-- <td>{{ $data->keterangan }}</td> --}}
              </tr>
              @endforeach
            </tbody>
          </table> 
        </div>
      </div>
    </div>
  </div>
  <!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection