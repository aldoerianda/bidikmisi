@extends('dosen.layouts.app')
@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Data Rumah</h3>
        </div>
        <div class="panel-body">
          <form action="{{ route('data-rumah.show') }}" method="post" class="form-horizontal">
            {{ csrf_field()}}
            <div class="form-group">
              <div class="col-md-2">
                <br><select name="tahun" class="form-control">
                  <option value="">== Pilih Tahun ==</option>
                  {{ $now = Carbon\carbon::now()->year }}
                  @for ($i = 2017; $i <= $now ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                  @endfor
                </select>              
              </div>
            </div>
            <input type="submit" class="btn btn-success" name="Cari" value="Cari">
          </form>
        </div>
        <!-- /.box-header -->
        <div class="panel-body">
          <table class="table datatable">
            <thead>
              <tr>
                <th>No.</th>
                <th>NISN</th>
                <th>Kepemilikan</th>
                <th>Tahun Peroleh</th>
                <th>Sumber Listrik</th>
                <th>Luas Tanah</th>
                <th>Luas Bangunan</th>
                <th>Sumber Air</th>
                <th>MCK</th>
                <th>Jarak Pusat Kota</th>
                <th>Tanggal Daftar</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($rumahs as $rumah)
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $rumah->nisn }}</td>
                <td>{{ $rumah->kepemilikan }}</td>
                <td>{{ $rumah->thn_peroleh }}</td>
                <td>{{ $rumah->sumber_listrik }}</td>
                <td>{{ $rumah->luas_tanah }}</td>
                <td>{{ $rumah->luas_bangunan }}</td>
                <td>{{ $rumah->sumber_air }}</td>
                <td>{{ $rumah->mck }}</td>
                <td>{{ $rumah->jrk_pusat_kota }}</td>
                <td>{{ $rumah->tgl_daftar }}</td>
              </tr>
              @endforeach
            </tbody>
          </table> 
        </div>
      </div>

    </div>
  </div>
<!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
@endsection