
@extends('dosen.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">

      <div class="panel panel-colorful animated zoomIn">
        <div class="panel-heading">
          <h3 class="panel-title">Data Nilai</h3>
        </div>
        <div class="panel-body">
          <table class="table datatable">
            <thead>
              <tr>
                <th>No.</th>
                <th>NISN</th>
                <th>Rangking Smst 4</th>
                <th>Total Nilai Smst 4</th>
                <th>Jumlah Mapel Smst 4</th>
                <th>Rangking Smst 5</th>
                <th>Total Nilai Smst 5</th>
                <th>Jumlah Mapel Smst 5</th>
                {{-- <th>Rangking Smst 6</th>
                  <th>Total Nilai Smst 6</th>
                  <th>Jumlah Mapel Smst 6</th>
                  <th>Nilai UNAS</th>
                  <th>Jumlah Mapel UNAS</th> --}}
                  <th>Prestasi Ekskul</th>
                  <th>Tanggal Daftar</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($nilais as $nilai)
                <tr>
                  <td>{{ $loop->index + 1 }}</td>
                  <td>{{ $nilai->nisn }}</td>
                  <td>{{ $nilai->rang_smst_4 }}</td>
                  <td>{{ $nilai->ttl_nilai_smst_4 }}</td>
                  <td>{{ $nilai->jmlh_mapel_smst_4 }}</td>
                  <td>{{ $nilai->rang_smst_5 }}</td>
                  <td>{{ $nilai->ttl_nilai_smst_5 }}</td>
                  <td>{{ $nilai->jmlh_mapel_smst_5 }}</td>
                  {{-- <td>{{ $nilai->rang_smst_6 }}</td>
                  <td>{{ $nilai->ttl_nilai_smst_6 }}</td>
                  <td>{{ $nilai->jmlh_mapel_smst_6 }}</td>
                  <td>{{ $nilai->nilai_unas }}</td>
                  <td>{{ $nilai->jmlh_mapel_unas }}</td> --}}
                  <td>{{ $nilai->pres_ekskul }}</td>
                  <td>{{ $nilai->tgl_daftar }}</td>
                </tr>
                @endforeach
              </tbody>
            </table> 
          </div>
          <div class="panel-footer">
            <a href='{{route('data-nilai.index')}}' class="btn btn-warning">Kembali</a>
          </div>
        </div>
      </div>
    </div>
<!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection