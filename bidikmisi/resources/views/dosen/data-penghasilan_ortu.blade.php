@extends('dosen.layouts.app')
@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel">
        <div class="panel-heading">
          <h3 class="panel-title">Data Penghasilan Ortu</h3>
        </div>
        <div class="panel-body">
          <form action="{{ route('data-penghasilan_ortu.show') }}" method="post">
            {{ csrf_field()}}
            <div class="form-group">
              <div class="col-md-2">
                <select name="tahun" class="form-control">
                  <option value="">== Pilih Tahun ==</option>
                  {{ $now = Carbon\carbon::now()->year }}
                  @for ($i = 2017; $i <= $now ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                  @endfor
                </select>              
              </div>
            </div>
            <input type="submit" class="btn btn-success" name="Cari" value="Cari">
          </form>
        </div>
        <!-- /.box-header -->
        <div class="panel-body">
          <table class="table datatable">
            <thead>
              <tr>
                <th>No.</th>
                <th>NISN</th>
                <th>Nama Ibu Kandung</th>
                <th>Status Ayah</th>
                <th>Status Ibu</th>
                <th>Pekerjaan Ayah</th>
                <th>Pekerjaan Ibu</th>
                <th>Penghasilan Ayah</th>
                <th>Penghasilan Ibu</th>
                <th>Jumlah Tanggungan</th>
                <th>Tanggal Daftar</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($penghasilan_ortus as $penghasilan_ortu)
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $penghasilan_ortu->nisn}}</td>
                <td>{{ $penghasilan_ortu->nama_ibu_kdg}}</td>
                <td>{{ $penghasilan_ortu->status_ayah }}</td>
                <td>{{ $penghasilan_ortu->status_ibu }}</td>
                <td>{{ $penghasilan_ortu->pek_ayah }}</td>
                <td>{{ $penghasilan_ortu->pek_ibu }}</td>
                <td>{{ $penghasilan_ortu->peng_ayah }}</td>
                <td>{{ $penghasilan_ortu->peng_ibu }}</td>
                <td>{{ $penghasilan_ortu->jmlh_tanggungan }}</td>
                <td>{{ $penghasilan_ortu->tgl_daftar }}</td>
              </tr>
              @endforeach
            </tbody>
          </table> 
        </div>
      </div>

    </div>
  </div>
<!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
@endsection