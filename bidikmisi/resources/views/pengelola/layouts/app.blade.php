<!DOCTYPE html>
<html lang="en">
<head>
	@include('pengelola.layouts.head')
</head>
<body>
  <div class="page-container">
    @include('pengelola.layouts.sidebar')
    <div class="page-content">
     @include('pengelola.layouts.header')
     <ul class="breadcrumb">
     </ul>
     @section('main-content')
     @show
     <!--@include('pengelola.layouts.footer')-->
   </div>
 </div>
 <!-- MODAL -->
 <div class="modal" id="modal_iedosen" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="defModalHead">Import Data Dosen</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ route('dosen.import')}}" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
           <label for="file" class="col-md-3 control-label">Pilih File</label>
           <div class="col-md-6">
            <input type="file" id="file" name="file" class="form-control" autofocus required>
            <span class="help-block with-errors"></span>
          </div>
          <button type="submit" class="btn btn-primary">Import</button>
        </div>
        
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>

<div class="modal" id="modal_ievisitasi" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="defModalHead">Import Data Visitasi</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ route('visitasi.import')}}" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
           <label for="file" class="col-md-3 control-label">Pilih File</label>
           <div class="col-md-6">
            <input type="file" id="file" name="file" class="form-control" autofocus required>
            <span class="help-block with-errors"></span>
          </div>
          <button type="submit" class="btn btn-primary">Import</button>
        </div>
        
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>
<!-- -->
<!-- MESSAGE BOX-->
<div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
  <div class="mb-container">
    <div class="mb-middle">
      <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
      <div class="mb-content">
        <p>Are you sure you want to log out?</p>                    
        <p>Press No if you want to continue work. Press Yes to logout current user.</p>
      </div>
      <div class="mb-footer">
        <div class="pull-right">
          <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="btn btn-success btn-lg">Yes</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
          </form>
          <button class="btn btn-default btn-lg mb-control-close">No</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- END MESSAGE BOX-->
<!-- START PRELOADS -->
<audio id="audio-alert" src="{{asset('audio/alert.mp3')}}" preload="auto"></audio>
<audio id="audio-fail" src="{{asset('audio/fail.mp3')}}" preload="auto"></audio>
<!-- END PRELOADS -->                  

<!-- START SCRIPTS -->
<!-- START PLUGINS -->
<script type="text/javascript" src="{{asset('js/plugins/jquery/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/jquery/jquery-ui.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap.min.js')}}"></script>        
<!-- END PLUGINS -->

<!-- START THIS PAGE PLUGINS-->        
<script type='text/javascript' src='{{asset("js/plugins/icheck/icheck.min.js")}}'></script>        
<script type="text/javascript" src="{{asset('js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/scrolltotop/scrolltopcontrol.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/morris/raphael-min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/morris/morris.min.js')}}"></script>       
<script type="text/javascript" src="{{asset('js/plugins/rickshaw/d3.v3.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/rickshaw/rickshaw.min.js')}}"></script>
<script type='text/javascript' src='{{asset("js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js")}}'></script>
<script type='text/javascript' src='{{asset("js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js")}}'></script>                
<script type='text/javascript' src='{{asset("js/plugins/bootstrap/bootstrap-datepicker.js")}}'></script>                
<script type="text/javascript" src="{{asset('js/plugins/owl/owl.carousel.min.js')}}"></script>                 

<script type="text/javascript" src="{{asset('js/plugins/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>

<!-- END THIS PAGE PLUGINS-->        

<!-- START TEMPLATE -->
<script type="text/javascript" src="{{asset('js/settings.js')}}"></script>

<script type="text/javascript" src="{{asset('js/plugins.js')}}"></script>        
<script type="text/javascript" src="{{asset('js/actions.js')}}"></script>

<script type="text/javascript" src="{{asset('js/demo_dashboard.js')}}"></script>
<!-- END TEMPLATE -->
<!-- END SCRIPTS -->         
@section('script')
@show

</body>
</html>