<footer class="navbar footer fixed-bottom content container-fluid
">
		<p>
			<a href="http://pnp.ac.id" target="_blank">Politeknik Negeri Padang &copy; 2018 - {{(int)date('Y')}}</a> 
		</p>
    <div class="credits">
      info@pnp.ac.id
    </div>
    <a href="{{ route('pengelola.home')}}#" class="go-top">
     <i class="fa fa-angle-up"></i>
   </a>
</footer>

@section('footerSection')
@show