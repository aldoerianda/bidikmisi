<!-- START PAGE SIDEBAR -->
<div class="page-sidebar">
  <!-- START X-NAVIGATION -->
  <ul class="x-navigation">
    <li class="xn-logo">
      <a href="{{ route('pengelola.home')}}"><span>SI BM PNP</span></a>
      <a href="#" class="x-navigation-control"></a>
    </li>
    <li class="xn-profile">
      <a href="#" class="profile-mini">
        <img src="{{asset('img/imgpeng.png')}}" alt="Profile"/>
      </a>
      <div class="profile">
        <div class="profile-image">
          <img src="{{asset('img/imgpeng.png')}}" alt="Profile"/>
        </div>
        <div class="profile-data">
          <div class="profile-data-name">Pengelola</div>
          <div class="profile-data-title">Bidikmisi Politeknik Negeri Padang</div>
        </div>
      </div>                                                                        
    </li>
    <li>
      <a href="{{ route('pengelola.home')}}">
        <span class="fa fa-desktop"></span>
        <span class="xn-text">Dashboard</span>
      </a>
    </li>
    <li>
      <a href="{{ route('pengelola.ieds')}}">
        <span class="fa fa-cloud-download"></span>
        <span class="xn-text">Import/Export Data Siswa</span>
      </a>
    </li>
    <li>
      <a href="{{ route('pengelola.importfuzzy')}}">
        <span class="fa fa-cloud-download"></span>
        <span class="xn-text">Import Skor Fuzzy</span>
      </a>
    </li>
    <li class="xn-openable">
      <a href="#">
        <span class="fa fa-fw fa-database"></span>
        <span class="xn-text">Data Master</span>
      </a>
      <ul>
       <li><a href="{{ route('siswa.index')}}"><i class="xn-text"></i> Data Siswa</a></li>
       <li><a href="{{ route('datasau.index')}}"><i class="xn-text"></i> Data Saudara</a></li>
       <li><a href="{{ route('dataketsau.index')}}"><i class="xn-text"></i> Data Keterangan Saudara</a></li>
       <li><a href="{{ route('sekolah.index')}}"><i class="xn-text"></i> Data Sekolah</a></li>
       <li><a href="{{ route('rumah.index')}}"><i class="xn-text"></i> Data Rumah</a></li>
       <li><a href="{{ route('datafoto.index')}}"><i class="xn-text"></i>Data Foto</a></li>
       <li><a href="{{ route('organisasi.index')}}"><i class="xn-text"></i>Data Organisasi</a></li>
       <li><a href="{{ route('penghasilan_ortu.index')}}"><i class="xn-text"></i> Data Penghasilan Orangtua</a></li>
       <li><a href="{{ route('nilai.index')}}"><i class="xn-text"></i> Data Nilai</a></li>
       <li><a href="{{ route('pengelola.skorfuzzy')}}"><i class="xn-text"></i>Skor Fuzzy</a></li>
       <li><a href="{{ route('pendaftaran.index')}}"><i class="xn-text"></i> Data Pendaftaran</a></li>
       <li><a href="{{ route('catatan.index')}}"><i class="xn-text"></i> Data Catatan</a></li>
       <li><a href="{{ route('dosen.index')}}"><i class="xn-text"></i> Data Dosen</a></li>
       <li><a href="{{ route('visitasi.index')}}"><i class="xn-text"></i> Data Visitasi</a></li>
       <li><a href="{{ route('berita.index')}}"><i class="xn-text"></i> Data Berita</a></li>
       <li><a href="{{ route('user.index')}}"><i class="xn-text"></i> Data Login</a></li>
     </ul>
   </li>
   <li class="xn-openable">
    <a href="#">
      <span class="glyphicon glyphicon-check"></span> <span class="xn-text">Validasi</span>
    </a>
    <ul>
      <li><a href="{{ route('validasinilai.index')}}"><i class="xn-text"></i>Validasi Data Nilai </a></li>
      <li><a href="{{ route('jurusan.index')}}"><i class="xn-text"></i>Data Jurusan</a></li>
      <li><a href="{{ route('validasijurusan.index')}}"><i class="xn-text"></i>Validasi Data Jurusan </a></li>
      <li><a href="{{ route('validasipenghasilan.index')}}"><i class="xn-text"></i>Validasi Data Penghasilan Ortu </a></li>
    </ul>
  </li>
  <li class="xn-openable">
    <a href="#">
      <span class="glyphicon glyphicon-book"></span> <span class="xn-text">Laporan</span>
    </a>
    <ul>
      <li><a href="{{ route('siswaperprodi.index')}}"><i class="xn-text"></i>Siswa Per Prodi</a></li>
      <li><a href="{{ route('banyaksiswapersekolah.index')}}"><i class="xn-text"></i>Banyak Siswa Per Sekolah</a></li>
      <li><a href="{{ route('banyaksiswaperprovinsi.index')}}"><i class="xn-text"></i>Banyak Siswa Per Provinsi</a></li>
      <li><a href="{{ route('banyaksiswaperkota.index')}}"><i class="xn-text"></i>Banyak Siswa Per Kota</a></li>
      <li><a href="{{ route('prestasiekskul.index')}}"><i class="xn-text"></i>Prestasi Ekskul Siswa</a></li>
      <li><a href="{{ route('laravel_google_chart.index')}}"><i class="xn-text"></i>Persentase Program Studi Pil. 1</a></li>
      <li><a href="{{ route('laravel_google_chart2.index')}}"><i class="xn-text"></i>Persentase Program Studi Pil. 2</a></li>
    </ul>
  </li>
  <li class="xn-title">Session</li>
  <li>
    <a href="#" class="mb-control" data-box="#mb-signout"><span class="fa fa-sign-out"></span><span>Logout</span></a>
  </li>
</ul>
<!-- END X-NAVIGATION -->
</div>
<!-- END PAGE SIDEBAR -->