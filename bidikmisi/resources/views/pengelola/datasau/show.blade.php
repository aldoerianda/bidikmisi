@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Saudara</h3>
              <a class='col-lg-offset-10 btn btn-success' href="{{ route('datasau.create')}}">Tambah Data Saudara</a>
            </div>
            <!-- /.box-header -->
            <div class="panel-body table-responsive">
              <table class="table datatable table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NISN</th>
                  <th>Jumlah Saudara yang dibiayai Orangtua</th>
                  <th>Jumlah Saudara Kandung</th>
                  <th>Jumlah Saudara Tiri</th>
                  <th>Anak Ke</th>
                  <th>Sumber Biaya</th>
                  <th>Tempat Tinggal</th>
                  <th>No Telp</th>
                  <th>No Telp Saudara</th>
                  <th>Hubungan</th>
                  <th>Status</th>
                  <th width="5%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($datasau as $data)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $data->nisn }}</td>
                    <td>{{ $data->jml_saubiaya }}</td>
                    <td>{{ $data->jml_saukandung }}</td>
                    <td>{{ $data->jml_sautiri }}</td>
                    <td>{{ $data->anak_ke }}</td>
                    <td>{{ $data->sum_biaya }}</td>
                    <td>{{ $data->tmp_tinggal }}</td>
                    <td>{{ $data->notelp }}</td>
                    <td>{{ $data->notelp_sau }}</td>
                    <td>{{ $data->hub }}</td>
                    <td>{{ $data->status }}</td>
                    <td><a href="{{route('datasau.edit',$data->nisn)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $data->nisn }}" method="post" action="{{route('datasau.destroy',$data->nisn)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data Saudara?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $data->nisn }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection