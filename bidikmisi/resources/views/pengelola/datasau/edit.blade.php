@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Form Data Saudara</h3>
        </div>

        @include('includes.messages')

        <form role="form" action="{{ route('datasau.update',$datasau->nisn)}}" method="post">
          {{ csrf_field()}}
          {{ method_field('PATCH')}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">NISN</label>
              <div class="col-md-9 col-xs-12">
                <select class="form-control select" name="nisn">
                  <option value="{{$datasau->nisn}}">{{ $datasau->nisn }}</option>

                  <?php foreach ($siswa as $key => $d) {?>
                    <option value="{{ $d->nisn }}"> {{ $d->nisn }} </option> <?php
                  } ?>
                </select>
              </div>
            </div>
          </div>
         <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jml_saubiaya">Jumlah Saudara yg Dibiayai Orangtua</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jml_saubiaya" name="jml_saubiaya" placeholder="Jumlah Saudara yg Dibiayai Orangtua" value="{{$datasau->jml_saubiaya}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jml_saukandung">Jumlah Saudara Kandung</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jml_saukandung" name="jml_saukandung" placeholder="jml_saukandung" value="{{$datasau->jml_saukandung}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jml_sautiri">Jumlah Saudara Tiri</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jml_sautiri" name="jml_sautiri" placeholder="jml_sautiri" value="{{$datasau->jml_sautiri}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="anak_ke">Anak Ke</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="anak_ke" name="anak_ke" placeholder="anak_ke" value="{{$datasau->anak_ke}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="sum_biaya">Sumber Biaya</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="sum_biaya" name="sum_biaya" placeholder="sum_biaya" value="{{$datasau->sum_biaya}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="tmp_tinggal">Tempat Tinggal</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="tmp_tinggal" name="tmp_tinggal" placeholder="tmp_tinggal" value="{{$datasau->tmp_tinggal}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="notelp">No Telp</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="notelp" name="notelp" placeholder="notelp" value="{{$datasau->notelp}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="notelp_sau">No Telp Saudara</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="notelp_sau" name="notelp_sau" placeholder="notelp_sau" value="{{$datasau->notelp_sau}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="hub">Hubungan</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="hub" name="hub" placeholder="hub" value="{{$datasau->hub}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="status">Status</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="status" name="status" placeholder="status" value="{{$datasau->status}}">
              </div>
            </div>
          </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <a href='{{route("dataketsau.index")}}' class="btn btn-warning">Kembali</a>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection