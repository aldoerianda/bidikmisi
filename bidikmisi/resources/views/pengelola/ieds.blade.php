@extends('pengelola.layouts.app')

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="page-content-wrap">
  <!-- Main content -->
  <div class="row">
    <!-- Default box -->
    <div class="col-md-12">
      <div class="panel panel-colorful animated fadeIn">
        <div class="panel-heading">
          <h3 class="panel-title"> Import atau Export Data Siswa </h3>
        </div>
        <!-- /.box-body -->
        <div class="panel-body">
         <form method="post" action="{{ route('siswa.import')}}" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form-group">
           <label for="file" class="col-md-3 control-label">Import Data Siswa</label>
           <div class="col-md-3">
            <input type="file" id="file" name="file" class="form-control" autofocus required>
            <span class="help-block with-errors"></span>
          </div>
          <button type="submit" class="btn btn-primary">Import</button>
        </div>
        
      </form>
      <br><form method="get" action="{{ route('siswa.export')}}" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
        {{ csrf_field() }}
        <div class="form-group">
         <label for="export" class="col-md-3 control-label">Export Data Siswa</label>
         <div class="col-md-3">
          <select name="tahun" class="form-control select">
            <option value="">== Pilih Tahun ==</option>
            {{ $now = Carbon\carbon::now()->year }}
            @for ($i = 2017; $i <= $now ; $i++)
            <option value="{{ $i }}">{{ $i }}</option>
            @endfor
          </select> 
          <span class="help-block with-errors"></span>
        </div>
        <button type="submit" class="btn btn-primary">Export</button>
      </div>
    </form><br><br>
    <!-- /.box-footer-->
  </div>
  <!-- /.box -->
</div>
<!-- /.content -->
</div>
</div>
</div>
<!-- /.content-wrapper -->
@endsection
@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
@endsection