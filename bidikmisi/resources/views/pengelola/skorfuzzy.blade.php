@extends('pengelola.layouts.app')

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="page-content-wrap">
  <!-- Main content -->
  <div class="row">
    <!-- Default box -->
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title"> Import Skor Fuzzy </h3>
        </div>
        <div class="panel-body">
        <!-- /.box-body -->
        <form method="post" action="{{ route('fuzzy.import')}}" class="form-horizontal" data-toggle="validator" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="form_group">
            <label for="file" class="col-md-3 control-label">Import Skor Fuzzy</label>
           <div class="col-md-3">
            <input type="file" id="file" name="file" class="form-control" autofocus required>
            <span class="help-block with-errors"></span>
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Import</button>
      </form><br><br>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->
  </div>
  <!-- /.content -->
</div>
</div>
</div>
<!-- /.content-wrapper -->
@endsection