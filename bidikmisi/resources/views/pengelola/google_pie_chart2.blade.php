@extends('pengelola.layouts.app')

@section('head')
<script src="{{ asset('pengelola/dist/js/jquery-3.1.0.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('pengelola/dist/js/loader.js')}}"></script>
<link rel="stylesheet" href="{{ asset('pengelola/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
<style type="text/css">
 .box{
  width:800px;
  margin:0 auto;
}
</style>
<script type="text/javascript">
 var analytics = <?php echo $pil_prodi_2; ?>

 google.charts.load('current', {'packages':['corechart']});

 google.charts.setOnLoadCallback(drawChart);

 function drawChart()
 {
  var data = google.visualization.arrayToDataTable(analytics);
  var options = {
   title : 'Persentase Pemilihan Prodi'
 };
 var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));
 chart.draw(data, options);
}
</script>
@endsection

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
         <div class="panel panel-colorful animated fadeIn">
          <div class="panel-heading">
           <h3 class="panel-title" align="center">Persentase Pemilihan Prodi 2</h3>
         </div>
         <div class="panel-body">
          <form action="{{ route('laravel_google_chart2.show') }}" method="post">
            {{ csrf_field()}}
            <div class="form-group">
              <div class="col-md-3 col-xs-12">
                <select name="tahun" class="form-control select">
                  <option value="">== Pilih Tahun ==</option>
                  {{ $now = Carbon\carbon::now()->year }}
                  @for ($i = 2017; $i <= $now ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                  @endfor
                </select>              
              </div>
            </div>
            <input type="submit" class="btn btn-success" name="tampil" value="Tampil">
          </form>
        </div>
        <div class="panel-body" align="center">
         <div id="pie_chart" style="width:750px; height:450px;">

         </div>
       </div>
     </div>
   </div>
 </div>
</div>
</div>
<!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
@endsection