<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Validasi Jurusan</title>
</head>
<body>
  <div align="center">
    <h2>DATA VALIDASI JURUSAN SISWA YANG MENDAFTAR BIDIKMISI</h2>
    <h2>DI POLITEKNIK NEGERI PADANG TAHUN {{ $tahun }}</h2>
  </div>
  <hr>
  <div>
    <table class="table">
      <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>No. Pendaftaran</th>
                  <th>NISN</th>
                  <th>Tanggal Daftar</th>
                  <th>Jurusan Sekolah</th>
                  <th>Pil. Prodi 1</th>
                  <th>Pil. Prodi 2</th>
                  <th>Keterangan</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($pendaftarans as $pendaftaran)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $pendaftaran->no_pend}}</td>
                    <td>{{ $pendaftaran->nisn }}</td>
                    <td>{{ $pendaftaran->tgl_daftar }}</td>
                    <td>{{ $pendaftaran->jur_sekolah }}</td>
                    <td>{{ $pendaftaran->pil_prodi_1}}</td>
                    <td>{{ $pendaftaran->pil_prodi_2 }}</td>
                    <td>{{ check($pendaftaran->jur_sekolah,$pendaftaran->pil_prodi_1, $pendaftaran->pil_prodi_2)}}</td>
                  </tr>
                  @endforeach
                </tbody>
    </table>
  </div>
  <style>
    body {
      font-size: 12px;
      font-family: 'Times New Roman', Times, serif;
    }
    h2 {
      margin: 0px;
      padding: 0px;
    }
    table {
      border-collapse: collapse;
      width: 100%
    }
    th, td {
      padding: 3px;
    }
    .table td, th {
      border: 1px solid #ccc;
    }
  </style>
</body>
</html>