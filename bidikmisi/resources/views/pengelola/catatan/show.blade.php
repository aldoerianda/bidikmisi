@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful animated fadeIn">
        <div class="panel-heading">
          <h3 class="panel-title">Data Catatan</h3>
          <a class='col-lg-offset-10 btn btn-success' href="{{ route('catatan.create')}}">Tambah Data</a>
        </div>
        <!-- /.box-header -->
        <div class="panel-body table-responsive">
          <table class="table datatable table-hover">
            <thead>
              <tr>
                <th>No.</th>
                <th>NISN</th>
                <th>Pemahaman 1</th>
                <th>Pemahaman 2</th>
                <th>Harapan 1</th>
                <th>Harapan 2</th>
                <th>Catatan</th>
                <th>Ijazah</th>
                <th>Rapor</th>
                <th>Sertifikat</th>
                <th>KK</th>
                <th>Surat Keterangan Penghasilan Orangtua</th>
                <th>BPJS</th>
                <th>Kartu Indonesia Sehat</th>
                <th>Kartu Indonesia Pintar</th>
                <th>Kartu Simpanan Keluarga Sejahtera</th>
                <th>Raskin</th>
                <th>Pajak Bumi dan Bangunan</th>
                <th>Bukti Listrik</th>
                <th>Surat Pernyataan Kebenaran Data</th>
                <th>Penilaian</th>
                <th>Status</th>
                <th width="5%">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($data as $data)
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $data->nisn }}</td>
                <td>{{ $data->pemahaman1 }}</td>
                <td>{{ $data->pemahaman2 }}</td>
                <td>{{ $data->harapan1 }}</td>
                <td>{{ $data->harapan2 }}</td>
                <td>{{ $data->catatan }}</td>
                <td>{{ $data->ijazah }}</td>
                <td>{{ $data->rapor }}</td>
                <td>{{ $data->sertifikat }}</td>
                <td>{{ $data->kk }}</td>
                <td>{{ $data->surat_peng }}</td>
                <td>{{ $data->bpjs }}</td>
                <td>{{ $data->kis }}</td>
                <td>{{ $data->kip }}</td>
                <td>{{ $data->ksks }}</td>
                <td>{{ $data->raskin }}</td>
                <td>{{ $data->pbb }}</td>
                <td>{{ $data->bukti_listrik }}</td>
                <td>{{ $data->surat_data }}</td>
                <td>{{ $data->penilaian }}</td>
                <td>{{ $data->status }}</td>
                <td><a href="{{route('catatan.edit',$data->nisn)}}"><span class="glyphicon glyphicon-edit"></span></a>
                  <form id="delete-form-{{ $data->nisn }}" method="post" action="{{route('catatan.destroy',$data->nisn)}}" style="display: none">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                  </form>
                  <a href="" onclick="
                  if(confirm('Yakin Ingin Menghapus Data Catatan?'))
                  {
                    event.preventDefault();
                    document.getElementById('delete-form-{{ $data->nisn }}').submit();
                  } else {
                    event.preventDefault();
                  }"><span class="glyphicon glyphicon-trash"></span></a>

                </td>
              </tr>
              @endforeach
            </tbody>
          </table> 
        </div>
      </div>

    </div>
  </div>
  <!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection