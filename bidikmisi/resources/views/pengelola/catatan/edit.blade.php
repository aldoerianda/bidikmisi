@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Form Data Catatan</h3>
        </div>

        @include('includes.messages')

        <form role="form" action="{{ route('catatan.update',$data->nisn)}}" method="post">
          {{ csrf_field()}}
          {{ method_field('PATCH')}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">NISN</label>
              <div class="col-md-9 col-xs-12">
                <select class="form-control select" name="nisn">
                  <option value="{{$data->nisn}}">{{ $data->nisn }}</option>

                  <?php foreach ($siswa as $key => $d) {?>
                    <option value="{{ $d->nisn }}"> {{ $d->nisn }} </option> <?php
                  } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="alamat">Pemahaman 1</label>
              <div class="col-md-9 col-xs-12">
                <textarea id="pemahaman1" name="pemahaman1" class="form-control" rows="5">{{ $data->pemahaman1}}</textarea>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="alamat">Pemahaman 2</label>
              <div class="col-md-9 col-xs-12">
                <textarea id="pemahaman2" name="pemahaman2" class="form-control" rows="5">{{ $data->pemahaman2}}</textarea>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="alamat">Harapan 1</label>
              <div class="col-md-9 col-xs-12">
                <textarea id="harapan1" name="harapan1" class="form-control" rows="5">{{ $data->harapan1}}</textarea>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="alamat">harapan 2</label>
              <div class="col-md-9 col-xs-12">
                <textarea id="harapan2" name="harapan2" class="form-control" rows="5">{{ $data->harapan2}}</textarea>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="alamat">Catatan</label>
              <div class="col-md-9 col-xs-12">
                <textarea id="catatan" name="catatan" class="form-control" rows="5">{{ $data->catatan}}</textarea>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="ijazah">Ijazah</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="ijazah" value="Ada" {{$data->ijazah == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="ijazah" value="Tidak Ada" {{$data->ijazah == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="rapor">Rapor</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="rapor" value="Ada" {{$data->rapor == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="rapor" value="Tidak Ada" {{$data->rapor == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="sertifikat">Sertifikat</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="sertifikat" value="Ada" {{$data->sertifikat == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="sertifikat" value="Tidak Ada" {{$data->sertifikat == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="kk">Kartu Keluarga</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="kk" value="Ada" {{$data->kk == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="kk" value="Tidak Ada" {{$data->kk == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="surat_peng">Surat Keterangan Penghasilan Orangtua</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="surat_peng" value="Ada" {{$data->surat_peng == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="surat_peng" value="Tidak Ada" {{$data->surat_peng == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="bpjs">BPJS</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="bpjs" value="Ada" {{$data->bpjs == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="bpjs" value="Tidak Ada" {{$data->bpjs == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="kis">Kartu Indonesia Sehat</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="kis" value="Ada" {{$data->kis == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="kis" value="Tidak Ada" {{$data->kis == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="kip">Kartu Indonesia Pintar</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="kip" value="Ada" {{$data->kip == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="kip" value="Tidak Ada" {{$data->kip == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="ksks">Kartu Simpanan Keluarga Sejahtera</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="ksks" value="Ada" {{$data->ksks == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="ksks" value="Tidak Ada" {{$data->ksks == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="raskin">Raskin</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="raskin" value="Ada" {{$data->raskin == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="raskin" value="Tidak Ada" {{$data->raskin == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="pbb">Pajak Bumi dan Bangunan</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="pbb" value="Ada" {{$data->pbb == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="pbb" value="Tidak Ada" {{$data->pbb == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="bukti_listrik">Bukti Listrik</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="bukti_listrik" value="Ada" {{$data->bukti_listrik == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="bukti_listrik" value="Tidak Ada" {{$data->bukti_listrik == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="surat_data">Surat Pernyataan Kebenaran Data</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="surat_data" value="Ada" {{$data->surat_data == 'Ada' ? 'checked' :''}}>Ada</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="surat_data" value="Tidak Ada" {{$data->surat_data == 'Tidak Ada' ? 'checked' :''}}>Tidak Ada</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="penilaian">Penilaian</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="penilaian" value="Tidak Layak" {{$data->penilaian == 'Tidak Layak' ? 'checked' :''}}>Tidak Layak</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="penilaian" value="Kurang Layak" {{$data->penilaian == 'Kurang Layak' ? 'checked' :''}}>Kurang Layak</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="penilaian" value="Layak" {{$data->penilaian == 'Layak' ? 'checked' :''}}>Layak</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="penilaian" value="Sangat Layak" {{$data->penilaian == 'Sangat Layak' ? 'checked' :''}}>Sangat Layak</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="status">Status</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="status" value="Verifikasi Selesai" {{$data->status == 'Verifikasi Selesai' ? 'checked' :''}}>Verifikasi Selesai</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="status" value="Belum Verifikasi" {{$data->status == 'Belum Verifikasi' ? 'checked' :''}}>Belum Verifikasi</label>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href='{{route("catatan.index")}}' class="btn btn-warning">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection