@extends('pengelola.layouts.app')

@section('head')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('pengelola/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
@endsection

@section('main-content')
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Rumah</h3>
            </div>
           
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>NISN</th>
                  <th>Kepemilikan</th>
                  <th>Tahun Peroleh</th>
                  <th>Sumber Listrik</th>
                  <th>Luas Tanah</th>
                  <th>Luas Bangunan</th>
                  <th>Sumber Air</th>
                  <th>MCK</th>
                  <th>Jarak Pusat Kota</th>
                  <th>Tanggal Daftar</th>
                  <th width="7%">Edit</th>
                  <th width="7%">Delete</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($rumahs as $rumah)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $rumah->nisn }}</td>
                    <td>{{ $rumah->kepemilikan }}</td>
                    <td>{{ $rumah->thn_peroleh }}</td>
                    <td>{{ $rumah->sumber_listrik }}</td>
                    <td>{{ $rumah->luas_tanah }}</td>
                    <td>{{ $rumah->luas_bangunan }}</td>
                    <td>{{ $rumah->sumber_air }}</td>
                    <td>{{ $rumah->mck }}</td>
                     <td>{{ $rumah->jrk_pusat_kota }}</td>
                     <td>{{ $rumah->tgl_daftar }}</td>
                    <td><a href="{{route('rumah.edit',$rumah->id)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                     <td>
                      <form id="delete-form-{{ $rumah->id }}" method="post" action="{{route('rumah.destroy',$rumah->id)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data rumah?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $rumah->id }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
      <div class="box-footer">
        <a href='{{route('rumah.index')}}' class="btn btn-warning">Kembali</a>
      </div>
    </section>

    </section>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('pengelola/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('pengelola/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script>
  $(function () {
    $('#myTable').DataTable()
  })
</script>
@endsection