@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <!-- Main content -->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Rumah</h3>
               <a class='col-lg-offset-10 btn btn-success' href="{{ route('rumah.create')}}">Tambah Data Rumah</a>
            </div>
            <div class="panel-body table-responsive">
              <table class="table datatable table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NISN</th>
                  <th>Kepemilikan</th>
                  <th>Tahun Peroleh</th>
                  <th>Sumber Listrik</th>
                  <th>Luas Tanah</th>
                  <th>Luas Bangunan</th>
                  <th>Sumber Air</th>
                  <th>MCK</th>
                  <th>Jarak Pusat Kota</th>
                  <th>Tanggal Daftar</th>
                  <th width="5%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($rumahs as $rumah)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $rumah->nisn }}</td>
                    <td>{{ $rumah->kepemilikan }}</td>
                    <td>{{ $rumah->thn_peroleh }}</td>
                    <td>{{ $rumah->sumber_listrik }}</td>
                    <td>{{ $rumah->luas_tanah }}</td>
                    <td>{{ $rumah->luas_bangunan }}</td>
                    <td>{{ $rumah->sumber_air }}</td>
                    <td>{{ $rumah->mck }}</td>
                     <td>{{ $rumah->jrk_pusat_kota }}</td>
                     <td>{{ $rumah->tgl_daftar }}</td>
                    <td><a href="{{route('rumah.edit',$rumah->nisn)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $rumah->nisn }}" method="post" action="{{route('rumah.destroy',$rumah->nisn)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data rumah?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $rumah->nisn }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>                
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>
        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection