@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Form Rumah</h3>
        </div>

        @include('includes.messages')

        <form role="form" action="{{ route('rumah.store')}}" method="post">
          {{ csrf_field()}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">NISN</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="kepemilikan">Kepemilikan</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="kepemilikan" name="kepemilikan" placeholder="Kepemilikan">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="thn_peroleh">Tahun Perolah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="thn_peroleh" name="thn_peroleh" placeholder="Tahun Perolah">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="sumber_listrik">Sumber Listrik</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="sumber_listrik" name="sumber_listrik" placeholder="Sumber Listrik">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="luas_tanah">Luas Tanah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="luas_tanah" name="luas_tanah" placeholder="Luas Tanah">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="luas_bangunan">Luas Bangunan</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="luas_bangunan" name="luas_bangunan" placeholder="Luas Bangunan">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="sumber_air">Sumber Air</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="sumber_air" name="sumber_air" placeholder="Sumber Air">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="mck">MCK</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="mck" name="mck" placeholder="MCK">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jrk_pusat_kota">Jarak Pusat Kota</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jrk_pusat_kota" name="jrk_pusat_kota" placeholder="Jarak Pusat Kota">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="tgl_daftar">Tanggal Daftar</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="tgl_daftar" name="tgl_daftar" placeholder="Tanggal Daftar">
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href='{{route("rumah.index")}}' class="btn btn-warning">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection