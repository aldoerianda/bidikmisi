@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">

      <div class="panel panel-colorful animated fadeIn">
        <div class="panel-heading" align="center">
          <h3 class="panel-title">Banyak Siswa Per Kota atau Kabupaten</h3>
        </div>
        <!-- /.box-header -->
        <div class="panel-body">
         <form action="{{route('banyaksiswaperkota.show')}}" method="post">
          {{ csrf_field()}}
          <div class="row">
            <div class="form-group">
              <div class="col-md-4 col-xs-12">
                <select name="prov_sklh" class="form-control select">
                  <option value="">==Pilih Provinsi==</option>
                  @foreach ($showprov as $data)
                  <option>{{ $data->prov_sklh }}</option>
                  @endforeach
                </select>
              </div>
            </div>
          </div><br>
          <div class="row">
            <div class="form-group">
              <div class="col-md-4 col-xs-12">
                <select name="tahun" class="form-control select">
                  <option value="">== Pilih Tahun ==</option>
                  {{ $now = Carbon\carbon::now()->year }}
                  @for ($i = 2017; $i <= $now ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                  @endfor
                </select>              
              </div>
            </div>
          </div><br>
          <div class="row">
            <div class="col-md-4 col-xs-12">
              <input type="submit" class="btn btn-success" name="cetak" value="Cetak">
            </div>
          </div>
        </form>
      </div>
      <div class="panel-body table-responsive">
        <table class="table datatable table-hover">
          <thead>
            <tr>
              <th>No.</th>
              <th>Nama Kota/Kab</th>
              <th>Jumlah Siswa</th>
            </tr>
          </thead>
          <tbody>
            @foreach ($sekolahs as $data)
            <tr>
              <td>{{ $loop->index + 1 }}</td>
              <td>{{ $data->kota_sklh }}</td>
              <td>{{ $data->total }} Siswa</td>
            </tr>
            @endforeach
          </tbody>
        </table> 
      </div>
    </div>

  </div>
</div>
<!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
@endsection