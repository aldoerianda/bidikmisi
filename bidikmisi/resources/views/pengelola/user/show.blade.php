@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data User</h3>
              <a class='col-lg-offset-10 btn btn-success' href="{{ route('user.create')}}">Tambah User</a>
            </div>
            <!-- /.box-header -->
            <div class="panel-body table-responsive">
              <table class="table datatable table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NIDN</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Level</th>
                  <th width="5%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($pengelolas as $pengelola)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $pengelola->nidn }}</td>
                    <td>{{ $pengelola->username }}</td>
                    <td>{{ $pengelola->password }}</td>
                    <td>{{ $pengelola->level }}</td>
                    <td><a href="{{route('user.edit',$pengelola->id)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $pengelola->id }}" method="post" action="{{route('user.destroy',$pengelola->id)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data User?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $pengelola->id }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection