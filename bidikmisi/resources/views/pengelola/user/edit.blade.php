@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Form User</h3>
        </div>

        @include('includes.messages')

        <form role="form" action="{{ route('user.update',$pengelola->id)}}" method="post">
          {{ csrf_field()}}
          {{ method_field('PATCH')}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">NIDN</label>
              <div class="col-md-9 col-xs-12">
                <select class="form-control select" name="nidn">
                  <option value="{{ $pengelola->nidn}}">{{ $pengelola->nidn}}</option>
                  <option value="">--NIDN--</option>
                  <?php foreach ($dosen as $key => $d) {?>
                    <option value="{{ $d->nidn }}"> {{ $d->nidn }} </option> <?php
                  } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="username">Username</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="username" name="username" placeholder="username" value="{{$pengelola->username}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="password">Password</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="password" name="password" placeholder="password" value="">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label">Level</label>
              <div class="col-md-9 col-xs-12">
                <select class="form-control select" name="level">
                  if({{$pengelola->level}}=="Pengelola"){
                  <option value="Pengelola" selected="">Pengelola</option> }
                  else{
                  <option value="Visitor" selected="">Visitor</option>
                }
              </select>
            </div>
          </div>
        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <a href='{{route("user.index")}}' class="btn btn-warning">Kembali</a>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection