@extends('pengelola.layouts.app')
@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful animated fadeIn">
        <div class="panel-heading">
          <h3 class="panel-title">Data Prestasi Siswa</h3>
        </div>
        <div class="panel-body">
          <form action="{{ route('prestasiekskul.show') }}" method="post" class="form-horizontal">
            {{ csrf_field()}}
            <div class="form-group">
              <div class="col-md-2 col-xs-12">
                <select name="tahun" class="form-control select">
                  <option value="">== Pilih Tahun ==</option>
                  {{ $now = Carbon\carbon::now()->year }}
                  @for ($i = 2017; $i <= $now ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                  @endfor
                </select>              
              </div>
            </div>
            <input type="submit" class="btn btn-success" name="cetak" value="Cetak">
          </form>
        </div>
        <div class="panel-body table-responsive">
          <table class="table datatable table-hover">
            <thead>
              <tr>
                <th>No.</th>
                <th>NISN</th>
                <th>Nama Siswa</th>
                <th>Asal Sekolah</th>
                {{-- <th>Rangking Smst 4</th>
                  <th>Total Nilai Smst 4</th>
                  <th>Jumlah Mapel Smst 4</th>
                  <th>Rangking Smst 5</th>
                  <th>Total Nilai Smst 5</th>
                  <th>Jumlah Mapel Smst 5</th> --}}
                  <th>Prestasi Ekskul</th>
                  <th>Tanggal Daftar</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($nilais as $nilai)
                <tr>
                  <td>{{ $loop->index + 1 }}</td>
                  <td>{{ $nilai->nisn }}</td>
                  <td>{{ $nilai->nama_siswa }}</td>
                  <td>{{ $nilai->nama_sklh }}</td>
                  {{-- 
                    <td>{{ $nilai->rang_smst_4 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_4 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_4 }}</td>
                    <td>{{ $nilai->rang_smst_5 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_5 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_5 }}</td> --}}
                    <td>{{ $nilai->pres_ekskul }}</td>
                    <td>{{ $nilai->tgl_daftar }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
      <!-- /.content -->
    </div>

    @endsection

    @section('script')
    <!-- DataTables -->
    <script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
    @endsection