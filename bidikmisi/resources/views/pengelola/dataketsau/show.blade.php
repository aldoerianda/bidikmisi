@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Keterangan Saudara</h3>
              <a class='col-lg-offset-10 btn btn-success' href="{{ route('dataketsau.create')}}">Tambah Data</a>
            </div>
            <!-- /.box-header -->
            <div class="panel-body table-responsive">
              <table class="table datatable table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NISN</th>
                  <th>Nama Saudara</th>
                  <th>Umur Saudara</th>
                  <th>Pekerjaan Saudara</th>
                  <th>Status Hubungan</th>
                  <th width="5%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($dataketsau as $data)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $data->nisn }}</td>
                    <td>{{ $data->nama_sau }}</td>
                    <td>{{ $data->umur_sau }}</td>
                    <td>{{ $data->pekerjaan_sau }}</td>
                    <td>{{ $data->status_hub }}</td>
                    <td><a href="{{route('dataketsau.edit',$data->id_ket)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $data->id_ket }}" method="post" action="{{route('dataketsau.destroy',$data->id_ket)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data Keterangan Saudara?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $data->id_ket }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection