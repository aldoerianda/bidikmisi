@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Form Data Keterangan Saudara</h3>
        </div>

        @include('includes.messages')

        <form role="form" action="{{ route('dataketsau.store')}}" method="post">
          {{ csrf_field()}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">NISN</label>
              <div class="col-md-9 col-xs-12">
                <select class="form-control select" name="nisn">
                  <option value="" selected="true">--NISN--</option>
                  <?php foreach ($siswa as $key => $d) {?>
                    <option value="{{ $d->nisn }}"> {{ $d->nisn }} </option> <?php
                  } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nama_sau">Nama Saudara</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nama_sau" name="nama_sau" placeholder="Nama Saudara" value="">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="umur_sau">Umur Saudara</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="umur_sau" name="umur_sau" placeholder="umur_sau" value="">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="pekerjaan_sau">Pekerjaan Saudara</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="pekerjaan_sau" name="pekerjaan_sau" placeholder="pekerjaan_sau" value="">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="status_hub">Status Hubungan</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="status_hub" name="status_hub" placeholder="status_hub" value="">
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href='{{route("dataketsau.index")}}' class="btn btn-warning">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection
@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script> 
@endsection