@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful animated fadeIn">
        <div class="panel-heading">
          <h3 class="panel-heading">Data Validasi Nilai</h3>
          <a class='col-lg-offset-10 btn btn-success' href="{{url('/pengelola/validatenilai/validasi')}}">Validasi</a>
        </div>
        <div class="panel-body">
          <form action="{{ route('validasinilai.show') }}" method="post">
            {{ csrf_field()}}
            <div class="form-group">
              <div class="col-md-2 col-xs-9">
                <select name="tahun" class="form-control select">
                  <option value="">== Pilih Tahun ==</option>
                  {{ $now = Carbon\carbon::now()->year }}
                  @for ($i = 2017; $i <= $now ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                  @endfor
                </select>             
              </div>
            </div>
            <input type="submit" class="btn btn-success" name="cetak" value="Cetak">
          </form>
        </div>
        <div class="panel-body table-responsive">
          <table class="table datatable table-hover">
            <thead>
              <tr>
                <th>No.</th>
                <th>NISN</th>
                <th>Nama Siswa</th>
                <th>Tanggal Daftar</th>
                <th>Total Nilai Smst 4</th>
                <th>Jumlah Mapel Smst 4</th>
                <th>Total Nilai Smst 5</th>
                <th>Jumlah Mapel Smst 5</th>
                <th>Rata-Rata</th>
                <th>Keterangan</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($nilais as $nilai)
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $nilai->nisn }}</td>
                <td>{{ $nilai->nama_siswa }}</td>
                <td>{{ $nilai->tgl_daftar }}</td>
                <td>{{ $nilai->ttl_nilai_smst_4 }}</td>
                <td>{{ $nilai->jmlh_mapel_smst_4 }}</td>
                <td>{{ $nilai->ttl_nilai_smst_5 }}</td>
                <td>{{ $nilai->jmlh_mapel_smst_5 }}</td>
                <td>{{ number_format($nilai->rata,2) }}</td>
                <td>
                  @if($nilai->rata > '0' && $nilai->rata <= '100')
                  Valid
                  @else Tidak Valid
                  @endif
                </td>

              </tr>
              @endforeach
            </tbody>
          </table> 
        </div>
      </div>
    </div>
  </div>
  <!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script> 
@endsection