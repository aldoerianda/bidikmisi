@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <!-- Main content -->
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Penghasilan Orangtua</h3>
              <a class='col-lg-offset-10 btn btn-success' href="{{ route('penghasilan_ortu.create')}}">Tambah</a>
            </div>
            <div class="panel-body table-reponsive">
              <table class="table datatable table-hover">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>NISN</th>
                    <th>Nama Ibu Kandung</th>
                    <th>Status Ayah</th>
                    <th>Status Ibu</th>
                    <th>Pekerjaan Ayah</th>
                    <th>Pekerjaan Ibu</th>
                    <th>Penghasilan Ayah</th>
                    <th>Penghasilan Ibu</th>
                    <th>Income</th>
                    <th>Jumlah Tanggungan</th>
                    <th>Tanggal Daftar</th>
                    <th width="5%">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($penghasilan_ortus as $penghasilan_ortu)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $penghasilan_ortu->nisn}}</td>
                    <td>{{ $penghasilan_ortu->nama_ibu_kdg}}</td>
                    <td>{{ $penghasilan_ortu->status_ayah }}</td>
                    <td>{{ $penghasilan_ortu->status_ibu }}</td>
                    <td>{{ $penghasilan_ortu->pek_ayah }}</td>
                    <td>{{ $penghasilan_ortu->pek_ibu }}</td>
                    <td>{{ $penghasilan_ortu->peng_ayah }}</td>
                    <td>{{ $penghasilan_ortu->peng_ibu }}</td>
                    <td>{{ $penghasilan_ortu->income }}</td>
                    <td>{{ $penghasilan_ortu->jmlh_tanggungan }}</td>
                    <td>{{ $penghasilan_ortu->tgl_daftar }}</td>
                    <td><a href="{{route('penghasilan_ortu.edit',$penghasilan_ortu->nisn)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $penghasilan_ortu->nisn }}" method="post" action="{{route('penghasilan_ortu.destroy',$penghasilan_ortu->nisn)}}" style="display:none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data penghasilan_ortu?'))
                      {
                        event.preventDefault();
                        document.getElementById('delete-form-{{ $penghasilan_ortu->nisn }}').submit();
                      } else {
                        event.preventDefault();
                      }"><span class="glyphicon glyphicon-trash"></span></a>
                      
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
  <!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection