@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Form Penghasilan Ortu</h3>
        </div>

        @include('includes.messages')

        <form role="form" action="{{ route('penghasilan_ortu.store')}}" method="post">
          {{ csrf_field()}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">NISN</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nama_ibu_kdg">Nama Ibu Kandung</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nama_ibu_kdg" name="nama_ibu_kdg" placeholder="Nama Ibu Kandung">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="status_ayah">Status Ayah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="status_ayah" name="status_ayah" placeholder="Status Ayah">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="status_ibu">Status Ibu</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="status_ibu" name="status_ibu" placeholder="Status Ibu">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="pek_ayah">Pekerjaan Ayah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="pek_ayah" name="pek_ayah" placeholder="Pekerjaan Ayah">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="pek_ibu">Pekerjaan Ibu</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="pek_ibu" name="pek_ibu" placeholder="Pekerjaan Ibu">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="peng_ayah">Penghasilan Ayah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="peng_ayah" name="peng_ayah" placeholder="Penghasilan Ayah">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="peng_ibu">Penghasilan Ibu</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="peng_ibu" name="peng_ibu" placeholder="Penghasilan Ibu">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jmlh_tanggungan">Jumlah Tanggungan</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jmlh_tanggungan" name="jmlh_tanggungan" placeholder="Jumlah Tanggungan">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="tgl_daftar">Tanggal Daftar</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="tgl_daftar" name="tgl_daftar" placeholder="Tanggal Daftar">
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href='{{route("penghasilan_ortu.index")}}' class="btn btn-warning">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection