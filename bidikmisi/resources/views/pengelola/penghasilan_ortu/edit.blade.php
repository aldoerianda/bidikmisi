@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-colorful">
          <div class="panel-heading">
              <h3 class="panel-title">Form Penghasilan Orangtua</h3>
          </div>

            @include('includes.messages')

          <form role="form" action="{{ route('penghasilan_ortu.update',$penghasilan_ortu->nisn)}}" method="post">
          {{ csrf_field()}}
          {{ method_field('PATCH')}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">NISN</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN" value="{{$penghasilan_ortu->nisn}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nama_ibu_kdg">Nama Ibu Kandung</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nama_ibu_kdg" name="nama_ibu_kdg" placeholder="Nama Ibu Kandung" value="{{$penghasilan_ortu->nama_ibu_kdg}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="status_ayah">Status Ayah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="status_ayah" name="status_ayah" placeholder="Status Ayah" value="{{$penghasilan_ortu->status_ayah}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="status_ibu">Status Ibu</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="status_ibu" name="status_ibu" placeholder="Status Ibu" value="{{$penghasilan_ortu->status_ibu}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="pek_ayah">Pekerjaan Ayah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="pek_ayah" name="pek_ayah" placeholder="Pekerjaan Ayah" value="{{$penghasilan_ortu->pek_ayah}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="pek_ibu">Pekerjaan Ibu</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="pek_ibu" name="pek_ibu" placeholder="Pekerjaan Ibu" value="{{$penghasilan_ortu->pek_ibu}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="peng_ayah">Penghasilan Ayah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="peng_ayah" name="peng_ayah" placeholder="Penghasilan Ayah" value="{{$penghasilan_ortu->peng_ayah}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="peng_ibu">Penghasilan Ibu</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="peng_ibu" name="peng_ibu" placeholder="Penghasilan Ibu" value="{{$penghasilan_ortu->peng_ibu}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jmlh_tanggungan">Jumlah Tanggungan</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jmlh_tanggungan" name="jmlh_tanggungan" placeholder="Jumlah Tanggungan" value="{{$penghasilan_ortu->jmlh_tanggungan}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="tgl_daftar">Tanggal Daftar</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="tgl_daftar" name="tgl_daftar" placeholder="Tanggal Daftar" value="{{$penghasilan_ortu->tgl_daftar}}">
              </div>
            </div>
          </div>
          <div class="panel-footer">
              <button type="submit" class="btn btn-primary">Simpan</button>
              <a href='{{route("penghasilan_ortu.index")}}' class="btn btn-warning">Kembali</a>
          </div>
          </form>
        </div>
      </div>
      </div>
  </section>
</div>
  @endsection