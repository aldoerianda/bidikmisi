@extends('pengelola.layouts.app')

@section('head')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('pengelola/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
@endsection

@section('main-content')
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Penghasilan Orangtua</h3>
            </div>
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>NISN</th>
                  <th>Nama Ibu Kandung</th>
                  <th>Status Ayah</th>
                  <th>Status Ibu</th>
                  <th>Pekerjaan Ayah</th>
                  <th>Pekerjaan Ibu</th>
                  <th>Penghasilan Ayah</th>
                  <th>Penghasilan Ibu</th>
                  <th>Income</th>
                  <th>Jumlah Tanggungan</th>
                  <th>Tanggal Daftar</th>
                  <th width="7%">Edit</th>
                  <th width="7%">Delete</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($penghasilan_ortus as $penghasilan_ortu)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $penghasilan_ortu->nisn}}</td>
                    <td>{{ $penghasilan_ortu->nama_ibu_kdg}}</td>
                    <td>{{ $penghasilan_ortu->status_ayah }}</td>
                    <td>{{ $penghasilan_ortu->status_ibu }}</td>
                    <td>{{ $penghasilan_ortu->pek_ayah }}</td>
                    <td>{{ $penghasilan_ortu->pek_ibu }}</td>
                    <td>{{ $penghasilan_ortu->peng_ayah }}</td>
                    <td>{{ $penghasilan_ortu->peng_ibu }}</td>
                    <td>{{ $penghasilan_ortu->income }}</td>
                    <td>{{ $penghasilan_ortu->jmlh_tanggungan }}</td>
                    <td>{{ $penghasilan_ortu->tgl_daftar }}</td>
                
                    <td><a href="{{route('penghasilan_ortu.edit',$penghasilan_ortu->nisn)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                     <td>
                      <form id="delete-form-{{ $penghasilan_ortu->nisn }}" method="post" action="{{route('penghasilan_ortu.destroy',$penghasilan_ortu->nisn)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data penghasilan_ortu?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $penghasilan_ortu->nisn }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
       <div class="box-footer">
        <a href='{{route('penghasilan_ortu.index')}}' class="btn btn-warning">Kembali</a>
      </div>
    </section>

    </section>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('pengelola/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('pengelola/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script>
  $(function () {
    $('#myTable').DataTable()
  })
</script>
@endsection