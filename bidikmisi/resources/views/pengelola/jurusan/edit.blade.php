@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Form Jurusan</h3>
        </div>

        @include('includes.messages')

        <form role="form" action="{{ route('jurusan.update',$pendaftaran->jur_sekolah)}}" method="post">
          {{ csrf_field()}}
          {{ method_field('PATCH')}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">Jurusan Sekolah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jur_sekolah" name="jur_sekolah" placeholder="Jurusan Sekolah" value="{{ $pendaftaran->jur_sekolah}}">
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href='{{route("jurusan.index")}}' class="btn btn-warning">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</section>
</div>
@endsection