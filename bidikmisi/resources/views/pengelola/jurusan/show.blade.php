@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
    <div class="row">
      <div class="col-md-6">
        <div class="panel panel-colorful animated fadeIn xn-drop-left">
          <div class="panel-heading">
            <h3 class="panel-title">Jurusan Sekolah </h3>
          </div>

          <div class="panel-body table-responsive">
            <table class="table datatable table-hover">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Jurusan Sekolah</th>
                  <th>Jumlah</th>
                  <th width="5%">Aksi</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($pendaftarans as $pendaftaran)
                <tr>
                  <td>{{ $loop->index + 1 }}</td>
                  <td>{{ $pendaftaran->jur_sekolah }}</td>
                  <td>{{ $pendaftaran->jumlah }}</td>
                  <td align="center"><a href="{{route('jurusan.edit',$pendaftaran->jur_sekolah)}}"><span class="glyphicon glyphicon-edit"></span></a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table> 
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="panel panel-colorful animated fadeIn xn-drop-left">
          <div class="panel-heading">
            <h3 class="panel-title">Data Master</h3>
          </div>
          <div class="panel-body table-responsive">
            <table class="table datatable table-hover">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>Jurusan Sekolah</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>IPA</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>IPS</td></tr>
                <tr>
                  <td>3</td>
                  <td>ALAT MESIN PERTANIAN</td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>KONTROL MEKANIK</td>
                </tr>
                <tr>
                  <td>5</td>
                  <td>KONTROL PROSES</td>
                </tr>
                <tr>
                  <td>6</td>
                  <td>KONTRUKSI BADAN PESAWAT UDARA</td>
                </tr>
                <tr>
                  <td>7</td>
                  <td>KONTRUKSI RANGKA PESAWAT UDARA</td>
                </tr>
                <tr>
                  <td>8</td>
                  <td>PEMESINAN PESAWAT UDARA</td>
                </tr>
                <tr>
                  <td>9</td>
                  <td>TEKNIK ALAT BERAT</td>
                </tr>
                <tr>
                  <td>10</td>
                  <td>TEKNIK DAN MANAJEMEN INDUSTRI</td></tr>
                <tr>
                  <td>11</td>
                  <td>TEKNIK DAN MANAJEMEN PRODUKSI</td>
                </tr>
                <tr>
                  <td>12</td>
                  <td>TEKNIK ENERGI BIOMASSA</td>
                </tr>
                <tr>
                  <td>13</td>
                  <td>TEKNIK ENERGI HIDRO</td>
                </tr>
                <tr>
                  <td>14</td>
                  <td>TEKNIK ENERGI SURYA DAN ANGIN</td>
                </tr>
                <tr>
                  <td>15</td>
                  <td>TEKNIK FABRIKASI LOGAM</td>
                </tr>
                <tr>
                  <td>16</td>
                  <td>TEKNIK GAMBAR MESIN</td>
                </tr>
                 <tr>
                  <td>17</td>
                  <td>TEKNIK GAMBAR RANCANG BANGUN KAPAL</td>
                </tr>
                <tr>
                  <td>18</td>
                  <td>TEKNIK INDUSTRI</td>
                </tr>
                <tr>
                  <td>19</td>
                  <td>TEKNIK INSTALASI PEMESINAN KAPAL</td>
                </tr>
                <tr>
                  <td>20</td>
                  <td>TEKNIK KAPAL NIAGA</td>
                </tr>
                <tr>
                  <td>21</td>
                  <td>TEKNIK KENDARAAN RINGAN</td>
                </tr>
                <tr>
                  <td>22</td>
                  <td>TEKNIK KETENAGALISTRIKAN</td></tr>
                <tr>
                  <td>23</td>
                  <td>TEKNIK KIMIA</td>
                </tr>
                <tr>
                  <td>24</td>
                  <td>TEKNIK KONTRUKSI KAPAL BAJA</td>
                </tr>
                <tr>
                  <td>25</td>
                  <td>TEKNIK KONTRUKSI KAPAL FIBERGLASS</td>
                </tr>
                <tr>
                  <td>26</td>
                  <td>TEKNIK KONTRUKSI KAPAL KAYU</td>
                </tr>
                <tr>
                  <td>27</td>
                  <td>TEKNIK MEKATRONIKA</td>
                </tr>
                <tr>
                  <td>28</td>
                  <td>TEKNIK MESIN</td>
                </tr>
                <tr>
                  <td>29</td>
                  <td>TEKNIK OTOMASI INDUSTRI</td>
                </tr>
                <tr>
                  <td>30</td>
                  <td>TEKNIK OTOMOTIF</td>
                </tr>
                <tr>
                  <td>31</td>
                  <td>TEKNIK OTOMOTIF ALAT BERAT</td>
                </tr>
                <tr>
                  <td>32</td>
                  <td>TEKNIK OTOMOTIF KENDARAAN RINGAN</td>
                </tr>
                <tr>
                  <td>33</td>
                  <td>TEKNIK OTOMOTIF SEPEDA MOTOR</td>
                </tr>
                 <tr>
                  <td>34</td>
                  <td>TEKNIK OTROTONIK</td>
                </tr>
                <tr>
                  <td>35</td>
                  <td>TEKNIK PEMBANGKIT TENAGA LISTRIK</td>
                </tr>
                <tr>
                  <td>36</td>
                  <td>TEKNIK PEMBORAN</td>
                </tr>
                <tr>
                  <td>37</td>
                  <td>TEKNIK PEMBORAN MINYAK</td>
                </tr>
                <tr>
                  <td>38</td>
                  <td>TEKNIK PEMBORAN MINYAK DAN GAS</td>
                </tr>
                <tr>
                  <td>39</td>
                  <td>TEKNIK PEMELIHARAAN MEKANIK INDUSTRI</td></tr>
                <tr>
                  <td>40</td>
                  <td>TEKNIK PEMESINAN</td>
                </tr>
                <tr>
                  <td>41</td>
                  <td>TEKNIK PENDINGIN DAN TATA UDARA</td>
                </tr>
                <tr>
                  <td>42</td>
                  <td>TEKNIK PENGECORAN LOGAM</td>
                </tr>
                <tr>
                  <td>43</td>
                  <td>TEKNIK PENGELASAN</td>
                </tr>
                <tr>
                  <td>44</td>
                  <td>TEKNIK PENGELASAN KAPAL</td>
                </tr>
                <tr>
                  <td>45</td>
                  <td>TEKNIK PENGOLAHAN MINYAK DAN GAS</td>
                </tr>
                <tr>
                  <td>46</td>
                  <td>TEKNIK PENGOLAHAN MINYAK, GAS DAN PETROKIMIA</td>
                </tr>
                <tr>
                  <td>47</td>
                  <td>TEKNIK PERBAIKAN BODI OTOMOTIF</td>
                </tr>
                <tr>
                  <td>48</td>
                  <td>TEKNIK PERKAPALAN</td>
                </tr>
                <tr>
                  <td>49</td>
                  <td>TEKNIK PERMINYAKAN</td>
                </tr>
                 <tr>
                  <td>50</td>
                  <td>TEKNIK PLAMBING DAN SANITASI</td>
                </tr>
                <tr>
                  <td>51</td>
                  <td>TEKNIK PRODUKSI PERMINYAKAN</td>
                </tr>
                <tr>
                  <td>52</td>
                  <td>TEKNIK SEPEDA MOTOR</td>
                </tr>
                <tr>
                  <td>53</td>
                  <td>TEKNIKA KAPAL NIAGA</td>
                </tr>
                <tr>
                  <td>54</td>
                  <td>TEKNIKA KAPAL PENANGKAP IKAN</td>
                </tr>
                <tr>
                  <td>55</td>
                  <td>TEKNOLOGI PESAWAT UDARA</td></tr>
                <tr>
                  <td>56</td>
                  <td>GEOLOGI PERTAMBANGAN</td>
                </tr>
                <tr>
                  <td>57</td>
                  <td>TEKNIK FURNITUR</td>
                </tr>
                <tr>
                  <td>58</td>
                  <td>TEKNIK GAMBAR BANGUNAN</td>
                </tr>
                <tr>
                  <td>59</td>
                  <td>TEKNIK KONSTRUKSI BAJA</td>
                </tr>
                <tr>
                  <td>60</td>
                  <td>TEKNIK KONSTRUKSI BATU DAN BETON</td>
                </tr>
                <tr>
                  <td>61</td>
                  <td>TEKNIK KONSTRUKSI KAYU</td>
                </tr>
                <tr>
                  <td>62</td>
                  <td>TEKNIK SURVEI DAN PEMETAAN</td></tr>
                <tr>
                  <td>63</td>
                  <td>ELEKTRONIKA PESAWAT UDARA</td>
                </tr>
                <tr>
                  <td>64</td>
                  <td>KELISTRIKAN PESAWAT UDARA</td>
                </tr>
                <tr>
                  <td>65</td>
                  <td>PEMELIHARAAN DAN PERBAIKAN INSTRUMEN ELEKTRONIKA</td>
                </tr>
                <tr>
                  <td>66</td>
                  <td>PEMELIHARAAN DAN PERBAIKAN INSTRUMEN ELEKTRONIKA PESAWAT UDARA</td>
                </tr>
                <tr>
                  <td>67</td>
                  <td>REKAYASA PERANGKAT LUNAK</td>
                </tr>
                <tr>
                  <td>68</td>
                  <td>TEKNIK ELEKTRONIKA</td>
                </tr>
                <tr>
                  <td>69</td>
                  <td>TEKNIK ELEKTRONIKA INDUSTRI</td>
                </tr>
                <tr>
                  <td>70</td>
                  <td>TEKNIK ELEKTRONIKA KOMUNIKASI</td>
                </tr>
                <tr>
                  <td>71</td>
                  <td>TEKNIK KOMPUTER DAN INFORMATIKA</td>
                </tr>
                <tr>
                  <td>72</td>
                  <td>TEKNIK KOMPUTER DAN JARINGAN</td>
                </tr>
                 <tr>
                  <td>73</td>
                  <td>TEKNIK TELEKOMUNIKASI</td>
                </tr>
                <tr>
                  <td>74</td>
                  <td>TEKNIK TRANSMISI TELEKOMUNIKASI</td>
                </tr>
                <tr>
                  <td>75</td>
                  <td>TEKNOLOGI INFORMASI DAN KOMUNIKASI</td>
                </tr>
                <tr>
                  <td>76</td>
                  <td>TEKNIK AUDIO VIDEO</td>
                </tr>
                <tr>
                  <td>77</td>
                  <td>TEKNIK BROADCASTING</td>
                </tr>
                <tr>
                  <td>78</td>
                  <td>TEKNIK TRANSMISI</td></tr>
                <tr>
                  <td>79</td>
                  <td>INSTRUMENTASI INDUSTRI</td>
                </tr>
                <tr>
                  <td>80</td>
                  <td>KELISTRIKAN KAPAL</td>
                </tr>
                <tr>
                  <td>81</td>
                  <td>KELISTRIKAN PESAWAT UDARA</td>
                </tr>
                <tr>
                  <td>82</td>
                  <td>PEMELIHARAAN DAN PERBAIKAN MOTOR DAN RANGKA PESAWAT UDARA</td>
                </tr>
                <tr>
                  <td>83</td>
                  <td>TEKNIK DISTRIBUSI TENAGA LISTRIK</td>
                </tr>
                <tr>
                  <td>84</td>
                  <td>TEKNIK INSTALASI PEMANFAATAN TENAGA LISTRIK</td>
                </tr>
                <tr>
                  <td>85</td>
                  <td>TEKNIK INSTALASI TENAGA LISTRIK</td>
                </tr>
                <tr>
                  <td>86</td>
                  <td>TEKNIK JARINGAN TENAGA LISTRIK</td>
                </tr>
                <tr>
                  <td>87</td>
                  <td>TEKNIK TRANSMISI TENAGA LISTRIK</td>
                </tr>
                <tr>
                  <td>88</td>
                  <td>TEKNIK JARINGAN AKSES</td>
                </tr>
                <tr>
                  <td>89</td>
                  <td>ANIMASI</td>
                </tr>
                <tr>
                  <td>90</td>
                  <td>DESAIN KOMUNIKASI VISUAL</td>
                </tr>
                <tr>
                  <td>91</td>
                  <td>MULTIMEDIA</td>
                </tr>
                <tr>
                  <td>92</td>
                  <td>TEKNIK PRODUKSI DAN PENYIARAN PROGRAM RADIO</td>
                </tr>
                <tr>
                  <td>93</td>
                  <td>TEKNIK PRODUKSI DAN PENYIARAN PROGRAM PERTELEVISIAN</td>
                </tr>
                 <tr>
                  <td>94</td>
                  <td>TEKNIK PRODUKSI DAN PENYIARAN PROGRAM RADIO DAN PERTELEVISIAN</td>
                </tr>
                <tr>
                  <td>95</td>
                  <td>ADMINISTRASI</td>
                </tr>
                <tr>
                  <td>96</td>
                  <td>ADMINISTRASI PERKANTORAN</td>
                </tr>
                <tr>
                  <td>97</td>
                  <td>AGRIBISNIS ANEKA TERNAK</td>
                </tr>
                <tr>
                  <td>98</td>
                  <td>AGRIBISNIS DAN AGROINDUSTRI</td>
                </tr>
                <tr>
                  <td>99</td>
                  <td>AKUNTANSI</td></tr>
                <tr>
                  <td>100</td>
                  <td>BISNIS DAN MANAJEMEN</td>
                </tr>
                <tr>
                  <td>101</td>
                  <td>KEUANGAN</td>
                </tr>
                <tr>
                  <td>102</td>
                  <td>PEMASARAN</td>
                </tr>
                <tr>
                  <td>103</td>
                  <td>PENGAWASAN MUTU</td>
                </tr>
                <tr>
                  <td>104</td>
                  <td>PENGAWASAN MUTU HASIL PERTANIAN</td>
                </tr>
                <tr>
                  <td>105</td>
                  <td>PENGAWASAN MUTU HASIL PERTANIAN DAN PERIKANAN</td>
                </tr>
                <tr>
                  <td>106</td>
                  <td>PERBANKAN</td>
                </tr>
                <tr>
                  <td>107</td>
                  <td>PERBANKAN SYARIAH</td>
                </tr>
                <tr>
                  <td>108</td>
                  <td>PRODUKSI GRAFIKA</td>
                </tr>
                <tr>
                  <td>109</td>
                  <td>TATA NIAGA</td>
                </tr>
                 <tr>
                  <td>110</td>
                  <td>TEKNIK DAN MANAJEMEN PERGUDANGAN</td>
                </tr>
                <tr>
                  <td>111</td>
                  <td>TEKNIK PERGUDANGAN</td>
                </tr>
                <tr>
                  <td>112</td>
                  <td>BAHASA</td>
                </tr>
                <tr>
                  <td>113</td>
                  <td>AKOMODASI PERHOTELAN</td>
                </tr>
                <tr>
                  <td>114</td>
                  <td>PARIWISATA</td>
                </tr>
                <tr>
                  <td>115</td>
                  <td>PATISERI</td></tr>
                <tr>
                  <td>116</td>
                  <td>USAHA PERJALANAN WISATA</td>
                </tr>
              </tbody>
            </table> 
          </div>
        </div>

      </div>
    </div>
  <!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection