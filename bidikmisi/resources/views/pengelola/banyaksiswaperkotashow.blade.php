<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Banyak Siswa Per Kota</title>
</head>
<body>
  <div align="center">
    <h2>BANYAK SISWA YANG MENDAFTAR BIDIKMISI PER KOTA/KABUPATEN</h2>
    <h2>DI POLITEKNIK NEGERI PADANG</h2>
    <h2>PROVINSI {{ $prov_sklh }} TAHUN {{ $tahun }}</h2>
  </div>
  <hr>
  <div>
    <table class="table">
      <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>Nama Kota/Kab</th>
                  <th>Jumlah Siswa</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($sekolahs as $data)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $data->kota_sklh }}</td>
                    <td>{{ $data->total }}</td>
                  </tr>
                  @endforeach
                </tbody>
    </table>
  </div>
  <style>
    body {
      font-size: 12px;
      font-family: 'Times New Roman', Times, serif;
    }
    h2 {
      margin: 0px;
      padding: 0px;
    }
    table {
      border-collapse: collapse;
      width: 100%
    }
    th, td {
      padding: 3px;
    }
    .table td, th {
      border: 1px solid #ccc;
    }
  </style>
</body>
</html>