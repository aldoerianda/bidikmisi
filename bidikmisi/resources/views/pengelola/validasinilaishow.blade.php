<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Validasi Nilai</title>
</head>
<body>
  <div align="center">
    <h2>DATA VALIDASI NILAI SISWA YANG MENDAFTAR BIDIKMISI</h2>
    <h2>DI POLITEKNIK NEGERI PADANG TAHUN {{ $tahun }}</h2>
  </div>
  <hr>
  <div>
    <table class="table">
       <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>NISN</th>
                  <th>Nama Siswa</th>
                  <th>Tanggal Daftar</th>
                  <th>Total Nilai Smst 4</th>
                  <th>Jumlah Mapel Smst 4</th>
                  <th>Total Nilai Smst 5</th>
                  <th>Jumlah Mapel Smst 5</th>
                  <th>Rata-Rata</th>
                  <th>Keterangan</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($nilais as $nilai)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $nilai->nisn }}</td>
                    <td>{{ $nilai->nama_siswa }}</td>
                    <td>{{ $nilai->tgl_daftar }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_4 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_4 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_5 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_5 }}</td>
                    <td>{{ number_format($nilai->rata,2) }}</td>
                    <td>
                      @if($nilai->rata > '0' && $nilai->rata <= '100')
                       Valid
                      @else Tidak Valid
                      @endif
                    </td>
                    
                  </tr>
                  @endforeach
                </tbody>
    </table>
  </div>
  <style>
    body {
      font-size: 12px;
      font-family: 'Times New Roman', Times, serif;
    }
    h2 {
      margin: 0px;
      padding: 0px;
    }
    table {
      border-collapse: collapse;
      width: 100%
    }
    th, td {
      padding: 3px;
    }
    .table td, th {
      border: 1px solid #ccc;
    }
  </style>
</body>
</html>