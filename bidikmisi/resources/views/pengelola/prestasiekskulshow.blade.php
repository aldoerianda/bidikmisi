<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Prestasi Ekskul Siswa</title>
</head>
<body>
  <div align="center">
    <h2>PRESTASI EKSKUL SISWA YANG MENDAFTAR BIDIKMISI</h2>
    <h2>DI POLITEKNIK NEGERI PADANG TAHUN {{ $tahun }}</h2>
  </div>
  <hr>
  <div>
    <table class="table">
       <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>NISN</th>
                  <th>Nama Siswa</th>
                  <th>Asal Sekolah</th>
                  {{-- <th>Rangking Smst 4</th>
                  <th>Total Nilai Smst 4</th>
                  <th>Jumlah Mapel Smst 4</th>
                  <th>Rangking Smst 5</th>
                  <th>Total Nilai Smst 5</th>
                  <th>Jumlah Mapel Smst 5</th> --}}
                  <th>Prestasi Ekskul</th>
                  <th>Tanggal Daftar</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($nilais as $nilai)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $nilai->nisn }}</td>
                    <td>{{ $nilai->nama_siswa }}</td>
                    <td>{{ $nilai->nama_sklh }}</td>
                    {{-- 
                    <td>{{ $nilai->rang_smst_4 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_4 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_4 }}</td>
                    <td>{{ $nilai->rang_smst_5 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_5 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_5 }}</td> --}}
                    <td>{{ $nilai->pres_ekskul }}</td>
                    <td>{{ $nilai->tgl_daftar }}</td>
                  </tr>
                  @endforeach
                </tbody>
    </table>
  </div>
  <style>
    body {
      font-size: 12px;
      font-family: 'Times New Roman', Times, serif;
    }
    h2 {
      margin: 0px;
      padding: 0px;
    }
    table {
      border-collapse: collapse;
      width: 100%
    }
    th, td {
      padding: 3px;
    }
    .table td, th {
      border: 1px solid #ccc;
    }
  </style>
</body>
</html>