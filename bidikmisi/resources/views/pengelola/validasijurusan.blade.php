@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful animated fadeIn">
        <div class="panel-heading">
          <h3 class="panel-title">Data Validasi Jurusan</h3>
          <a class='col-lg-offset-10 btn btn-success' href="{{url('/pengelola/validatejurusan/validasi')}}">Validasi</a>
        </div>
        <div class="panel-body">
          <form action="{{ route('validasijurusan.show') }}" method="post">
            {{ csrf_field()}}
            <div class="form-group">
              <div class="col-md-2 col-xs-9">
                <select name="tahun" class="form-control select">
                  <option value="">== Pilih Tahun ==</option>
                  {{ $now = Carbon\carbon::now()->year }}
                  @for ($i = 2017; $i <= $now ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                  @endfor
                </select>             
              </div>
            </div>
            <input type="submit" class="btn btn-success" name="cetak" value="Cetak">
          </form>
        </div>
        <div class="panel-body table-responsive">
          <table class="table datatable table-hover">
            <thead>
              <tr>
                <th>No.</th>
                <th>No. Pendaftaran</th>
                <th>NISN</th>
                {{-- <th>Tanggal Daftar</th> --}}
                <th>Jurusan Sekolah</th>
                <th>Pil. Prodi 1</th>
                <th>Pil. Prodi 2</th>
                <th>Keterangan</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($pendaftarans as $pendaftaran)
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $pendaftaran->no_pend}}</td>
                <td>{{ $pendaftaran->nisn }}</td>
                {{-- <td>{{ $pendaftaran->tgl_daftar }}</td> --}}
                <td>{{ $pendaftaran->jur_sekolah }}</td>
                <td>{{ $pendaftaran->pil_prodi_1}}</td>
                <td>{{ $pendaftaran->pil_prodi_2 }}</td>
                <td>{{ check($pendaftaran->jur_sekolah,$pendaftaran->pil_prodi_1,$pendaftaran->pil_prodi_2)}}</td>
              </tr>
              @endforeach
            </tbody>
          </table> 
        </div>
      </div>
    </div>
  </div>
<!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script> 
@endsection