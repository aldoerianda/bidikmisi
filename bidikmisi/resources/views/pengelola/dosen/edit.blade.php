@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-colorful">
          <div class="panel-heading">
              <h3 class="panel-title">Form dosen</h3>
          </div>

            @include('includes.messages')

          <form role="form" action="{{ route('dosen.update',$dosen->nidn)}}" method="post">
          {{ csrf_field()}}
          {{ method_field('PATCH')}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nidn">NIDN</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nidn" name="nidn" placeholder="NIDN" value="{{$dosen->nidn}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nama_dosen">Nama Dosen</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nama_dosen" name="nama_dosen" placeholder="Nama Dosen" value="{{$dosen->nama_dosen}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="tempat_lahir">Tempat Lahir</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="Tempat Lahir" value="{{$dosen->tempat_lahir}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="tgl_lahir">Tanggal Lahir</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="Tanggal Lahir" value="{{$dosen->tgl_lahir}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jekel">Jenis Kelamin</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="jekel" value="L" {{$dosen->jekel == 'L' ? 'checked' :''}}>Laki-Laki</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="jekel" value="P" {{$dosen->jekel == 'P' ? 'checked' :''}}>Perempuan</label>
              </div>
            </div>
          </div>
           {{-- <div class="box-body">
            <div class="form-group">
              <label for="no_hp">No. HP</label>
              <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="no_hp" value="{{ $dosen->no_hp}}">
            </div>
          </div>
           <div class="box-body">
            <div class="form-group">
              <label for="alamat">Alamat</label>
              <input type="text" class="form-control" id="alamat" name="alamat" placeholder="alamat" value="{{ $dosen->alamat}}">
            </div>
          </div> --}}

          <div class="panel-footer">
              <button type="submit" class="btn btn-primary">Simpan</button>
              <a href='{{route("dosen.index")}}' class="btn btn-warning">Kembali</a>
          </div>
          </form>
        </div>
      </div>
      </div>
  </section>
</div>
  @endsection