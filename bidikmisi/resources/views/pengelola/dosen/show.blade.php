@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Visitor</h3>
            </div>
            <!-- /.box-header -->
            <div class="panel-body table-responsive">
              <a class=' btn btn-success' href="{{ route('dosen.create')}}">Tambah Visitor</a>
              <button class="btn btn-success" data-toggle="modal" data-target="#modal_iedosen">Import Data</button>
              <a class='btn btn-success' href="{{ route('dosen.export')}}">Export Data</a>
            </div>
            <div class="panel-body table-responsive">
              <table class="table datatable table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NIDN</th>
                  <th>Nama Visitor</th>
                  <th>Tempat Lahir</th>
                  <th>Tanggal Lahir</th>
                  <th>JK</th>
                 {{--  <th>No. HP</th>
                  <th>Alamat</th> --}}
                  <th width="5%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($dosens as $dosen)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $dosen->nidn }}</td>
                    <td>{{ $dosen->nama_dosen }}</td>
                    <td>{{ $dosen->tempat_lahir }}</td>
                    <td>{{ $dosen->tgl_lahir }}</td>
                    <td>{{ $dosen->jekel }}</td>
                    {{-- <td>{{ $dosen->no_hp }}</td>
                    <td>{{ $dosen->alamat }}</td> --}}
                    <td><a href="{{route('dosen.edit',$dosen->nidn)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $dosen->nidn }}" method="post" action="{{route('dosen.destroy',$dosen->nidn)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data dosen?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $dosen->nidn }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script> 
@endsection