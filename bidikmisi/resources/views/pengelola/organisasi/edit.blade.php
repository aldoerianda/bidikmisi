@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Form Data Organisasi</h3>
        </div>

        @include('includes.messages')

        <form role="form" action="{{ route('organisasi.update',$data->id_organisasi)}}" method="post">
          {{ csrf_field()}}
          {{ method_field('PATCH')}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">NISN</label>
              <div class="col-md-9 col-xs-12">
                <select class="form-control select" name="nisn">
                  <option value="{{$data->nisn}}">{{ $data->nisn }}</option>

                  <?php foreach ($siswa as $key => $d) {?>
                    <option value="{{ $d->nisn }}"> {{ $d->nisn }} </option> <?php
                  } ?>
                </select>
              </div>
            </div>
          </div>
         <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="temp_orga">Tempat Organisasi</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="temp_orga" name="temp_orga" placeholder="Tempat Organisasi" value="{{$data->temp_orga}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nama_orga">Nama Organisasi</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nama_orga" name="nama_orga" placeholder="nama_orga" value="{{$data->nama_orga}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jabatan">Jabatan</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="jabatan" value="{{$data->jabatan}}">
              </div>
            </div>
          </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <a href='{{route("organisasi.index")}}' class="btn btn-warning">Kembali</a>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection