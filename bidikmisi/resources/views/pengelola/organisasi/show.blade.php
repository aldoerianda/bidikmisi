@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Organisasi</h3>
              <a class='col-lg-offset-10 btn btn-success' href="{{ route('organisasi.create')}}">Tambah Data</a>
            </div>
            <!-- /.box-header -->
            <div class="panel-body table-responsive">
              <table class="table datatable table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NISN</th>
                  <th>Tempat Organisasi</th>
                  <th>Nama Organisasi</th>
                  <th>Jabatan</th>
                  <th width="5%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($data as $d)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $d->nisn }}</td>
                    <td>{{ $d->temp_orga }}</td>
                    <td>{{ $d->nama_orga }}</td>
                    <td>{{ $d->jabatan }}</td>
                    <td><a href="{{route('organisasi.edit',$d->id_organisasi)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $d->id_organisasi }}" method="post" action="{{route('organisasi.destroy',$d->id_organisasi)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data Organisasi?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $d->id_organisasi }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection