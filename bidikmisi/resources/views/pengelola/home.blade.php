@extends('pengelola.layouts.app')
@section('head')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<style type="text/css">
 .box{
  width:800px;
  margin:0 auto;
}
</style>
<script type="text/javascript">
  var analytics3 = <?php echo $kota_sklh; ?>

  var analytics2 = <?php echo $prov_sklh; ?>

  var analytics = <?php echo $pil_prodi_1; ?>

  google.charts.load('current', {'packages':['corechart']});

  google.charts.setOnLoadCallback(drawChart);

  function drawChart()
  {
    var data = google.visualization.arrayToDataTable(analytics);
    var options = {
      title : ''
    };
    var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));
    chart.draw(data, options);

    var data2 = google.visualization.arrayToDataTable(analytics2);
    var options2 = {
     title : ''
   };
   var chart2 = new google.visualization.PieChart(document.getElementById('pie_chart2'));
   chart2.draw(data2, options2);

   var data3 = google.visualization.arrayToDataTable(analytics3);
   var options3 = {
     title : ''
   };
   var chart3 = new google.visualization.PieChart(document.getElementById('pie_chart3'));
   chart3.draw(data3, options3);
 }
</script>
@endsection
@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="page-content-wrap">
  <!-- Small boxes (Stat box) -->
  <div class="col-md-12">
    <div class="row">
      <!-- ./col -->
      <div class="col-md-3">
        <!-- small box -->
        <!-- START WIDGET SLIDER -->
        <div class="widget widget-warning widget-item-icon">
          <div class="widget-item-right">
            <span class="fa fa-users"></span>
          </div>
          <div class="widget-data-left">
            <div class="widget-int num-count">
              <?php echo $siswa; ?>
            </div>
            <div class="widget-title">Jumlah Siswa</div>
            <div class="widget-subtitle">Tahun {{(int)date('Y')}}</div>
          </div>                            
        </div>           
        <!-- END WIDGET SLIDER -->
      </div>
      <div class="col-md-3">
        <div class="widget widget-warning widget-item-icon">
          <div class="widget-item-right">
            <span class="fa fa-check-square-o"></span>
          </div>                             
          <div class="widget-data-left">
            <div class="widget-int num-count">
              <?php echo $validasi; ?>
            </div>
            <div class="widget-title">Jumlah Divalidasi</div>
            <div class="widget-subtitle">Tahun {{(int)date('Y')}}</div>
          </div>
        </div>     
      </div>
      <div class="col-md-3">
        <div class="widget widget-warning widget-item-icon">
          <div class="widget-item-right">
            <span class="fa fa-user"></span>
          </div>
          <div class="widget-data-left">
            <div class="widget-int num-count">
              <?php echo $dosen; ?></div>
              <div class="widget-title">Jumlah Dosen</div>
              <div class="widget-subtitle"></div>
            </div>                           
          </div>   
        </div>
        <div class="col-md-3">
          <div class="widget widget-info widget-padding-sm">
            <div class="widget-big-int plugin-clock">00:00</div>                            
            <div class="widget-subtitle plugin-date">Loading...</div>
            <div class="widget-controls">                                
              <a href="#" class="widget-control-right widget-remove" data-toggle="tooltip" data-placement="left" title="Remove Widget"><span class="fa fa-times"></span></a>
            </div>                            
            <div class="widget-buttons widget-c3">
              <div class="col">
                <a href="#"><span class="fa fa-clock-o"></span></a>
              </div>
              <div class="col">
                <a href="#"><span class="fa fa-bell"></span></a>
              </div>
              <div class="col">
                <a href="#"><span class="fa fa-calendar"></span></a>
              </div>
            </div>                            
          </div>
        </div>
        <!-- ./col -->

        <!-- ./col -->

        <!-- ./col -->
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title"></h3>
            </div>
            <div class="panel-body table-responsive">   
              <div class="col-md-6">
                <h3 align="center">Persentase Banyak Siswa per Provinsi</h3><br><hr>
                <div id="pie_chart2" style="width:500px; height:350px;">
                </div>
              </div>                 
              <!-- START DOUNT CHART -->
              <div class="col-md-6">
                <h3 align="center">Persentase Banyak Siswa per Kabupaten/kota<br>di Sumatera Barat</h3><hr>
                <div id="pie_chart3" style="width:500px; height:350px;">
                </div>
                <!-- END DOUNT CHART -->
              </div>
            </div>
          </div>
        </div>
        <!-- /.row -->
        <div class="row">
          <div class="col-md-12">
            <div class="panel panel-colorful animated fadeIn">
              <div class="panel-heading">
                <h3 class="panel-title"></h3>
              </div>
              <div class="panel-body table-responsive">
                <div class="col-md-5">
                  <h3 align="center">Persentase Pemilihan Prodi</h3><hr>
                  <div id="pie_chart" style="width:600px; height:350px;"></div>
                </div><div class="col-md-1"></div>  
                <div class="col-md-5">
                  <h3 align="center">Jumlah Siswa per Sekolah</h3><hr><br><br><br>
                  <div class="chart-holder" id="dashboard-donut-1" style="height: 250px;"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
        <!-- /.row -->
        <!-- /.content -->
      </div>
    </div>
    <!-- /.content-wrapper -->
    @endsection
    @section('script')
    <script>
      $(function(){
        Morris.Donut({
          element: 'dashboard-donut-1',
          data: [
          {label: "SMK", value: <?php echo $datasmk; ?>},
          {label: "SMA", value: <?php echo $datasma; ?>},
          {label: "MA", value: <?php echo $datama; ?>}
          ],
          colors: ['#33414E', '#1caf9a', '#FEA223'],
          resize: true
        }); 
      });
    </script>
    @endsection
