@extends('pengelola.layouts.app')

@section('head')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('pengelola/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
@endsection

@section('main-content')
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data pengelola</h3>
              <a class='col-lg-offset-10 btn btn-success' href="{{ route('pengelola.create')}}">Tambah Pengelola</a>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Email</th>
                  <th>Password</th>
                  <th>Nama</th>
                  <th>Tempat Lahir</th>
                  <th>Tanggal Lahir</th>
                  <th>JK</th>
                  <th>No. HP</th>
                  <th>Alamat</th>
                  <th width="5%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($pengelolas as $pengelola)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $pengelola->email}}</td>
                    <td>{{ $pengelola->password}}</td>
                    <td>{{ $pengelola->nama}}</td>
                    <td>{{ $pengelola->tempat_lahir}}</td>
                    <td>{{ $pengelola->tgl_lahir}}</td>
                    <td>{{ $pengelola->jekel}}</td>
                    <td>{{ $pengelola->no_hp}}</td>
                    <td>{{ $pengelola->alamat}}</td>
                
                    <td><a href="{{route('pengelola.edit',$pengelola->id)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $pengelola->id }}" method="post" action="{{route('pengelola.destroy',$pengelola->id)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data pengelola?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $pengelola->id }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    </section>

    </section>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('pengelola/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('pengelola/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script>
  $(function () {
    $('#myTable').DataTable()
  })
</script>
@endsection