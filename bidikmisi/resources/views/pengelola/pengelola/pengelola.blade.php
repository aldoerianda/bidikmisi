@extends('pengelola.layouts.app')

@section('main-content')
<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
              <h3 class="box-title">Form Pengelola</h3>
          </div>

            @include('includes.messages')

          <form role="form" action="{{ route('pengelola.store')}}" method="post">
          {{ csrf_field()}}
          <div class="box-body">
            <div class="form-group">
              <label for="email">E-mail</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="email">
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label for="password">Password</label>
              <input type="text" class="form-control" id="password" name="password" placeholder="password">
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" id="nama" name="nama" placeholder="nama">
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label for="tempat_lahir">Tempat Lahir</label>
              <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="tempat_lahir">
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label for="tgl_lahir">Tanggal Lahir</label>
              <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="tgl_lahir">
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label for="jekel">Jenis Kelamin</label>
              <br>
              <input type="radio" name="jekel" value="P" checked="true">Perempuan
              <br>
              <input type="radio" name="jekel" value="L">Laki-Laki
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label for="no_hp">No. HP</label>
              <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="no_hp">
            </div>
          </div>
          <div class="box-body">
            <div class="form-group">
              <label for="alamat">Alamat</label>
              <input type="text" class="form-control" id="alamat" name="alamat" placeholder="alamat">
            </div>
          </div>
         
          
          <div class="box-footer">
              <button type="submit" class="btn btn-primary">Simpan</button>
              <a href='{{route("pengelola.index")}}' class="btn btn-warning">Kembali</a>
          </div>
          </form>
        </div>
      </div>
      </div>
  </section>
</div>
  @endsection