<!DOCTYPE html>
<html lang="en" class="body-full-height">
<head>        
  <!-- META SECTION -->
  <title>Login | Bidikmisi PNP</title>           
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <link rel="icon" href="{{asset('img/logopnp3.png')}}" type="image/x-icon" />
  <!-- END META SECTION -->

  <!-- CSS INCLUDE -->        
  <link rel="stylesheet" type="text/css" id="theme" href="{{asset('css/theme-default.css')}}"/>
  <!-- EOF CSS INCLUDE -->                                    
</head>
<body>
  <div class="login-container">
    <div class="login-box animated fadeInDown">
      <div class="login-logo"></div>
      <div class="login-body">
        <div class="login-title"><strong>Welcome</strong>, Please login</div>
        @include('includes.messages')
        <form action="{{ route('signin')}}" class="form-horizontal" method="post">
          {{ csrf_field()}}
          <div class="form-group">
            <div class="col-md-12">
              <input type="text" class="form-control" name="username" placeholder="Username"/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <input type="password" class="form-control" name="password" placeholder="Password"/>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-12">
              <select class="form-control" style="background-color: #2b2d30" name="level">
                <option value="" selected="true">Level</option>
                <option value="Pengelola">Pengelola</option>
                <option value="Visitor">Visitor</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-6">

            </div>
            <div class="col-md-6">
              <button type="submit" class="btn btn-info btn-block">Log In</button>
            </div>
          </div>
        </form>
      </div>
      <div class="login-footer">
        <div class="pull-left">
          &copy; 2019 SIBMPNP
        </div>
      </div>
    </div>
    <!-- /.login-box-body -->
  </div>
  <!-- /.login-box -->
</body>
</html>
