@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Nilai Siswa</h3>
               <a class='col-lg-offset-10 btn btn-success' href="{{ route('nilai.create')}}">Tambah Nilai</a>
            </div>
            <div class="panel-body table-responsive">
              <table class="table datatable table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NISN</th>
                  <th>Nama Siswa</th>
                  <th>Nama Sekolah</th>
                  <th>Rangking Smst 4</th>
                  <th>Total Nilai Smst 4</th>
                  <th>Jumlah Mapel Smst 4</th>
                  <th>Rangking Smst 5</th>
                  <th>Total Nilai Smst 5</th>
                  <th>Jumlah Mapel Smst 5</th>
                  {{-- <th>Rangking Smst 6</th>
                  <th>Total Nilai Smst 6</th>
                  <th>Jumlah Mapel Smst 6</th>
                  <th>Nilai UNAS</th>
                  <th>Jumlah Mapel UNAS</th> --}}
                  <th>Prestasi Ekskul</th>
                  <th>Jumlah Prestasi</th>
                  <th>Tanggal Daftar</th>
                  <th width="5%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($nilais as $nilai)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $nilai->nisn }}</td>
                    <td>{{ $nilai->nama_siswa }}</td>
                    <td>{{ $nilai->nama_sklh }}</td>
                    <td>{{ $nilai->rang_smst_4 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_4 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_4 }}</td>
                    <td>{{ $nilai->rang_smst_5 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_5 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_5 }}</td>
                   {{--  <td>{{ $nilai->rang_smst_6 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_6 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_6 }}</td>
                    <td>{{ $nilai->nilai_unas }}</td>
                    <td>{{ $nilai->jmlh_mapel_unas }}</td> --}}
                    <td>
                      {{ $nilai->pres_ekskul }}
                    </td>
                    <td>
                      {{$nilai->jml_prestasi}}
                    </td>
                    <td>{{ $nilai->tgl_daftar }}</td>
                    <td><a href="{{route('nilai.edit',$nilai->nisn)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $nilai->nisn }}" method="post" action="{{route('nilai.destroy',$nilai->nisn)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data nilai?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $nilai->nisn }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script> 
@endsection