@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-colorful">
          <div class="panel-heading">
              <h3 class="panel-title">Form Nilai</h3>
          </div>

            @include('includes.messages')

          <form role="form" action="{{ route('nilai.store')}}" method="post">
          {{ csrf_field()}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">NISN</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nama_siswa">Nama Siswa</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nama_siswa" name="nama_siswa" placeholder="Nama SIswa">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nama_sklh">Nama Sekolah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nama_sklh" name="nama_sklh" placeholder="Nama Sekolah">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="rang_smst_4">Rangking Semester 4</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="rang_smst_4" name="rang_smst_4" placeholder="Rangking Semester 4">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="ttl_nilai_smst_4">Total Nilai Semester 4</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="ttl_nilai_smst_4" name="ttl_nilai_smst_4" placeholder="Total Nilai Semester 4">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jmlh_mapel_smst_4">Jumlah Mapel Semester 4</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jmlh_mapel_smst_4" name="jmlh_mapel_smst_4" placeholder="Jumlah Mapel Semester 4">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="rang_smst_5">Rangking Semester 5</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="rang_smst_5" name="rang_smst_5" placeholder="Rangking Semester 5">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="ttl_nilai_smst_5">Total Nilai Semester 5</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="ttl_nilai_smst_5" name="ttl_nilai_smst_5" placeholder="Total Nilai Semester 5">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jmlh_mapel_smst_5">Jumlah Mapel Semester 5</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jmlh_mapel_smst_5" name="jmlh_mapel_smst_5" placeholder="Jumlah Mapel Semester 5">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="rang_smst_6">Rangking Semester 6</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="rang_smst_6" name="rang_smst_6" placeholder="Rangking Semester 6">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="ttl_nilai_smst_6">Total Nilai Semester 6</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="ttl_nilai_smst_6" name="ttl_nilai_smst_6" placeholder="Total Nilai Semester 6">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jmlh_mapel_smst_6">Jumlah Mapel Semester 6</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jmlh_mapel_smst_6" name="jmlh_mapel_smst_6" placeholder="Jumlah Mapel Semester 6">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nilai_unas">Nilai UNAS</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nilai_unas" name="nilai_unas" placeholder="Nilai UNAS">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jmlh_mapel_unas">Jumlah Mapel UNAS</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jmlh_mapel_unas" name="jmlh_mapel_unas" placeholder="Jumlah Mapel UNAS">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="pres_ekskul">Prestasi Ekskul</label>
              <div class="col-md-9 col-xs-12">
              <textarea id="pres_ekskul" name="pres_ekskul" class="form-control" rows="4"></textarea>
            </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="tgl_daftar">Tanggal Daftar</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="tgl_daftar" name="tgl_daftar" placeholder="Tanggal Daftar">
              </div>
            </div>
          </div>
          <div class="panel-footer">
              <button type="submit" class="btn btn-primary">Simpan</button>
              <a href='{{route("nilai.index")}}' class="btn btn-warning">Kembali</a>
          </div>
          </form>
        </div>
      </div>
      </div>
  </section>
</div>
  @endsection