@extends('pengelola.layouts.app')

@section('head')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('pengelola/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
@endsection

@section('main-content')
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data nilai</h3>
            </div>
          
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>NISN</th>
                  <th>Nama Siswa</th>
                  <th>Nama Sekolah</th>
                  <th>Rangking Smst 4</th>
                  <th>Total Nilai Smst 4</th>
                  <th>Jumlah Mapel Smst 4</th>
                  <th>Rangking Smst 5</th>
                  <th>Total Nilai Smst 5</th>
                  <th>Jumlah Mapel Smst 5</th>
                  {{-- <th>Rangking Smst 6</th>
                  <th>Total Nilai Smst 6</th>
                  <th>Jumlah Mapel Smst 6</th>
                  <th>Nilai UNAS</th>
                  <th>Jumlah Mapel UNAS</th> --}}
                  <th>Prestasi Ekskul</th>
                  <th>Tanggal Daftar</th>
                  <th width="7%">Edit</th>
                  <th width="7%">Delete</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($nilais as $nilai)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $nilai->nisn }}</td>
                    <td>{{ $nilai->nama_siswa }}</td>
                    <td>{{ $nilai->nama_sklh }}</td>
                    <td>{{ $nilai->rang_smst_4 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_4 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_4 }}</td>
                    <td>{{ $nilai->rang_smst_5 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_5 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_5 }}</td>
                   {{--  <td>{{ $nilai->rang_smst_6 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_6 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_6 }}</td>
                    <td>{{ $nilai->nilai_unas }}</td>
                    <td>{{ $nilai->jmlh_mapel_unas }}</td> --}}
                    <td>{{ $nilai->pres_ekskul }}</td>
                    <td>{{ $nilai->tgl_daftar }}</td>
                    <td><a href="{{route('nilai.edit',$nilai->id)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                     <td>
                      <form id="delete-form-{{ $nilai->id }}" method="post" action="{{route('nilai.destroy',$nilai->id)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data nilai?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $nilai->id }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
       <div class="box-footer">
        <a href='{{route('nilai.index')}}' class="btn btn-warning">Kembali</a>
      </div>
    </section>

    </section>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('pengelola/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('pengelola/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script>
  $(function () {
    $('#myTable').DataTable()
  })
</script>
@endsection