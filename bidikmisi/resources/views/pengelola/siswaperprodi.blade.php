@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Siswa Per Prodi</h3>          
            </div>
            <div class="panel-body">
              <form action="{{ route('siswaperprodi.show') }}" method="post" class="form-horizontal">
              {{ csrf_field()}}
               <div class="form-group">
                <div class="col-md-2 col-xs-9">
                 <select name="prodi" class="form-control select">
                    <option value="">==Pilih Prodi==</option>
                    <option>21401 - D3 Teknik Mesin</option>
                    <option>21307 - D4 Teknik Manufaktur</option>
                    <option>21413 - D3 Teknik Alat Berat</option>
                    <option>22401 - D3 Teknik Sipil</option>
                    <option>22301 - D4 Perancangan Jalan dan Jembatan</option>
                    <option>22302 - D4 Manajemen Rekayasa Konstruksi</option>
                    <option>22304 - D4 Teknik Perencanaan Irigasi dan Rawa</option>
                    <option>20401 - D3 Teknik Elektronika</option>
                    <option>20301 - D4 Teknik Elektronika</option>
                    <option>20402 - D3 Teknik Telekomunikasi</option>
                    <option>20302 - D4 Teknik Telekomunikasi</option>
                    <option>20403 - D3 Teknik Listrik</option>
                    <option>56401 - D3 Teknik Komputer</option>
                    <option>57401 - D3 Manajemen Informatika</option>
                    <option>58301 - D4 Teknologi Rekayasa Perangkat Lunak</option>
                    <option>63411 - D3 Administrasi Bisnis</option>
                    <option>93401 - D3 Usaha Perjalanan Wisata</option>
                    <option>62401 - D3 Akuntansi</option>
                    <option>62301 - D4 Akuntansi</option>
                    <option>79402 - D3 Bahasa Inggris</option>
                  </select>
                  </div>
                </div>
              <div class="form-group">
                <div class="col-md-2 col-xs-9">
              <select name="tahun" class="form-control select">
                  <option value="">== Pilih Tahun ==</option>
                  {{ $now = Carbon\carbon::now()->year }}
                  @for ($i = 2017; $i <= $now ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                  @endfor
                </select>              
                </div>
              </div>
              <input type="submit" class="btn btn-success" name="cetak" value="Cetak">
            </form>
            </div>
            <div class="panel-body">
              <table class="table datatable">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NISN</th>
                  <th>Tanggal Daftar</th>
                  <th>Pil. Prodi 1</th>
                  <th>Pil. Prodi 2</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($pendaftarans as $pendaftaran)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $pendaftaran->nisn }}</td>
                    <td>{{ $pendaftaran->tgl_daftar }}</td>
                    <td>{{ $pendaftaran->pil_prodi_1}}</td>
                    <td>{{ $pendaftaran->pil_prodi_2 }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
@endsection