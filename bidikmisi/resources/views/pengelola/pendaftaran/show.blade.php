@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Pendaftaran Siswa</h3>
               <a class='col-lg-offset-10 btn btn-success' href="{{ route('pendaftaran.create')}}">Tambah Pendaftaran</a>
            </div>
            <div class="panel-body table-responsive">
              <table class="table datatable table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>No. Pendaftaran</th>
                  <th>NISN</th>
                  <th>Tanggal Daftar</th>
                  <th>Jenis Seleksi</th>
                  <th>Jurusan Sekolah</th>
                  <th>Pil. Prodi 1</th>
                  <th>Pil. Prodi 2</th>
                  <th width="5%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($pendaftarans as $pendaftaran)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $pendaftaran->no_pend}}</td>
                    <td>{{ $pendaftaran->nisn }}</td>
                    <td>{{ $pendaftaran->tgl_daftar }}</td>
                    <td>{{ $pendaftaran->jenis_seleksi }}</td>
                    <td>{{ $pendaftaran->jur_sekolah }}</td>
                    <td>{{ $pendaftaran->pil_prodi_1}}</td>
                    <td>{{ $pendaftaran->pil_prodi_2 }}</td>
                    <td><a href="{{route('pendaftaran.edit',$pendaftaran->id)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $pendaftaran->id }}" method="post" action="{{route('pendaftaran.destroy',$pendaftaran->id)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data pendaftaran?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $pendaftaran->id }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script> 
@endsection