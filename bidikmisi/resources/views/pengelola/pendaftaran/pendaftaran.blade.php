@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Form Pendaftaran</h3>
        </div>

        @include('includes.messages')

        <form role="form" action="{{ route('pendaftaran.store')}}" method="post">
          {{ csrf_field()}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="no_pend">No Pendaftaran</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="no_pend" name="no_pend" placeholder="No Pendaftaran">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">NISN</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="tgl_daftar">Tanggal Daftar</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="tgl_daftar" name="tgl_daftar" placeholder="Tanggal Daftar">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jenis_seleksi">Jenis Seleksi</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jenis_seleksi" name="jenis_seleksi" placeholder="Jenis Seleksi">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jur_sekolah">Jurusan Sekolah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="jur_sekolah" name="jur_sekolah" placeholder="Jurusan Sekolah">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="pil_prodi_1">Pil. Prodi 1</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="pil_prodi_1" name="pil_prodi_1" placeholder="Pil. Prodi 1">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="pil_prodi_2">Pil. Prodi 2</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="pil_prodi_2" name="pil_prodi_2" placeholder="Pil. Prodi 2">
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href='{{route("pendaftaran.index")}}' class="btn btn-warning">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection