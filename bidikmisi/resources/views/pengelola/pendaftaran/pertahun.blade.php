@extends('pengelola.layouts.app')

@section('head')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('pengelola/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
@endsection

@section('main-content')
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data pendaftaran</h3>
               
            </div>
            
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>No. Pendaftaran</th>
                  <th>NISN</th>
                  <th>Tanggal Daftar</th>
                  <th>Jenis Seleksi</th>
                  <th>Jurusan Sekolah</th>
                  <th>Pil. Prodi 1</th>
                  <th>Pil. Prodi 2</th>
                  <th width="7%">Edit</th>
                  <th width="7%">Delete</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($pendaftarans as $pendaftaran)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $pendaftaran->no_pend}}</td>
                    <td>{{ $pendaftaran->nisn }}</td>
                    <td>{{ $pendaftaran->tgl_daftar }}</td>
                    <td>{{ $pendaftaran->jenis_seleksi }}</td>
                    <td>{{ $pendaftaran->jur_sekolah }}</td>
                    <td>{{ $pendaftaran->pil_prodi_1}}</td>
                    <td>{{ $pendaftaran->pil_prodi_2 }}</td>
                    <td><a href="{{route('pendaftaran.edit',$pendaftaran->id)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                     <td>
                      <form id="delete-form-{{ $pendaftaran->id }}" method="post" action="{{route('pendaftaran.destroy',$pendaftaran->id)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data pendaftaran?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $pendaftaran->id }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
       <div class="box-footer">
        <a href='{{route('pendaftaran.index')}}' class="btn btn-warning">Kembali</a>
      </div>
    </section>

    </section>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('pengelola/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('pengelola/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script>
  $(function () {
    $('#myTable').DataTable()
  })
</script>
@endsection