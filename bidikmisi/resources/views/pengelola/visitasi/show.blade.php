@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Visitasi</h3>
            </div>
            <!-- /.box-header -->
            <div class="panel-body table-responsive">
              <a class='btn btn-success' href="{{ route('visitasi.create')}}">Tambah Data</a>
              <button class="btn btn-success" data-toggle="modal" data-target="#modal_ievisitasi">Import Data</button>
              <a class='btn btn-success' href="{{ route('visitasi.export')}}">Export Data</a>
            </div>
            <div class="panel-body table-responsive">
              <table class="table datatable table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NISN</th>
                  <th>Nama Siswa</th>
                  <th>Nama Dosen 1</th>
                  <th>Nama Dosen 2</th>
                  <th>Alamat</th>
                  <th>Tanggal Visit</th>
                  {{-- <th>Keterangan</th> --}}
                  <th width="5%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($visitasis as $data)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $data->nisn }}</td>
                    <td>{{ $data->nama_siswa }}</td>
                    <td>{{ $data->nama_dosen1 }}</td>
                    <td>{{ $data->nama_dosen2 }}</td>
                    <td>{{ $data->alamat }}</td>
                    <td>{{ $data->tgl_visit }}</td>
                    {{-- <td>{{ $data->keterangan }}</td> --}}
                    <td><a href="{{route('visitasi.edit',$data->id)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $data->id }}" method="post" action="{{route('visitasi.destroy',$data->id)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data visitasi?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $data->id }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection