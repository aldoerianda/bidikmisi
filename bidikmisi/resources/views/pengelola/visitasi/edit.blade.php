@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-colorful">
          <div class="panel-heading">
              <h3 class="panel-title">Form Edit Data Visitasi</h3>
          </div>

            @include('includes.messages')

          <form role="form" action="{{ route('visitasi.update',$visitasi->id)}}" method="post">
          {{ csrf_field()}}
          {{ method_field('PATCH')}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">Nama Siswa</label>
              <div class="col-md-9 col-xs-12">
              <select name="nisn" class="form-control select">
                <option value="{{ $visitasi->nisn}}">{{ $visitasi->nama_siswa}}</option>
                <option value="">==Pilih Nama Siswa ==</option>
                @foreach($siswas as $data)
                <option value="{{$data->nisn}}">{{ $data->nama_siswa }}</option>
                @endforeach
              </select>
            </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nidn">Nama Dosen 1</label>
              <div class="col-md-9 col-xs-12">
              <select name="nidn" class="form-control select">
                <option value="{{ $visitasi->nidn}}">{{ $visitasi->nama_dosen1}}</option>
                <option value="">==Pilih Nama Dosen 1==</option>
                @foreach($dosens as $data)
                <option value="{{ $data->nidn}}">{{ $data->nama_dosen }}</option>
                @endforeach
              </select>
            </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nidn2">Nama Dosen 2</label>
              <div class="col-md-9 col-xs-12">
              <select name="nidn2" class="form-control select">
                <option value="{{ $visitasi->nidn2}}">{{ $visitasi->nama_dosen2}}</option>
                <option value="">==Pilih Nama Dosen 2==</option>
                @foreach($dosens as $data)
                <option value="{{ $data->nidn }}">{{ $data->nama_dosen }}</option>
                @endforeach
              </select>
            </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="tgl_visit">Tanggal Visit</label>
              <div class="col-md-9 col-xs-12">
              <input type="date" class="form-control" id="tgl_visit" name="tgl_visit" placeholder="tgl_visit" value="{{ $visitasi->tgl_visit}}">
            </div>
            </div>
          </div>
         {{--  <div class="box-body">
            <div class="form-group">
              <label for="keterangan">Keterangan</label>
              <input type="text" class="form-control" id="keterangan" name="keterangan" placeholder="keterangan" value="{{ $visitasi->keterangan}}">
            </div>
          </div> --}}
          
          <div class="panel-footer">
              <button type="submit" class="btn btn-primary">Simpan</button>
              <a href='{{route("visitasi.index")}}' class="btn btn-warning">Kembali</a>
          </div>
          </form>
        </div>
      </div>
      </div>
  </section>
</div>
  @endsection