@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Berita</h3>
              <a class='col-lg-offset-10 btn btn-success' href="{{ route('berita.create')}}">Tambah Berita</a>
            </div>
            <!-- /.box-header -->
            <div class="panel-body table-responsive">
              <table class="table datatable table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>Judul</th>
                  <th>Isi</th>
                  <th width="6%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($beritas as $berita)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $berita->judul }}</td>
                    <td>{{ substr($berita->isi,0,200) }}</td>
                    <td><a href="{{route('berita.edit',$berita->id)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $berita->id }}" method="post" action="{{route('berita.destroy',$berita->id)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data Berita?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $berita->id }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
@endsection