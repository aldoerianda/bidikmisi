@extends('pengelola.layouts.app')
@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Form Berita</h3>
        </div>

        @include('includes.messages')

        <form role="form" action="{{ route('berita.update',$berita->id)}}" method="post">
          {{ csrf_field()}}
          {{ method_field('PATCH')}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="judul">Judul</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="judul" name="judul" placeholder="Judul" value="{{ $berita->judul}}">
              </div>
            </div>
          </div>
          
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="isi">Isi</label>
              <div class="col-md-9 col-xs-12">
                <textarea class="form-control" name="isi" id="isi" placeholder="Tulis Isi Berita" rows="20">{{$berita->isi}}</textarea>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href='{{route("berita.index")}}' class="btn btn-warning">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection