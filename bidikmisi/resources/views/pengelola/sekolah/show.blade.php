@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
      <div class="row">
        <div class="col-md-12">
          <div class="panel panel-colorful animated fadeIn">
            <div class="panel-heading">
              <h3 class="panel-title">Data Sekolah</h3>
               <a class='col-lg-offset-10 btn btn-success' href="{{ route('sekolah.create')}}">Tambah Data Sekolah</a>
            </div>
            <!-- /.box-header -->
            <div class="panel-body table-responsive">
              <table class="table datatable table-hover">
                <thead>
                <tr>
                  <th>No.</th>
                  <th>NISN</th>
                  <th>NPSN</th>
                  <th>Asal Sekolah</th>
                  <th>Provinsi Sekolah</th>
                  <th>Kota Sekolah</th>
                  <th>Tanggal Daftar</th>
                  <th width="5%">Aksi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($sekolahs as $sekolah)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $sekolah->nisn }}</td>
                    <td>{{ $sekolah->npsn }}</td>
                    <td>{{ $sekolah->nama_sklh }}</td>
                    <td>{{ $sekolah->prov_sklh }}</td>
                    <td>{{ $sekolah->kota_sklh }}</td>
                    <td>{{ $sekolah->tgl_daftar }}</td>
                    <td><a href="{{route('sekolah.edit',$sekolah->nisn)}}"><span class="glyphicon glyphicon-edit"></span></a>
                      <form id="delete-form-{{ $sekolah->nisn }}" method="post" action="{{route('sekolah.destroy',$sekolah->nisn)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data sekolah?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $sekolah->nisn }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script> 
@endsection