@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Form Sekolah</h3>
        </div>

        @include('includes.messages')

        <form role="form" action="{{ route('sekolah.update',$sekolah->nisn)}}" method="post">
          {{ csrf_field()}}
          {{ method_field('PATCH')}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">NISN</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN" value="{{$sekolah->nisn}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="npsn">NPSN</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="npsn" name="npsn" placeholder="NPSN" value="{{$sekolah->npsn}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nama_sklh">Asal Sekolah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nama_sklh" name="nama_sklh" placeholder="Asal Sekolah" value="{{$sekolah->nama_sklh}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="prov_sklh">Provinsi Sekolah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="prov_sklh" name="prov_sklh" placeholder="Provinsi Sekolah" value="{{$sekolah->prov_sklh}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="kota_sklh">Kota Sekolah</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="kota_sklh" name="kota_sklh" placeholder="Kota Sekolah" value="{{$sekolah->kota_sklh}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="tgl_daftar">Tanggal Daftar</label>
              <div class="col-md-9 col-xs-12">
                <input type="datetime" class="form-control" id="tgl_daftar" name="tgl_daftar" placeholder="Tanggal Daftar" value="{{$sekolah->tgl_daftar}}">
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <button type="submit" class="btn btn-primary">Simpan</button>
            <a href='{{route("sekolah.index")}}' class="btn btn-warning">Kembali</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection