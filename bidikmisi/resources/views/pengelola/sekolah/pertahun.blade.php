@extends('pengelola.layouts.app')

@section('head')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('pengelola/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
@endsection

@section('main-content')
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Sekolah</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>NISN</th>
                  <th>NPSN</th>
                  <th>Asal Sekolah</th>
                  <th>Provinsi Sekolah</th>
                  <th>Kota Sekolah</th>
                  <th>Tanggal Daftar</th>
                  <th width="7%">Edit</th>
                  <th width="7%">Delete</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($sekolahs as $sekolah)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $sekolah->nisn }}</td>
                    <td>{{ $sekolah->npsn }}</td>
                    <td>{{ $sekolah->nama_sklh }}</td>
                    <td>{{ $sekolah->prov_sklh }}</td>
                    <td>{{ $sekolah->kota_sklh }}</td>
                    <td>{{ $sekolah->tgl_daftar }}</td>
                    <td><a href="{{route('sekolah.edit',$sekolah->id)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                     <td>
                      <form id="delete-form-{{ $sekolah->id }}" method="post" action="{{route('sekolah.destroy',$sekolah->id)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data sekolah?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $sekolah->id }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
       <div class="box-footer">
        <a href='{{route('sekolah.index')}}' class="btn btn-warning">Kembali</a>
      </div>
    </section>

    </section>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('pengelola/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('pengelola/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script>
  $(function () {
    $('#myTable').DataTable()
  })
</script>
@endsection