@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-colorful animated fadeIn xn-drop-left">
          <div class="panel-heading">
            <h3 class="panel-title">Skor Fuzzy</h3>
          </div>
          <div class="panel-body">
            <form action="{{ route('pengelola.kuotafuzzy') }}" method="post">
              {{ csrf_field()}}
              <div class="form-group">
                <div class="col-md-2 col-xs-12">
                  <input type="text" name="kuota" class="form-group" placeholder="Kuota">              
                </div>
                <div class="col-md-2 col-xs-12">
                  <input type="submit" class="btn btn-success" name="submit" value="OK">
                </div>
              </div>
            </form>
          </div>
          <div class="panel-body table-responsive">
            <table class="table datatable table-hover">
              <thead>
                <tr>
                  <th>No.</th>
                  <th>NISN</th>
                  <th>Nama Siswa</th>
                  <th>Alamat</th>
                  <th>Income</th>
                  <th>Dependent</th>
                  <th>Out Economic</th>
                  <th>Assessment</th>
                  <th>Achievment</th>
                  <th>Out Academic</th>
                  <th>Homeower</th>
                  <th>Sanitation</th>
                  <th>Marital</th>
                  <th>Source Water</th>
                  <th>OutHouseHold</th>
                  <th>Land</th>
                  <th>House</th>
                  <th>Out Area</th>
                  <th>Out Decision</th>
                  <th>Time</th>
                  <th>Tanggal Sekarang</th>
                  <th>Durasi</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($fuzzy as $f)
                <tr>
                  <td>{{ $loop->index + 1 }}</td>
                  <td>{{ $f->nisn }}</td>
                  <td>{{ $f->nama }}</td>
                  <td>{{ $f->alamat }}</td>
                  <td>{{ $f->income }}</td>
                  <td>{{ $f->dependent }}</td>
                  <td>{{ $f->outeconomic }}</td>
                  <td>{{ $f->assessment }}</td>
                  <td>{{ $f->achievment }}</td>
                  <td>{{ $f->outacademic }}</td>
                  <td>{{ $f->homeower }}</td>
                  <td>{{ $f->sanitation }}</td>
                  <td>{{ $f->marital }}</td>
                  <td>{{ $f->sourcewater }}</td>
                  <td>{{ $f->outhousehold }}</td>
                  <td>{{ $f->land }}</td>
                  <td>{{ $f->house }}</td>
                  <td>{{ $f->outarea }}</td>
                  <td>{{ $f->outdecision }}</td>
                  <td>{{ $f->time }}</td>
                  <td>{{ $f->tgl_skrg }}</td>
                  <td>{{ $f->durasi }}</td>
                </tr>
                @endforeach
              </tbody>
            </table> 
          </div>
        </div>

      </div>
    </div>
  <!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script>
@endsection