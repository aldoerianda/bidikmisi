@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful animated fadeIn">
        <div class="panel-heading">
          <h3 class="panel-title">Validasi Data Penghasilan Orangtua</h3>
        </div>
        <div class="panel-body">
          <form action="{{ url('pengelola/validpenghasilan') }}" method="post">
            {{ csrf_field()}}
            <div class="form-group">
              <div class="row">
                <label for="pendapatan" class="col-md-2 col-xs-12 control-label">Pendapatan Maksimal</label>
                <div class="col-md-9 col-xs-12">
                  <input type="text" name="pendapatan" class="form-control" placeholder="Pendapatan">
                </div>           
              </div><br>
              <div class="row">
                <label for="rata" class="col-md-2 col-xs-12 control-label">Rata-Rata Pendapatan</label>
                <div class="col-md-9 col-xs-12">
                  <input type="text" name="rata" class="form-control" placeholder="Rata-Rata Pendapatan">
                </div>
              </div><br>
              <div class="row">
                <div class="col-md-2 col-xs-12">
                  <input type="submit" class="btn btn-success" name="submit" value="Validasi">
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="panel-body table-responsive">
          <table class="table datatable table-hover">
            <thead>
              <tr>
                <th>No.</th>
                <th>NISN</th>
                <th>Nama Siswa</th>
                <th>Pendapatan Orangtua</th>
                <th>Jumlah Tanggungan</th>
                <th>Rata Biaya</th>
                <!--<th>Keterangan</th>-->
              </tr>
            </thead>
            <tbody>
              @foreach ($penghasilans as $penghasilan)
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $penghasilan->nisn }}</td>
                <td>{{ $penghasilan->nama_siswa}}</td>
                <td>{{ $penghasilan->income }}</td>
                <td>{{ $penghasilan->jmlh_tanggungan }}</td>
                <td>{{ $penghasilan->rata_biaya}}</td>
              </tr>
              @endforeach
            </tbody>
          </table> 
        </div>
      </div>
    </div>
  </div>
  <!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->
<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/plugins/bootstrap/bootstrap-select.js')}}"></script> 
@endsection