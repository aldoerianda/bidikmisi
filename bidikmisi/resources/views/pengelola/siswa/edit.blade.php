@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful">
        <div class="panel-heading">
          <h3 class="panel-title">Form Edit Siswa</h3>
        </div>

        @include('includes.messages')

        <form role="form" action="{{ route('siswa.update',$siswa->nisn)}}" method="post" class="form-horizontal">
          {{ csrf_field()}}
          {{ method_field('PATCH')}}
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nisn">NISN</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nisn" name="nisn" placeholder="NISN" value="{{$siswa->nisn}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="nama_siswa">Nama siswa</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="nama_siswa" name="nama_siswa" placeholder="nama_siswa" value="{{ $siswa->nama_siswa}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="tempat_lahir">Tempat Lahir</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" placeholder="tempat_lahir" value="{{ $siswa->tempat_lahir}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="tgl_lahir">Tanggal Lahir</label>
              <div class="col-md-9 col-xs-12">
                <input type="date" class="form-control" id="tgl_lahir" name="tgl_lahir" placeholder="tgl_lahir" value="{{ $siswa->tgl_lahir}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="jekel">Jenis Kelamin</label>
              <div class="col-md-9 col-xs-12">
                <label class="check"><input type="radio" class="iradio" name="jekel" value="L" {{$siswa->jekel == 'L' ? 'checked' :''}}>Laki-Laki</label>
                <br>
                <label class="check"><input type="radio" class="iradio" name="jekel" value="P" {{$siswa->jekel == 'P' ? 'checked' :''}}>Perempuan</label>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="no_hp">No. HP</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="no hp" value="{{ $siswa->no_hp}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="no_telp">No. Telp</label>
              <div class="col-md-9 col-xs-12">
                <input type="text" class="form-control" id="no_telp" name="no_telp" placeholder="no telp" value="{{ $siswa->no_telp}}">
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="form-group">
              <label class="col-md-2 col-xs-12 control-label" for="alamat">Alamat</label>
              <div class="col-md-9 col-xs-12">
                <textarea id="alamat" name="alamat" class="form-control" rows="5">{{ $siswa->alamat}}</textarea>
              </div>
            </div>
          </div>
          <div class="panel-body">
           <div class="form-group">
            <label class="col-md-2 col-xs-12 control-label" for="tgl_daftar">Tanggal Daftar</label>
            <div class="col-md-9 col-xs-12">
              <input type="datetime" class="form-control" id="tgl_daftar" name="tgl_daftar" placeholder="tanggal daftar" value="{{ $siswa->tgl_daftar}}">
            </div>
          </div>
        </div>
        <div class="panel-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <a href='{{route("siswa.index")}}' class="btn btn-warning">Kembali</a>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
@endsection