@extends('pengelola.layouts.app')

@section('main-content')
<div class="page-content-wrap">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-colorful animated fadeIn">
        <div class="panel-heading">
          <h3 class="panel-title">Data Siswa</h3>
          <a class='col-lg-offset-10 btn btn-success' href="{{ route('siswa.create')}}">Tambah Siswa</a>
        </div>
        <div class="panel-body table-responsive">
          <table class="table datatable table-hover">
            <thead>
              <tr>
                <th>No.</th>
                <th>NISN</th>
                <th>Nama Siswa</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>JK</th>
                <th>No. HP</th>
                <th>No. Telp</th>
                <th>Alamat</th>
                <th>Tanggal Daftar</th>
                <th width="5%">Aksi</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($siswas as $siswa)
              <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $siswa->nisn }}</td>
                <td>{{ $siswa->nama_siswa }}</td>
                <td>{{ $siswa->tempat_lahir }}</td>
                <td>{{ $siswa->tgl_lahir }}</td>
                <td>{{ $siswa->jekel }}</td>
                <td>{{ $siswa->no_hp }}</td>
                <td>{{ $siswa->no_telp }}</td>
                <td>{{ $siswa->alamat }}</td>
                <td>{{ $siswa->tgl_daftar }}</td>
                <td><a href="{{route('siswa.edit',$siswa->nisn)}}"><span class="glyphicon glyphicon-edit"></span></a>
                  <form id="delete-form-{{ $siswa->nisn }}" method="post" action="{{route('siswa.destroy',$siswa->nisn)}}" style="display: none">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                  </form>
                  <a href="" onclick="
                  if(confirm('Yakin Ingin Menghapus Data siswa?'))
                  {
                    event.preventDefault();
                    document.getElementById('delete-form-{{ $siswa->nisn }}').submit();
                  } else {
                    event.preventDefault();
                  }"><span class="glyphicon glyphicon-trash"></span></a>
                </td>
              </tr>
              @endforeach
            </tbody>
          </table> 
        </div>
      </div>
    </div>
  </div>
  <!-- /.content -->
</div>

@endsection

@section('script')
<!-- DataTables -->

<script type="text/javascript" src="{{asset('js/plugins/datatables/jquery.dataTables.min.js')}}"></script> 
@endsection