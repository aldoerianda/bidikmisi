@extends('pengelola.layouts.app')

@section('head')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('pengelola/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
@endsection

@section('main-content')
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Siswa</h3>
             
              
              {{-- <a class='col-lg-offset-10 btn btn-success' href="{{ route('siswa.create')}}">Tambah Siswa</a> --}}
            </div>
        
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>NISN</th>
                  <th>Nama Siswa</th>
                  <th>Tempat Lahir</th>
                  <th>Tanggal Lahir</th>
                  <th>JK</th>
                  <th>No. HP</th>
                  <th>No. Telp</th>
                  <th>Alamat</th>
                  <th>Tanggal Daftar</th>
                  <th width="7%">Edit</th>
                  <th width="7%">Delete</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($siswas as $siswa)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $siswa->nisn }}</td>
                    <td>{{ $siswa->nama_siswa }}</td>
                    <td>{{ $siswa->tempat_lahir }}</td>
                    <td>{{ $siswa->tgl_lahir }}</td>
                    <td>{{ $siswa->jekel }}</td>
                    <td>{{ $siswa->no_hp }}</td>
                    <td>{{ $siswa->no_telp }}</td>
                    <td>{{ $siswa->alamat }}</td>
                    <td>{{ $siswa->tgl_daftar }}</td>
                    <td><a href="{{route('siswa.edit',$siswa->id)}}"><span class="glyphicon glyphicon-edit"></span></a></td>
                     <td>
                      <form id="delete-form-{{ $siswa->id }}" method="post" action="{{route('siswa.destroy',$siswa->id)}}" style="display: none">
                        {{csrf_field()}}
                        {{method_field('DELETE')}}
                      </form>
                      <a href="" onclick="
                      if(confirm('Yakin Ingin Menghapus Data siswa?'))
                        {
                          event.preventDefault();
                          document.getElementById('delete-form-{{ $siswa->id }}').submit();
                        } else {
                          event.preventDefault();
                        }"><span class="glyphicon glyphicon-trash"></span></a>
                        
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>

        </div>
      </div>
      <div class="box-footer">
        <a href='{{route('siswa.index')}}' class="btn btn-warning">Kembali</a>
      </div>
    </section>

    </section>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('pengelola/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('pengelola/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script>
  $(function () {
    $('#myTable').DataTable()
  })
</script>
@endsection