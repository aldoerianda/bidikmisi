<!DOCTYPE html>
<html lang="en">
<head>
	@include('user.layouts.head')
</head>
<body>
	<!-- Navigation -->
	@include('user.layouts.header')
	@section('main-content')
	@show
	@include('user.layouts.footer')
	<!-- jQuery 3 -->
	<script src="{{ asset('pengelola/bower_components/jquery/dist/jquery.min.js' )}}"></script>
	<!-- Bootstrap 3.3.7 -->
	<script src="{{ asset('pengelola/bower_components/bootstrap/dist/js/bootstrap.min.js' )}}"></script>
	<!-- SlimScroll -->
	<script src="{{ asset('pengelola/bower_components/jquery-slimscroll/jquery.slimscroll.min.js' )}}"></script>
	<!-- FastClick -->
	<script src="{{ asset('pengelola/bower_components/fastclick/lib/fastclick.js' )}}"></script>
	<!-- AdminLTE App -->
	<script src="{{ asset('pengelola/dist/js/adminlte.min.js' )}}"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="{{ asset('pengelola/dist/js/demo.js' )}}"></script>
	@section('script')
	@show
</body>
</html>