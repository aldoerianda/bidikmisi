@extends('user/app')
@section('head')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('dosen/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
@endsection
@section('main-content')
<div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-xs-12">
            <section class="content">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Prestasi Siswa</h3>
            </div>
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>NISN</th>
                  <th>Nama Siswa</th>
                  <th>Asal Sekolah</th>
                  {{-- <th>Rangking Smst 4</th>
                  <th>Total Nilai Smst 4</th>
                  <th>Jumlah Mapel Smst 4</th>
                  <th>Rangking Smst 5</th>
                  <th>Total Nilai Smst 5</th>
                  <th>Jumlah Mapel Smst 5</th> --}}
                  <th>Prestasi Ekskul</th>
                  <th>Tanggal Daftar</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($nilais as $nilai)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $nilai->nisn }}</td>
                    <td>{{ $nilai->nama_siswa }}</td>
                    <td>{{ $nilai->nama_sklh }}</td>
                    {{-- 
                    <td>{{ $nilai->rang_smst_4 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_4 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_4 }}</td>
                    <td>{{ $nilai->rang_smst_5 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_5 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_5 }}</td> --}}
                    <td>{{ $nilai->pres_ekskul }}</td>
                    <td>{{ $nilai->tgl_daftar }}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>
      <div class="box-footer">
        <a href='{{route('prestasi_ekskul.index')}}' class="btn btn-warning">Kembali</a>
      </div>
      </section>
        </div>

   

      </div>
      <!-- /.row -->

    </div>
@endsection
@section('script')
<!-- DataTables -->
<script src="{{ asset('dosen/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('dosen/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script>
  $(function () {
    $('#myTable').DataTable()
  })
</script>
@endsection