@extends('user/app')

@section('main-content')
    <div class="container">
      <div class="row">
        <section class="content">
          <div class="col-md-8">
            <div class="box box solid">
              <div class="box-body">
                <h3>
                  {{ $berita->judul}}
                </h3>
                <h5><p>{{ $berita->isi}}</p></h5>
              </div>
            </div>
          </div>
          <div class="col-md-4">
      @include('includes.side')
    </div>
        </section>
      </div>
    </div>

@endsection