@extends('user/app')

@section('head')
<script src="{{ asset('pengelola/dist/js/jquery-3.1.0.min.js')}}"></script>
  <script type="text/javascript" src="{{ asset('pengelola/dist/js/loader.js')}}"></script>
  <link rel="stylesheet" href="{{ asset('pengelola/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
   <style type="text/css">
   .box{
    width:800px;
    margin:0 auto;
   }
  </style>
  <script type="text/javascript">
   var analytics = <?php echo $kota_sklh; ?>

   google.charts.load('current', {'packages':['corechart']});

   google.charts.setOnLoadCallback(drawChart);

   function drawChart()
   {
    var data = google.visualization.arrayToDataTable(analytics);
    var options = {
     title : 'Persentase Banyak Siswa Per Kota/Kabupaten'
    };
    var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));
    chart.draw(data, options);
   }
  </script>
@endsection

@section('main-content')
<div class="content-wrapper">

    <!-- Main content -->
    <section class="content container-fluid">

      <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          
          <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
   
   <div class="panel panel-default">
    <div class="panel-heading">
     <h3 class="panel-title" align="center">Persentase Banyak Siswa Per Kota</h3>
     <form action="{{route('banyaksiswa_perkota.show')}}" method="post" class="form-horizontal">
                {{ csrf_field()}}
                <div class="form-group">
                  <label>Pilih Provinsi</label>
                  <select name="prov_sklh" class="form-control">
                    <option value="">==Pilih Provinsi==</option>
                    @foreach ($showprov as $data)
                    <option>{{ $data->prov_sklh }}</option>
                    @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <div class="col-md-2">
                <br><select name="tahun" class="form-control">
                    <option value="">== Pilih Tahun ==</option>
                    {{ $now = Carbon\carbon::now()->year }}
                    @for ($i = 2017; $i <= $now ; $i++)
                    <option value="{{ $i }}">{{ $i }}</option>
                    @endfor
                  </select>              
                  </div>
                </div>
                <input type="submit" class="btn btn-success" name="cari" value="Cari">
              </form>
    </div>
    <div class="panel-body" align="center">
     <div id="pie_chart" style="width:750px; height:450px;">

     </div>
    </div>
   </div>



            </div>
          </div>

        </div>
      </div>
    </section>

    </section>
    <!-- /.content -->
  </div>

@endsection

@section('script')
<!-- DataTables -->
<script src="{{ asset('pengelola/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('pengelola/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script>
  $(function () {
    $('#myTable').DataTable()
  })
</script>
@endsection