@extends('user/app')
@section('head')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('dosen/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
@endsection
@section('main-content')
<div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-xs-12">
            <section class="content">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data nilai</h3>
              <form action="{{ route('validasi_nilai.show') }}" method="post" class="form-horizontal">
              {{ csrf_field()}}
              <div class="form-group">
                <div class="col-md-2">
              <br><select name="tahun" class="form-control">
                  <option value="">== Pilih Tahun ==</option>
                  {{ $now = Carbon\carbon::now()->year }}
                  @for ($i = 2017; $i <= $now ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                  @endfor
                </select>              
                </div>
              </div>
              <input type="submit" class="btn btn-success" name="Cari" value="Cari">
            </form>
            </div>
          
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>NISN</th>
                  <th>Nama Siswa</th>
                  {{-- <th>Tanggal Daftar</th> --}}
                  <th>Total Nilai Smst 4</th>
                  <th>Jumlah Mapel Smst 4</th>
                  <th>Total Nilai Smst 5</th>
                  <th>Jumlah Mapel Smst 5</th>
                  <th>Rata-Rata</th>
                  <th>Validasi</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($nilais as $nilai)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $nilai->nisn }}</td>
                    <td>{{ $nilai->nama_siswa }}</td>
                    {{-- <td>{{ $nilai->tgl_daftar }}</td> --}}
                    <td>{{ $nilai->ttl_nilai_smst_4 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_4 }}</td>
                    <td>{{ $nilai->ttl_nilai_smst_5 }}</td>
                    <td>{{ $nilai->jmlh_mapel_smst_5 }}</td>
                    <td>{{ number_format($nilai->rata,2) }}</td>
                    <td>
                      @if($nilai->rata > '0' && $nilai->rata <= '100')
                       Valid
                      @else Tidak Valid
                      @endif
                    </td>
                    
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>
        <!-- /.box -->
      </section>
        </div>

 

      </div>
      <!-- /.row -->

    </div>
@endsection
@section('script')
<!-- DataTables -->
<script src="{{ asset('dosen/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('dosen/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script>
  $(function () {
    $('#myTable').DataTable()
  })
</script>
@endsection