  <footer class="py-0 bg-blue">
    <div class="container">
      <div class="text-center hidden-xs">
        <br>POLITEKNIK NEGERI PADANG
        <br>Limau Manis, Kecamatan Pauh, Kota Padang
        <br>Telp/Fax  :(0751) 72590/(0751) 72576
        <br>Email :info@pnp.ac.id</br></br>
      </div>
    </footer>
  </div>
  <!-- ./wrapper -->
  @section('script')
  @show

