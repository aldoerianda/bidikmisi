<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">
    <header class="main-header">
      <nav class="navbar navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <div class="pull-left image">
              <img src="{{ asset('user/img/logopnp.png')}}" width="60px" height="45px" class="img" alt="User Image">
            </div>
            <a href="{{ route('user.home')}}" class="navbar-brand"><b>&nbsp;&nbsp;&nbsp;SI-BM</b> PNP</a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse pull-right" id="navbar-collapse">
            <ul class="nav navbar-nav">
              <li class="treeview"><a href="{{ route('user.home')}}">Beranda</a></li>
            <!-- <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Validasi Data <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ route('validasi_nilai.index')}}">Validasi Data Nilai</a></li>
                <li><a href="{{ route('validasi_jurusan.index')}}">Validasi Data Jurusan</a></li>
              </ul>
            </li> -->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Banyak Siswa <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ route('banyaksiswa_persekolah.index')}}">Banyak Siswa Per Sekolah</a></li>
                <li><a href="{{ route('banyaksiswa_perprovinsi.index')}}">Banyak Siswa Per Provinsi</a></li>
                <li><a href="{{ route('banyaksiswa_perkota.index')}}">Banyak Siswa Per Kota/Kabupaten</a></li>
              </ul>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Persentase Pemilihan Prodi <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="{{ route('laravelgooglechart.index')}}">Prodi Pilihan 1</a></li>
                <li><a href="{{ route('laravelgooglechart2.index')}}">Prodi Pilihan 2</a></li>
              </ul>
            </li>
            <!-- <li class="treeview"><a href="{{ route('prestasi_ekskul.index')}}">Prestasi Ekskul Siswa</a></li>
              <li class="treeview"><a href="{{ route('jadwal_visitasi.index')}}">Jadwal Visitasi</a></li> -->
              
            </ul>
          </div>
          <!-- /.navbar-collapse -->
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <!-- Menu toggle button -->
              </div>
              <!-- /.container-fluid -->
            </nav>
          </header>
          <!-- Full Width Column --> 
          <div class="content-wrapper">
              <!-- Content Header (Page header) -->
              
              