@extends('user/app')

@section('main-content')
<div class="container">
  <div class="row">
    <!-- Blog Entries Column -->
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"></h3>
        </div>
        <div class="box-body">
          <img src="{{ asset('user/img/bm.jpg')}}" class="img-responsive" style="width:50%; height: 150px;" ><br>
          <div class="callout callout-danger">
            <h5>Sistem Informasi Bidikmisi Politeknik Negeri Padang</h5>

            <p>Sistem Informasi Bidikmisi Politeknik Negeri Padang merupakan sebuah sistem yang menyajikan informasi yang berkaitan dengan calon mahasiswa bidikmisi yang telah mendaftar pada tahun yang bersangkutan. informasi yang disajikan berupa data calon mahasiswa bidikmisi yang valid, data calon mahasiswa bidikmisi yang tidak valid, data banyak siswa per sekolah, per provinsi, per kabupaten serta informasi mengenai prestasi ekstrakurikuler yang dimiliki oleh calon mahasiswa bidikmisi yang telah mendaftar di Politeknik Negeri Padang  </p>
          </div>
        </div>
      </div>
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Berita</h3>
        </div>
          <div class="box-body">
            <h4>Berita</h4>
            @foreach ($show as $data)
            <h4>
              <a href="{{route('berita', $data->slug)}}" style="color: #555">{{ $data->judul}}</a>
            </h4>
            <small>{{ $data->crated_at}}</small>
            <h5><p>{{ substr($data->isi, 0, 750) }}... <a href="{{route('berita', $data->slug) }}">Selengkapnya</a></p></h5>
            <hr>
            @endforeach
            <div class="pagination">
              {{ $show->links()}}
            </div>
          </div>
        </div>
      </div>
      </div>
    </div>
  </div>
</div>
@endsection