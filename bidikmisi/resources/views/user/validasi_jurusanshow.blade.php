@extends('user/app')
@section('head')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('dosen/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
@endsection
@section('main-content')
<div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-xs-12">
            <section class="content">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Validasi Jurusan</h3>
            </div>
            
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>No. Pendaftaran</th>
                  <th>NISN</th>
                  <th>Tanggal Daftar</th>
                  <th>Jurusan Sekolah</th>
                  <th>Pil. Prodi 1</th>
                  <th>Pil. Prodi 2</th>
                  <th>Keterangan</th>
                </tr>
                </thead>
                <tbody>
                  @foreach ($pendaftarans as $pendaftaran)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $pendaftaran->no_pend}}</td>
                    <td>{{ $pendaftaran->nisn }}</td>
                    <td>{{ $pendaftaran->tgl_daftar }}</td>
                    <td>{{ $pendaftaran->jur_sekolah }}</td>
                    <td>{{ $pendaftaran->pil_prodi_1}}</td>
                    <td>{{ $pendaftaran->pil_prodi_2 }}</td>
                    <td>{{ check($pendaftaran->jur_sekolah,$pendaftaran->pil_prodi_1, $pendaftaran->pil_prodi_2)}}</td>
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>
        <!-- /.box -->
        <div class="box-footer">
        <a href='{{route('validasi_jurusan.index')}}' class="btn btn-warning">Kembali</a>
      </div>
      </section>
        </div>

  

      </div>
      <!-- /.row -->

    </div>
@endsection
@section('script')
<!-- DataTables -->
<script src="{{ asset('dosen/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('dosen/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script>
  $(function () {
    $('#myTable').DataTable()
  })
</script>
@endsection