@extends('user/app')
@section('head')
<!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('dosen/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }} ">
@endsection
@section('main-content')
<div class="container">

      <div class="row">

        <!-- Blog Entries Column -->
        <div class="col-xs-12">
            <section class="content">
         <div class="box">
            <div class="box-header">
              <h3 class="box-title" align="center">Jadwal Visitasi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="myTable" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th width="7%">No.</th>
                  <th>Nama Siswa</th>
                  <th>Nama Dosen</th>
                  <th>Tanggal Visit</th>
                  {{-- <th>Keterangan</th> --}}
                </tr>
                </thead>
                <tbody>
                  @foreach ($visitasis as $data)
                  <tr>
                    <td>{{ $loop->index + 1 }}</td>
                    <td>{{ $data->nama_siswa }}</td>
                    <td>{{ $data->nama_dosen }}</td>
                    <td>{{ $data->tgl_visit }}</td>
                   {{--  <td>{{ $data->keterangan }}</td> --}}
                  </tr>
                  @endforeach
                </tbody>
              </table> 
            </div>
          </div>
        <!-- /.box -->
      </section>
        </div>

  

      </div>
      <!-- /.row -->

    </div>
@endsection
@section('script')
<!-- DataTables -->
<script src="{{ asset('dosen/bower_components/datatables.net/js/jquery.dataTables.min.js') }} "></script>
<script src="{{ asset('dosen/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }} "></script>
<script>
  $(function () {
    $('#myTable').DataTable()
  })
</script>
@endsection